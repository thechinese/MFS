package main;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;
import org.neo4j.driver.v1.Record;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.StatementResult;
import org.neo4j.driver.v1.Transaction;
import org.neo4j.driver.v1.TransactionWork;
import org.neo4j.driver.v1.types.Node;

import com.jayway.jsonpath.JsonPath;

public class QueryHelper {

	private int idCounter = 0;

	protected int getIdCounter() {
		return idCounter;
	}

	protected void setIdCounter(int idCounter) {
		this.idCounter = idCounter;
	}

	protected static boolean parametersExist(String queryFlag, Map<String, String[]> queryParams) {
		boolean parametersExist = false;
		for (String key : queryParams.keySet()) {
			if (key != queryFlag && queryParams.get(key) != null) {
				parametersExist = true;
				break;
			}
		}
		if (!parametersExist)
			return false;
		return true;
	}

	protected JSONArray searchPerson(Session session, Map<String, String[]> queryParams) {
		return searchPerson(session, queryParams, 0);
	}

	protected JSONArray searchPerson(Session session, Map<String, String[]> queryParams, int offset) {

		String queryString = QueryConstants.searchPersonPrefix;
		if (queryParams.get(Servlet.nameParameter) != null)
			queryString += QueryConstants.searchPersonLabelInfix1 + queryParams.get(Servlet.nameParameter)[0]
					+ QueryConstants.searchPersonLabelInfix2;

		if (queryParams.get(Servlet.ageParameter) != null)
			queryString += QueryConstants.searchPersonAgeInfix + queryParams.get(Servlet.ageParameter)[0];

		if (queryParams.get(Servlet.yearOfBirthParameter) != null)
			queryString += QueryConstants.searchPersonYearOfBirthInfix1
					+ queryParams.get(Servlet.yearOfBirthParameter)[0] + "";

		if (queryParams.get(Servlet.yearOfDeathParameter) != null)
			queryString += QueryConstants.searchPersonYearOfDeathInfix2
					+ queryParams.get(Servlet.yearOfDeathParameter)[0] + "";

		if (queryParams.get(Servlet.dateOfBirthParameter) != null)
			queryString += QueryConstants.searchPersonDateOfBirthInfix1
					+ queryParams.get(Servlet.dateOfBirthParameter)[0] + "\'"
					+ QueryConstants.searchPersonDateOfBirthInfix2 + queryParams.get(Servlet.dateOfBirthParameter)[1]
					+ "\'";

		if (queryParams.get(Servlet.dateOfDeathParameter) != null)
			queryString += QueryConstants.searchPersonDateOfDeathInfix1
					+ queryParams.get(Servlet.dateOfDeathParameter)[0] + "\'"
					+ QueryConstants.searchPersonDateOfDeathInfix2 + queryParams.get(Servlet.dateOfDeathParameter)[1]
					+ "\'";

		if (queryParams.get(Servlet.childCountParameter) != null)
			queryString += QueryConstants.searchPersonChildrenCountInfix
					+ queryParams.get(Servlet.childCountParameter)[0];
		if (queryParams.get(Servlet.spouseCountParameter) != null)
			queryString += QueryConstants.searchPersonSpousesCountInfix
					+ queryParams.get(Servlet.spouseCountParameter)[0];
		if (queryParams.get(Servlet.hasSpouseParameter) != null)
			queryString += QueryConstants.searchPersonSpousesExistInfix;

		if (queryParams.get(Servlet.hasChildParameter) != null)
			queryString += QueryConstants.searchPersonChildrenExistInfix;
		if (queryParams.get(Servlet.siblingCountParameter) != null)
			queryString += QueryConstants.searchPersonSiblingsCountInfix
					+ queryParams.get(Servlet.siblingCountParameter)[0];
		if (queryParams.get(Servlet.hasSiblingParameter) != null)
			queryString += QueryConstants.searchPersonSiblingsExistInfix;

		if (offset > 0)
			queryString += QueryConstants.searchPersonSkipSuffix1 + offset * 30 + QueryConstants.searchPersonSkipSuffix2
					+ (offset + 1) * 30 + QueryConstants.searchPersonSkipSuffix3;
		else
			queryString += QueryConstants.searchPersonSuffix;
		return executeQuery(session, queryString);
	}

	private JSONArray setArrayIDs(JSONArray array, boolean setNoParentFlag) {
		int currentCounter = getIdCounter();
		for (int i = 0; i < array.length(); i++) {
			array.getJSONObject(i).put("id", currentCounter++);
			if (setNoParentFlag)
				array.getJSONObject(i).put("no_parent", true);
		}
		setIdCounter(currentCounter);
		return array;
	}

	private JSONArray setArrayIDs(JSONArray array) {
		return setArrayIDs(array, false);
	}

	protected JSONObject setNodeID(JSONObject node, boolean setNoParentFlag) {
		int currentCounter = getIdCounter();
		node.put("id", currentCounter++);
		if (setNoParentFlag)
			node.put("no_parent", true);
		setIdCounter(currentCounter);
		return node;
	}

	protected JSONObject setNodeID(JSONObject node) {
		return setNodeID(node, false);
	}

	protected JSONObject getPlaceHolderNode() {
		JSONObject result = new JSONObject();
		result.put("name", "");
		result.put("hidden", true);
		result.put("no_parent", true);
		return result;
	}

	/**
	 * @return The initial tree that is displayed. It consists of the parents of the
	 *         selected person, the siblings of the person, up to two spouses of the
	 *         person and children of the person
	 */
	protected JSONObject fetchInitialTree(Session session, String genID) {
		// Top level of the returned JSON, includes all tree elements
		JSONObject rootNode = getPlaceHolderNode();
		rootNode.remove("no_parent");

		// Returned relations between all parents and children, including parent to
		// selected node and node to its own children
		JSONArray childRelations = new JSONArray();
		// Returned relations between spouses, between the selected node's parents and
		// between the node and its spouses
		JSONArray spouseRelations = new JSONArray();

		// Container for nodes that are on the first level of the tree:
		// parents and parentplaceholder or person, siblings and spouses if no parents
		// are found
		JSONArray rootLayerChildren = new JSONArray();
		// Parents fetched from the db, fetched respective to the selected UI node
		JSONArray parents = new JSONArray();
		// Spouses fetched from the db, fetched respective to the selected UI node
		JSONArray spouses = new JSONArray();
		// Siblings fetched from the db, fetched respective to the selected UI node
		// Has to match at least one parent of the selected node
		JSONArray siblings = new JSONArray();
		// Children of the selected node, if a spouse exists, they must be children of
		// that node as well
		JSONArray children = new JSONArray();
		// Easier parsable representation of the fetched childRelations, only lists
		// relations between locally stored nodes
		Map<String, Set<String>> childParentRelations = new HashMap<>();
		// Unique permutations of genIDs of parent nodes that contain children that can
		// include the node and its siblings
		List<Set<String>> uniqueParentPlaceholders = new ArrayList<>();
		// Unique placeholders that contain children of the parent nodes that can
		// contain the node selected from the UI and its siblings, for each unique
		// permutation of parent nodes a unique placeholder is created
		List<JSONObject> uniqueParentPlaceholderNodes = new ArrayList<>();
		// Unique permutations
		List<Set<String>> uniqueChildPlaceholders = new ArrayList<>();
		// Unique placeholders that contain children of the node selected from the UI
		// and its spouses, for each unique permutation of parent nodes a unique
		// placeholder is created
		List<JSONObject> uniqueChildPlaceholderNodes = new ArrayList<>();
		// Unique relations of spouse to a set of spouses, includes parents of the
		// node selected in the UI and between the selected node and its spouses
		Map<String, Set<String>> spouseToSpouseRelations = new HashMap<>();

		setIdCounter(1);

		// currentLayer contains the node of the sent genID, fetch Counts collectively
		// at the end
		JSONObject person = getPersonByGenID(session, genID);
		if (person == null)
			return rootNode;
		// Check structure for errors
		spouses = getSpouses(session, genID);

		// Set up ids, properties and placeholder nodes
		// No spouse relation exists anymore, but for modeling a hierarchy it's needed
		if (!spouses.isEmpty()) {
			String existingSpouse = spouses.getJSONObject(0).getString("genID");
			children = getChildren(session, genID, new String[] { existingSpouse });
		} else
			children = getChildren(session, genID, null);
		parents = getParents(session, genID);

		// Only fetch siblings for the existing parents, only when extending the tree,
		// half-siblings and potentially missing parents are fetched as well
		List<String> parentList = new ArrayList<>();
		if (parents.length() > 0) {
			for (int i = 0; i < parents.length(); i++)
				parentList.add(parents.getJSONObject(i).getString("genID"));
			siblings = getSiblings(session, genID, parentList.toArray(new String[parentList.size()]), true);
		}

		// Unset no_parent if siblings only have one parent
		// Get ids for setting up id-based relations later

		// List of ids of all siblings, used to decide to create additional
		// placeholder parent nodes
		List<String> siblingIds = new ArrayList<>();
		// List of ids of all children fetched for the selected UI node, used in
		// combination with childParentIds to create new relations between nodes of the
		// level of the selected UI node and child nodes
		List<String> childIds = new ArrayList<>();
		// Store person, sibling and spouse ids, used to decide whether to create
		// additional placeholder parent nodes
		// Also used for creating new relations between parent and children on level of
		// the UI selected node if it doesn't exist
		// Mappings between existing parent and child relations is extended with these
		// ids
		List<String> childParentIds = new ArrayList<>();
		// List of ids at level of the node selected in the UI, used to determine, if
		// relations between fetched parents of the selected node in the UI and node on
		// the level as the selected node can be created
		List<String> nodeLevelIds = new ArrayList<>();
		nodeLevelIds.add(person.getString("genID"));
		// Check if parents are in a spouse relation or not
		childParentIds.add(person.getString("genID"));
		for (int i = 0; i < siblings.length(); i++)
			childParentIds.add(siblings.getJSONObject(i).getString("genID"));
		for (int i = 0; i < spouses.length(); i++)
			childParentIds.add(spouses.getJSONObject(i).getString("genID"));

		if (parents.length() > 1) {
			// Add relations for node selected in the UI
			{
				Set<String> parentIds = new HashSet<String>();
				for (int i = 0; i < parents.length(); i++)
					parentIds.add(parents.getJSONObject(i).getString("genID"));
				childParentRelations.put(genID, parentIds);
			}
			if (siblings.length() > 0) {
				String[] limitedSiblingArray = new String[siblings.length() - 1];
				nodeLevelIds.add(siblings.getJSONObject(0).getString("genID"));
				siblingIds.add(siblings.getJSONObject(0).getString("genID"));
				for (int i = 1; i < siblings.length(); i++) {
					siblingIds.add(siblings.getJSONObject(i).getString("genID"));
					limitedSiblingArray[i - 1] = siblings.getJSONObject(i).getString("genID");
					nodeLevelIds.add(siblings.getJSONObject(i).getString("genID"));
				}
				// Fetch relations between parents and siblings to later decide whether to
				// create additional placeholder for every unique permutation of parents
				JSONArray siblingParentRelations = getParentChildRelationsOfChildren(session,
						siblings.getJSONObject(0).getString("genID"), limitedSiblingArray);
				Collection<Node> list = (Collection<Node>) (siblingParentRelations.getJSONObject(0))
						.get("childRelations");
				Iterator<Node> iterator = list.iterator();
				while (iterator.hasNext()) {
					Map<String, String> map = new HashMap<String, String>();
					map = (Map<String, String>) iterator.next();
					String parentString = map.get("parent");
					String personString = map.get("person");
					if (parentList.contains(parentString) && nodeLevelIds.contains(personString))
						if (!childParentRelations.getOrDefault(personString, new HashSet<String>())
								.contains(parentString)) {
							Set<String> existingParents = childParentRelations.getOrDefault(personString,
									new HashSet<String>());
							existingParents.add(parentString);
							childParentRelations.put(personString, existingParents);
						}
				}
			}
			// Fetch relationship among parent and step parent
			JSONArray parentRelations = getParentSpouseRelationsOfChild(session, genID);
			// Relation not yet present in UI JSON
			Collection<Node> list = (Collection<Node>) (parentRelations.getJSONObject(0)).get("parentRelations");
			Iterator<Node> iterator = list.iterator();
			while (iterator.hasNext()) {
				Map<String, String> map = new HashMap<String, String>();
				map = (Map<String, String>) iterator.next();
				String spouse1 = map.get("spouse1");
				String spouse2 = map.get("spouse2");
				if (parentList.contains(spouse1) && parentList.contains(spouse2))
					if (!(spouseToSpouseRelations.getOrDefault(spouse1, new HashSet<String>()).contains(spouse2)
							|| spouseToSpouseRelations.getOrDefault(spouse2, new HashSet<String>())
									.contains(spouse1))) {
						Set<String> existingSpouses = spouseToSpouseRelations.getOrDefault(spouse1,
								new HashSet<String>());
						existingSpouses.add(spouse2);
						spouseToSpouseRelations.put(spouse1, existingSpouses);
					}

			}
		}
		if (children.length() > 0) {
			String[] limitedChildIdArray = new String[children.length() - 1];
			for (int i = 0; i < children.length(); i++) {
				childIds.add(children.getJSONObject(i).getString("genID"));
				if (i > 0)
					limitedChildIdArray[i - 1] = children.getJSONObject(i).getString("genID");
			}
			// Fetch relations to parents of the children of the node selected in the UI,
			// adjust to more than one child in the function call
			JSONArray fetchedChildParentRelations = limitedChildIdArray.length > 0
					? getParentChildRelationsOfChildren(session, children.getJSONObject(0).getString("genID"),
							limitedChildIdArray)
					: getParentChildRelationsOfChildren(session, children.getJSONObject(0).getString("genID"));
			Collection<Node> list = (Collection<Node>) (fetchedChildParentRelations.getJSONObject(0))
					.get("childRelations");
			Iterator<Node> iterator = list.iterator();
			while (iterator.hasNext()) {
				Map<String, String> map = new HashMap<String, String>();
				map = (Map<String, String>) iterator.next();
				String parentString = map.get("parent");
				String personString = map.get("person");
				if (childParentIds.contains(parentString) && childIds.contains(personString))
					if (!childParentRelations.getOrDefault(personString, new HashSet<String>())
							.contains(parentString)) {
						Set<String> existingParents = childParentRelations.getOrDefault(personString,
								new HashSet<String>());
						existingParents.add(parentString);
						childParentRelations.put(personString, existingParents);
					}
			}
		}
		// Create unique parent- and child-placeholders if a node has more than one
		// parent it has to be moved to
		for (String currentSibling : childParentRelations.keySet()) {
			// Parent->Node/Sibling relations
			if (siblingIds.contains(currentSibling) && childParentRelations.get(currentSibling).size() > 1) {
				Set<String> existingParentIds = null;
				for (Set<String> currentSet : uniqueParentPlaceholders)
					// A placeholder already exists for the permutation of parents, add to existing
					// one
					if (currentSet.containsAll(childParentRelations.get(currentSibling))
							&& currentSet.size() == childParentRelations.get(currentSibling).size())
						existingParentIds = currentSet;
				// Add new placeholder to list if it doesn't exist yet
				if (existingParentIds == null) {
					uniqueParentPlaceholders.add(childParentRelations.get(currentSibling));
					JSONObject newPlaceholderNode = getPlaceHolderNode();
					JSONArray parentIds = new JSONArray();
					childParentRelations.get(currentSibling).forEach(currentParentId -> parentIds.put(currentParentId));
					newPlaceholderNode.put("parentIds", parentIds);
					uniqueParentPlaceholderNodes.add(newPlaceholderNode);
				}

			}
			// Parent->selected node relation
			else if (currentSibling.equals(genID)) {
				Set<String> existingNodeLevelParentIds = null;
				for (Set<String> currentSet : uniqueParentPlaceholders)
					if (currentSet.containsAll(childParentRelations.get(currentSibling))
							&& currentSet.size() == childParentRelations.get(currentSibling).size())
						existingNodeLevelParentIds = currentSet;
				if (existingNodeLevelParentIds == null) {
					uniqueParentPlaceholders.add(childParentRelations.get(currentSibling));
					JSONObject newPlaceholderNode = getPlaceHolderNode();
					JSONArray parentIds = new JSONArray();
					childParentRelations.get(currentSibling).forEach(currentParentId -> parentIds.put(currentParentId));
					newPlaceholderNode.put("parentIds", parentIds);
					uniqueParentPlaceholderNodes.add(newPlaceholderNode);
				}
			}
			// Node/spouse->children relations
			else if (childIds.contains(currentSibling) && childParentRelations.get(currentSibling).size() > 1) {
				Set<String> existingNodeLevelParentIds = null;
				for (Set<String> currentSet : uniqueChildPlaceholders)
					if (currentSet.containsAll(childParentRelations.get(currentSibling))
							&& currentSet.size() == childParentRelations.get(currentSibling).size())
						existingNodeLevelParentIds = currentSet;
				if (existingNodeLevelParentIds == null) {
					uniqueChildPlaceholders.add(childParentRelations.get(currentSibling));
					JSONObject newPlaceholderNode = getPlaceHolderNode();
					JSONArray parentIds = new JSONArray();
					childParentRelations.get(currentSibling).forEach(currentParentId -> parentIds.put(currentParentId));
					newPlaceholderNode.put("parentIds", parentIds);
					uniqueChildPlaceholderNodes.add(newPlaceholderNode);
				}
			}

		}
// ################################################
// Prepare id to genID map, top down
// ################################################

		Map<String, Integer> genIdToIdMap = new HashMap<>();

		// No genId, set in advance
		setNodeID(rootNode);
		// Use parent-layer
		if (parents.length() > 0) {
			genIdToIdMap.put(parents.getJSONObject(0).getString("genID"), getIdCounter());
			setIdCounter(getIdCounter() + 1);
			if (parents.length() > 1) {
				// Set ids of all placeholders
				for (JSONObject currentParentPlaceholder : uniqueParentPlaceholderNodes)
					setNodeID(currentParentPlaceholder);
				// Set ids of individual parents
				for (int i = 1; i < parents.length(); i++) {
					genIdToIdMap.put(parents.getJSONObject(i).getString("genID"), getIdCounter());
					setIdCounter(getIdCounter() + 1);
				}

				// Check for relation with single parents
				for (String currentSibling : childParentRelations.keySet())
					// Direct child of parent instead of placement in placeholder
					if (childParentRelations.get(currentSibling).size() == 1) {
						genIdToIdMap.put(currentSibling, getIdCounter());
						setIdCounter(getIdCounter() + 1);
					}
			}
		}
		// Nodes that have multiple parents in the parent layer
		genIdToIdMap.put(person.getString("genID"), getIdCounter());
		setIdCounter(getIdCounter() + 1);
		if (spouses.length() > 0) {
			Set<String> nodeSpouses = new HashSet<>();
			for (int i = 0; i < spouses.length(); i++) {
				nodeSpouses.add(spouses.getJSONObject(i).getString("genID"));
			}
			spouseToSpouseRelations.put(person.getString("genID"), nodeSpouses);

			if (children.length() > 0) {
				for (JSONObject currentChildPlaceholder : uniqueChildPlaceholderNodes)
					setNodeID(currentChildPlaceholder);
			}
		}
		for (int i = 0; i < spouses.length(); i++) {
			genIdToIdMap.put(spouses.getJSONObject(i).getString("genID"), getIdCounter());
			setIdCounter(getIdCounter() + 1);
		}
		// Check for relation with multiple parents
		for (String currentSibling : childParentRelations.keySet())
			// Direct child of parent instead of placement in placeholder
			if (childParentRelations.get(currentSibling).size() > 1) {
				genIdToIdMap.put(currentSibling, getIdCounter());
				setIdCounter(getIdCounter() + 1);
			}
		// Add ids for siblings that have shared parents with person that were not
		// fetched (person has more than two parents)
		for (String currentSibling : siblingIds)
			if (childParentRelations.getOrDefault(currentSibling, new HashSet<String>()).isEmpty()) {
				genIdToIdMap.put(currentSibling, getIdCounter());
				setIdCounter(getIdCounter() + 1);
			}

		for (int i = 0; i < children.length(); i++) {
			genIdToIdMap.put(children.getJSONObject(i).getString("genID"), getIdCounter());
			setIdCounter(getIdCounter() + 1);
		}

		// ################################################
		// Put everything together, bottom up
		// ################################################

		// 1. Put each child in their respective placeholder
		for (JSONObject currentPlaceholder : uniqueChildPlaceholderNodes) {
			JSONArray placeholderChildren = new JSONArray();
			Set<String> parentIds = new HashSet<>();
			currentPlaceholder.getJSONArray("parentIds").forEach(currentId -> parentIds.add((String) currentId));
			for (int i = 0; i < children.length(); i++) {
				Set<String> matchingIds = childParentRelations
						.getOrDefault(children.getJSONObject(i).getString("genID"), new HashSet<>());
				if (matchingIds.containsAll(parentIds) && matchingIds.size() == parentIds.size())
					placeholderChildren.put(children.getJSONObject(i));
			}
			currentPlaceholder.put("children", placeholderChildren);
		}

		// 2. Children with only one parent
		singleParentLoop: for (String currentChild : childParentRelations.keySet()) {
			if (childParentRelations.get(currentChild).size() == 1) {
				String parentId = childParentRelations.get(currentChild).iterator().next();
				JSONObject child = null;
				for (int j = 0; j < children.length(); j++)
					if (children.getJSONObject(j).getString("genID").equals(currentChild)) {
						child = children.getJSONObject(j);
						break;
					}

				if (person.getString("genID").equals(parentId)) {
					JSONArray parentChildren = person.optJSONArray("children") != null ? person.getJSONArray("children")
							: new JSONArray();
					parentChildren.put(child);
					child.put("children", parentChildren);
					continue singleParentLoop;

				}
				for (int i = 0; i < spouses.length(); i++)
					if (spouses.getJSONObject(i).getString("genID").equals(parentId)) {
						JSONArray parentChildren = spouses.getJSONObject(i).optJSONArray("children") != null
								? spouses.getJSONObject(i).getJSONArray("children")
								: new JSONArray();
						parentChildren.put(child);
						spouses.getJSONObject(i).put("children", parentChildren);
						continue singleParentLoop;
					}
			}

		}
		// 3. More than one parent: Put siblings, the selected search result, spouses in
		// the correct placeholder
		for (JSONObject currentPlaceholder : uniqueParentPlaceholderNodes) {
			JSONArray placeholderChildren = new JSONArray();
			Set<String> uniqueIds = new HashSet<>();
			currentPlaceholder.getJSONArray("parentIds").forEach(currentId -> uniqueIds.add((String) currentId));
			// 3A Siblings in ParentPlaceHolder
			for (String currentSibling : childParentRelations.keySet())
				if (childParentRelations.get(currentSibling).containsAll(uniqueIds)
						&& childParentRelations.get(currentSibling).size() == uniqueIds.size()) {
					JSONObject matchingSibling = null;
					for (int i = 0; i < siblings.length(); i++)
						if (siblings.getJSONObject(i).getString("genID").equals(currentSibling)) {
							matchingSibling = siblings.getJSONObject(i);
							break;
						}
					if (matchingSibling != null)
						placeholderChildren.put(matchingSibling);
				}

			// 3B Add person to PPH
			if (uniqueIds.size() == parents.length()) {
				placeholderChildren.put(person);
				// 3C ChildPlaceHolders to PPH
				for (JSONObject currentChildPlaceholder : uniqueChildPlaceholderNodes)
					placeholderChildren.put(currentChildPlaceholder);
				// 3D and spouses to PPH
				for (int i = 0; i < spouses.length(); i++) {
					// Otherwise a connection to the invisible root node is drawn <3
					spouses.getJSONObject(i).put("no_parent", true);
					placeholderChildren.put(spouses.getJSONObject(i));}
			}
			if (placeholderChildren.length() > 0)
				currentPlaceholder.put("children", placeholderChildren);
		}
		// 4A Put siblings with one parent to its children list
		for (int i = 0; i < parents.length(); i++) {
			String parentId = parents.getJSONObject(i).getString("genID");
			JSONArray parentChildren = new JSONArray();
			for (String currentSibling : childParentRelations.keySet()) {
				if (childParentRelations.get(currentSibling).contains(parentId)
						&& childParentRelations.get(currentSibling).size() == 1) {
					JSONObject matchingSibling = null;
					for (int j = 0; j < siblings.length(); j++)
						if (siblings.getJSONObject(j).getString("genID").equals(currentSibling)) {
							matchingSibling = siblings.getJSONObject(j);
							break;
						}
					if (matchingSibling != null)
						parentChildren.put(matchingSibling);
				}
			}
			if (parentChildren.length() > 0)
				parents.getJSONObject(i).put("children", parentChildren);
		}
		// 4B put person, ChildParentPlaceholders and spouses in single parent if not
		// more
		if (parents.length() == 1) {
			JSONArray parentChildren = new JSONArray();
			parentChildren.put(person);
			// 4C
			for (int i = 0; i < uniqueChildPlaceholderNodes.size(); i++)
				parentChildren.put(uniqueChildPlaceholderNodes.get(i));
			// 4D
			for (int i = 0; i < spouses.length(); i++) {
				// Otherwise a connection to the invisible root node is drawn <3
				spouses.getJSONObject(i).put("no_parent", true);
				parentChildren.put(spouses.getJSONObject(i));
			}
		}

		// 5A Put parent level nodes in root
		if (parents.length() > 0) {

			for (int i = 0; i < parents.length(); i++) {
				if (i == 1 && parents.length() > 1 && uniqueParentPlaceholderNodes.size() > 0)
					for (JSONObject currentPlaceholder : uniqueParentPlaceholderNodes)
						rootLayerChildren.put(currentPlaceholder);
				// Otherwise a connection to the invisible root node is drawn <3
				parents.getJSONObject(i).put("no_parent", true);
				rootLayerChildren.put(parents.getJSONObject(i));
			}
			rootNode.put("children", rootLayerChildren);
		}
		// 5B Put person level nodes in root
		else {
			// Otherwise a connection to the invisible root node is drawn <3
			person.put("no_parent", true);
			rootLayerChildren.put(person);
			if (spouses.length() > 0 && children.length() > 0)
				for (JSONObject currentPlaceholder : uniqueChildPlaceholderNodes)
					rootLayerChildren.put(currentPlaceholder);
			for (int i = 0; i < spouses.length(); i++) {
				// Otherwise a connection to the invisible root node is drawn <3
				spouses.getJSONObject(i).put("no_parent", true);
				rootLayerChildren.put(spouses.getJSONObject(i));
			}
			rootNode.put("children", rootLayerChildren);
		}
		// Fetch id to genID map, create spouseRelations and childRelations based on it
		// Replace each node with a version that has an integer id field
		for (String currentKey : genIdToIdMap.keySet())
			rootNode = addIdFieldToNode(rootNode, currentKey, genIdToIdMap.get(currentKey));

		// Create spouse- and child-relation entries
		for (int i = 0; i < parents.length(); i++) {
			JSONObject newRelation = new JSONObject();
			newRelation.put("source", genIdToIdMap.get(parents.getJSONObject(i).getString("genID")));
			newRelation.put("target", genIdToIdMap.get(person.getString("genID")));
			childRelations.put(newRelation);
		}

		for (String currentChild : childParentRelations.keySet())
			for (String currentParent : childParentRelations.getOrDefault(currentChild, new HashSet<String>())) {
				JSONObject newRelation = new JSONObject();
				newRelation.put("source", genIdToIdMap.get(currentParent));
				newRelation.put("target", genIdToIdMap.get(currentChild));
				childRelations.put(newRelation);
			}
		for (String spouse1 : spouseToSpouseRelations.keySet())
			for (String spouse2 : spouseToSpouseRelations.getOrDefault(spouse1, new HashSet<String>())) {
				JSONObject newRelation = new JSONObject();
				newRelation.put("source", genIdToIdMap.get(spouse1));
				newRelation.put("target", genIdToIdMap.get(spouse2));
				spouseRelations.put(newRelation);
			}
		rootNode.put("spouses", spouseRelations);
		rootNode.put("childRelations", childRelations);
		rootNode = addCountInformation(session, getGenIdList(rootNode.toString()), rootNode);
		return rootNode;
	}

	protected JSONObject addCountInformation(Session session, String[] genIDList, JSONObject tree) {
		JSONArray countInformation = getCountInformationByGenId(session, genIDList);
		// Add count information to tree
		for (int i = 0; i < countInformation.length(); i++) {
			JSONObject currentNode = getNodeByGenID(countInformation.getJSONObject(i).getString("genID"), tree);
			for (String currentKey : countInformation.getJSONObject(i).keySet()) {
				if (currentKey.equals("genID"))
					continue;
				currentNode.put(currentKey, countInformation.getJSONObject(i).get(currentKey));
			}
			tree = replaceTreeNode(tree, currentNode);
		}
		return tree;
	}

	protected JSONObject getNodeByGenID(String genID, JSONObject node) {
		// JSONPath changes order of JSONObject key/value contained in JSONArrays
		// destroys possibility for easy JSON replacement<3
//		List<LinkedHashMap> pathResult = JsonPath.parse(node.toString()).read("$..[?(@.genID == \'" + genID + "\')]");
//		if (!pathResult.isEmpty())
//		return mapToJSONObject(pathResult.get(0));

		// Necessary as node replacement isn't possible otherwise

		if (node.optJSONArray("children") != null)
			for (int i = 0; i < node.optJSONArray("children").length(); i++) {
				if (node.optJSONArray("children").getJSONObject(i).optString("genID").equals(genID))
					return node.optJSONArray("children").getJSONObject(i);
			}

		JSONArray children = node.optJSONArray("children");
		JSONObject res = null;
		if (children != null)
			for (int i = 0; res == null && i < children.length(); i++) {
				res = getNodeByGenID(genID, children.getJSONObject(i));
			}
		return res;
	}

	protected JSONObject getNodeById(Integer id, JSONObject node) {
//		List<LinkedHashMap> pathResult = JsonPath.parse(node.toString()).read("$..[?(@.id == \'" + id + "\')]");
//		if (!pathResult.isEmpty())
//		// Necessary as node replacement isn't possible otherwise
//		return mapToJSONObject(pathResult.get(0));

		if (node.optJSONArray("children") != null)
			for (int i = 0; i < node.optJSONArray("children").length(); i++) {
				if (node.optJSONArray("children").getJSONObject(i).optInt("id") == id)
					return node.optJSONArray("children").getJSONObject(i);
			}

		JSONArray children = node.optJSONArray("children");
		JSONObject res = null;
		if (children != null)
			for (int i = 0; res == null && i < children.length(); i++) {
				res = getNodeById(id, children.getJSONObject(i));
			}
		return res;
	}

	protected JSONObject replaceTreeNode(JSONObject tree, JSONObject node, String... genID) {
		String jsonString = tree.toString();
		List<LinkedHashMap> originalNode = null;
		if (genID != null && genID.length == 1)
			originalNode = JsonPath.parse(jsonString).read("$..[?(@.genID == \'" + genID[0] + "\')]");
		else
			originalNode = JsonPath.parse(jsonString).read("$..[?(@.id == " + node.getInt("id") + ")]");
		String nodeResult = mapToJSONObject(originalNode.get(0)).toString();
		String nodeReplacement = node.toString();
		if (!jsonString.contains(nodeResult))
			System.out.println("Replacement didn't work for: " + nodeResult);

		return new JSONObject(jsonString.replace(nodeResult, node != null ? nodeReplacement : ""));
	}

	protected JSONObject addIdFieldToNode(JSONObject tree, String genID, int id) {
		JSONObject matchingNode = getNodeByGenID(genID, tree);
		matchingNode.put("id", id);
		tree = replaceTreeNode(tree, matchingNode, genID);
		return tree;
	}

	/**
	 * Necessary as JSONPath only returns maps as result
	 */
	protected JSONObject mapToJSONObject(LinkedHashMap map) {
		JSONObject result = new JSONObject();
		for (Object currentKey : map.keySet())
			result.put((String) currentKey, map.get(currentKey));
		return result;
	}

	/**
	 * Get genIDs contained in the JSON string representation
	 */
	protected String[] getGenIdList(String jsonString) {
		List<String> genIdList = new ArrayList<>();
		List<Map> pathResult = JsonPath.parse(jsonString).read("$..[?(@.genID)]");
		for (int i = 0; i < pathResult.size(); i++)
			genIdList.add((String) pathResult.get(i).get("genID"));
		return genIdList.toArray(new String[genIdList.size()]);
	}

	/**
	 * Return list of genIDs of nodes other than the one selected by the user
	 */
	protected String[] getNonSelectedGenIdList(String childId, Map<Integer, String> idToGenIDMap,
			JSONArray childRelations, JSONObject requestJSON) {
		Set<String> nonSelectedList = new HashSet<>();
		// Get all genIDs from tree, remove selected node genID from it
		List<Map> pathResult = JsonPath.parse(requestJSON.toString()).read("$..[?(@.id > 0)]");
		for (int i = 0; i < pathResult.size(); i++) {
			Map currentResult = pathResult.get(i);
			if (currentResult.get("genID") != null && !((String) currentResult.get("genID")).equals(childId))
				nonSelectedList.add((String) currentResult.get("genID"));
		}
		return nonSelectedList.toArray(new String[nonSelectedList.size()]);
	}

	/**
	 * Return map of placeholder id to children genIDs that are affected by newly
	 * added parents
	 */
	protected Map<Integer, Set<String>> getDirtyParentsSiblingMap(Map<String, Set<String>> siblingParentRelations,
			List<String> additionalParentIds, Map<Integer, List<String>> placeholderChildMap) {
		// As soon as a child from the relations has one parent beyond the existing
		// placeholder add it to the per parent list of affected children
		Map<Integer, Set<String>> dirtyParentChildMap = new HashMap<>();

		for (String currentChild : siblingParentRelations.keySet())
			for (String currentParent : siblingParentRelations.get(currentChild))
				if (additionalParentIds.contains(currentParent)) {
					Integer placeholderId = 0;
					// One placeholder can contain multiple children per parent list, so only one
					// has to be changed per affected child
					for (List<String> currentPlaceHolderChildList : placeholderChildMap.values())
						if (currentPlaceHolderChildList.contains(currentChild))
							for (Integer currentPlaceholderId : placeholderChildMap.keySet())
								if (placeholderChildMap.get(currentPlaceholderId).equals(currentPlaceHolderChildList)) {
									placeholderId = currentPlaceholderId;
									break;
								}
					// Shouldn't be possible
					if (placeholderId == 0)
						continue;
					Set<String> outdatedChildrenEntries = dirtyParentChildMap.get(placeholderId);
					outdatedChildrenEntries = outdatedChildrenEntries != null ? outdatedChildrenEntries
							: new HashSet<String>();
					outdatedChildrenEntries.add(currentChild);
					dirtyParentChildMap.put(placeholderId, outdatedChildrenEntries);
				}
		return dirtyParentChildMap;
	}

	protected JSONObject getPersonByGenID(Session session, String genID) {
		String queryString = QueryConstants.fetchPersonByGenIDNoCountPrefix + genID + "\'"
				+ QueryConstants.fetchPersonByGenIDNoCountSuffix;
		JSONArray result = executeQuery(session, queryString);
		if (result.length() > 0)
			return result.getJSONObject(0);
		return null;
	}

	protected JSONArray getParents(Session session, String genID, String... existingParentIds) {
		String queryString = QueryConstants.fetchParentsPrefix + QueryConstants.fetchParentsInfix + genID + "\'";
		if (existingParentIds != null && existingParentIds.length > 0)
			for (String currentParentId : existingParentIds)
				queryString += QueryConstants.fetchParentsSuffix1 + currentParentId + "'";
		queryString += QueryConstants.fetchParentsSuffix2;
		return executeQuery(session, queryString);
	}

	protected JSONArray getParentSpouseRelationsOfChild(Session session, String childID) {
		String queryString = QueryConstants.fetchParentRelationsPrefix + QueryConstants.fetchParentsInfix + childID
				+ "\'" + QueryConstants.fetchParentRelationSuffix;
		return executeQuery(session, queryString);
	}

	protected JSONArray getParentChildRelationsOfChildren(Session session, String childID,
			String... existingSiblingIds) {
		String queryString = "";
		if (childID != null) {
			queryString = String.format(QueryConstants.fetchParentRelationsOfChildPrefix, childID);
			if (existingSiblingIds != null && existingSiblingIds.length > 0)
				for (int i = 0; i < existingSiblingIds.length; i++)
					queryString += String.format(QueryConstants.fetchParentRelationsOfChildInfix,
							existingSiblingIds[i]);

		} else if (existingSiblingIds != null && existingSiblingIds.length > 0) {
			queryString = String.format(QueryConstants.fetchParentRelationsOfChildPrefix, existingSiblingIds[0]);
			for (int i = 1; i < existingSiblingIds.length; i++)
				queryString += String.format(QueryConstants.fetchParentRelationsOfChildInfix, existingSiblingIds[i]);
		}
		queryString += QueryConstants.fetchParentRelationsOfChildSuffix;
		return executeQuery(session, queryString);
	}

	// TODO continue here fix usage
	// Example: OPTIONAL MATCH (person:Person)-[:CHILD_OF]->(parent1:Person) WHERE
	// parent1.genID='G000043' and person.genID='G000047' OPTIONAL MATCH
	// (parent2:Person)<-[:CHILD_OF]-(person:Person) where parent2.genID<>'G000043'
	// RETURN collect({person: person.genID, parent1: parent1.genID, parent2:
	// parent2.genID}) as childRelations
	protected JSONArray getChildRelationsOfParent(Session session, String parentID, String... existingsChildIds) {
		String queryString = QueryConstants.fetchChildRelationsPrefix + parentID + "\'";
		if (existingsChildIds != null && existingsChildIds.length > 0) {
			queryString += QueryConstants.fetchChildRelationsInfix1;
			for (int i = 0; i < existingsChildIds.length; i++) {
				queryString += QueryConstants.fetchChildRelationsInfix2 + existingsChildIds[i] + "\'";
				if (existingsChildIds.length > 1 && i < (existingsChildIds.length - 1))
					queryString += QueryConstants.fetchChildRelationsInfix3;
			}
			queryString += ")";
		}
		queryString += QueryConstants.fetchChildRelationsSuffix;
		return executeQuery(session, queryString);
	}

	protected JSONArray getCountInformationByGenId(Session session, String... genIDs) {
		JSONArray countResults = new JSONArray();
		for (String currentGenID : genIDs) {
			JSONObject currentObject = new JSONObject();
			currentObject.put("genID", currentGenID);
			// Necessary as zero counts are not returned as result
			currentObject.put("parentCount", 0);
			currentObject.put("siblingCount", 0);
			currentObject.put("spouseCount", 0);
			currentObject.put("childCount", 0);
			countResults.put(currentObject);
		}
		// Split up fetch the count information in separate queries to reduce time
		JSONArray parentCounts = getParentCount(session, genIDs);
		JSONArray siblingCounts = getSiblingCount(session, genIDs);
		JSONArray spouseCounts = getSpouseCount(session, genIDs);
		JSONArray childCounts = getChildCount(session, genIDs);

		countResults = replaceCountValues(countResults, parentCounts, "parentCount");
		countResults = replaceCountValues(countResults, siblingCounts, "siblingCount");
		countResults = replaceCountValues(countResults, spouseCounts, "spouseCount");
		countResults = replaceCountValues(countResults, childCounts, "childCount");

		return countResults;
	}

	private JSONArray replaceCountValues(JSONArray result, JSONArray values, String key) {
		Collection<Node> countList = (Collection<Node>) values.getJSONObject(0).get(key);
		Iterator iterator = countList.iterator();
		while (iterator.hasNext()) {
			Map<String, Object> currentCountResult = (Map<String, Object>) iterator.next();
			for (int j = 0; j < result.length(); j++)
				if (result.getJSONObject(j).getString("genID").equals((String) currentCountResult.get("genID"))) {
					result.getJSONObject(j).put(key, (Long) currentCountResult.get(key));
					break;
				}
		}
		return result;
	}

	private JSONArray getParentCount(Session session, String... genID) {
		String queryString = QueryConstants.fetchParentsPrefix + QueryConstants.fetchParentCountInfix
				+ QueryConstants.fetchParentsInfix + genID[0] + "\'";
		for (int i = 1; i < genID.length; i++)
			queryString += " OR child.genID=\'" + genID[i] + "\'";
		queryString += QueryConstants.fetchParentCountSuffix;
		return executeQuery(session, queryString);
	}

	/**
	 * Remove redundant parent relations from query results
	 */
	protected JSONArray removeRedundantParentRelations(JSONArray redundantRelations, Map<Integer, String> idGenIDMap,
			Map<String, Integer> genIDIdMap) {
		Collection<Node> list = (Collection<Node>) ((JSONObject) redundantRelations.get(0)).get("parentRelations");
		JSONArray cleanedRelations = new JSONArray();
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			Map<String, String> map = new HashMap<String, String>();
			map = (Map<String, String>) iterator.next();
			String spouse1 = map.get("spouse1");
			String spouse2 = map.get("spouse2");
			if (idGenIDMap.values().contains(spouse1) && idGenIDMap.values().contains(spouse2)) {
				boolean relationExists = false;
				for (int i = 0; i < cleanedRelations.length(); i++) {
					if ((cleanedRelations.getJSONObject(i).getInt("source") == genIDIdMap.get(spouse1)
							&& cleanedRelations.getJSONObject(i).getInt("target") == genIDIdMap.get(spouse2))
							|| (cleanedRelations.getJSONObject(i).getInt("source") == genIDIdMap.get(spouse2)
									&& cleanedRelations.getJSONObject(i).getInt("target") == genIDIdMap.get(spouse1))) {
						relationExists = true;
						break;
					}
				}
				if (!relationExists) {
					JSONObject newRelation = new JSONObject();
					newRelation.put("source", genIDIdMap.get(spouse1));
					newRelation.put("target", genIDIdMap.get(spouse2));
					cleanedRelations.put(newRelation);
				}
			}
		}
		return cleanedRelations;
	}

	protected JSONArray removeChild(JSONArray children, int childId) {
		for (int i = 0; i < children.length(); i++)
			if (children.getJSONObject(i).optInt("id", 0) == (childId)) {
				children.remove(i);
				break;
			}
		return children;
	}

	/**
	 * Remove redundant parent relations from query results
	 */
	protected Map<String, Set<String>> cleanChildRelations(JSONArray redundantRelations,
			Map<String, Integer> genIDToIdMap) {
		Map<String, Set<String>> childRelations = new HashMap<>();
		Collection<Node> list = (Collection<Node>) ((JSONObject) redundantRelations.get(0)).get("childRelations");
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			Map<String, String> map = new HashMap<String, String>();
			map = (Map<String, String>) iterator.next();
			String person = map.get("person");
			// Results from getParentChildRelationsOfChildren
			String parent = map.get("parent");
			// Results from getChildRelationsOfParent
			String parent1 = map.get("parent1");
			String parent2 = map.get("parent2");
			if (person != null) {
				Set<String> currentParentSet = childRelations.get(person);
				currentParentSet = currentParentSet == null ? new HashSet<>() : currentParentSet;
				if (parent != null) {
					currentParentSet.add(parent);
					childRelations.put(person, currentParentSet);
				}
				if (parent1 != null) {
					currentParentSet.add(parent1);
					childRelations.put(person, currentParentSet);
				}
				if (parent2 != null && genIDToIdMap.keySet().contains(parent2)) {
					currentParentSet.add(parent2);
					childRelations.put(person, currentParentSet);
				}
			}

		}
		return childRelations;
	}

	/**
	 * Remove redundant parent relations from query results and return a Map from
	 * children to their parents
	 * 
	 * @param childRelations Existing relations to exclude from the results
	 */
	protected Map<String, Set<String>> cleanParentRelationsOfChild(JSONArray redundantRelations,
			Map<Integer, String> idToGenIDToMap, JSONArray childRelations, String... filterParentIDs) {
		List<String> parentIdList = Arrays.asList(filterParentIDs);
		Map<String, Set<String>> childRelationResults = new HashMap<>();
		Collection<Node> list = (Collection<Node>) ((JSONObject) redundantRelations.get(0)).get("childRelations");
		Iterator iterator = list.iterator();
		newRelationLoop: while (iterator.hasNext()) {
			Map<String, String> map = new HashMap<String, String>();
			map = (Map<String, String>) iterator.next();
			String person = map.get("person");
			String parent = map.get("parent");
			if (parent != null && idToGenIDToMap.containsValue(parent)) {
				for (int i = 0; i < childRelations.length(); i++) {
					if (idToGenIDToMap.get(childRelations.getJSONObject(i).getInt("source")).equals(parent)
							&& idToGenIDToMap.get(childRelations.getJSONObject(i).getInt("target")).equals(person))
						continue newRelationLoop;

				}
				if (filterParentIDs.length > 0) {
					if (parentIdList.contains(parent)) {
						Set<String> currentParentSet = childRelationResults.get(person);
						currentParentSet = currentParentSet == null ? new HashSet<>() : currentParentSet;
						currentParentSet.add(parent);
						childRelationResults.put(person, currentParentSet);
					}
				} else {
					Set<String> currentParentSet = childRelationResults.get(person);
					currentParentSet = currentParentSet == null ? new HashSet<>() : currentParentSet;
					currentParentSet.add(parent);
					childRelationResults.put(person, currentParentSet);
				}
			}

		}

		return childRelationResults;
	}

	protected Map<String, Set<String>> cleanSpouseRelations(JSONArray redundantRelations,
			Map<String, Integer> genIDToIdMap, String... filterParentIds) {
		List<String> parentIdList = Arrays.asList(filterParentIds);
		Map<String, Set<String>> spouseRelations = new HashMap<>();
		Collection<Node> list = (Collection<Node>) ((JSONObject) redundantRelations.get(0)).get("spouseRelations");
		Iterator iterator = list.iterator();
		while (iterator.hasNext()) {
			Map<String, String> map = new HashMap<String, String>();
			map = (Map<String, String>) iterator.next();
			String spouse1 = map.get("spouse1");
			String spouse2 = map.get("spouse2");
			if (spouse1 != null && spouse2 != null && genIDToIdMap.containsKey(spouse1)
					&& genIDToIdMap.containsKey(spouse2)) {
				if (filterParentIds.length > 0) {
					if (parentIdList.contains(spouse2)) {
						Set<String> currentSpouseSet = spouseRelations.get(spouse1);
						currentSpouseSet = currentSpouseSet == null ? new HashSet<>() : currentSpouseSet;
						currentSpouseSet.add(spouse2);
						spouseRelations.put(spouse1, currentSpouseSet);
					}

				} else {
					Set<String> currentSpouseSet = spouseRelations.get(spouse1);
					currentSpouseSet = currentSpouseSet == null ? new HashSet<>() : currentSpouseSet;
					currentSpouseSet.add(spouse2);
					spouseRelations.put(spouse1, currentSpouseSet);
				}

			}
		}
		return spouseRelations;
	}

	protected JSONArray getChildren(Session session, String parentID, String[] spouseIDs,
			String... existingChildrenID) {
		String queryString = QueryConstants.fetchChildrenPrefix;
		if (spouseIDs != null && spouseIDs.length > 0)
			for (int i = 0; i < spouseIDs.length; i++)
				queryString += String.format(QueryConstants.fetchChildrenDynamicPrefix, i + 2);

		queryString += QueryConstants.fetchChildrenInfix1 + parentID + "'";
		if (spouseIDs != null && spouseIDs.length > 0)
			for (int i = 0; i < spouseIDs.length; i++)
				queryString += String.format(QueryConstants.fetchChildrenDynamicInfix, i + 2) + spouseIDs[i] + "'";

		if (existingChildrenID.length > 0)
			for (String existingChildID : existingChildrenID)
				queryString += QueryConstants.fetchChildrenInfix3 + existingChildID + "\'";

		queryString += QueryConstants.fetchChildrenSuffix;
		return executeQuery(session, queryString);
	}

	protected JSONArray getChildCount(Session session, String... genID) {
		String queryString = QueryConstants.fetchChildrenPrefix + QueryConstants.fetchChildrenCountInfix
				+ QueryConstants.fetchChildrenInfix1 + genID[0] + "\'";
		for (int i = 1; i < genID.length; i++)
			queryString += " OR parent1.genID=\'" + genID[i] + "\'";
		queryString += QueryConstants.fetchChildrenCountSuffix;
		return executeQuery(session, queryString);
	}

	protected JSONArray getSiblings(Session session, String genID, String[] existingParentID,
			String... existingSiblingsID) {
		return getSiblings(session, genID, existingParentID, false, existingSiblingsID);

	}

	/**
	 * Fetch siblings that are either all from the same parents or depend on a
	 * common sibling
	 */
	protected JSONArray getSiblings(Session session, String genID, String[] existingParentID,
			boolean strictParentRelations, String... existingSiblingsID) {
		String queryString = "";
		// Case 1: Fetch siblings for initial tree, forced parent relations
		if (strictParentRelations) {
			// Only fetch siblings for a maximum of two parents
			queryString = QueryConstants.fetchSiblingsParentDependentPrefix;
			if (existingParentID != null && existingParentID.length > 0) {
				// TODO add options for 1 and 3+ parents
				String formatString = QueryConstants.fetchSiblingsParentDependentInfix1;
				for (int i = 1; i < existingParentID.length; i++)
					formatString += ",\'%s\'";
				formatString += "]";
				queryString += String.format(formatString, (Object[]) existingParentID);
				queryString += QueryConstants.fetchSiblingsParentDependentInfix2;
				queryString = String.format(queryString, genID);
			} else {
				// Invalid method call
				return null;
			}
		}

		else {
			// Case 2: Get siblings of a child, optionally ignore existing siblings
			queryString = QueryConstants.fetchSiblingsPrefix;

//			if (existingParentID != null && existingParentID.length > 0) {
//				for (int i = 1; i < existingParentID.length; i++)
//					queryString += String.format(QueryConstants.fetchSiblingsDynamicPrefix, i + 1);
//			}
			queryString += String.format(QueryConstants.fetchSiblingsEndPrefix, genID);
//			if (existingParentID != null && existingParentID.length > 0) {
//				for (int i = 0; i < existingParentID.length; i++)
//					queryString += String.format(QueryConstants.fetchSiblingsDynamicInfix, i + 1, existingParentID[i]);
//			}
			if (existingSiblingsID.length > 0) {
				queryString += String.format(QueryConstants.fetchSiblingsInfix, existingSiblingsID[0]);
				for (int i = 1; i < existingSiblingsID.length; i++) {
					queryString += String.format(QueryConstants.fetchSiblingsInfix, existingSiblingsID[i]);
				}
				queryString += "]";
			}
		}
		queryString += QueryConstants.fetchSiblingsSuffix;
		return executeQuery(session, queryString);
	}

	protected JSONArray getSiblingCount(Session session, String... genID) {
		String queryString = QueryConstants.fetchSiblingsPrefix + QueryConstants.fetchSiblingsCountInfix
				+ String.format(QueryConstants.fetchSiblingsEndPrefix, genID[0]);
		for (int i = 1; i < genID.length; i++)
			queryString += " OR child.genID=\'" + genID[i] + "\'";
		queryString += QueryConstants.fetchSiblingsCountSuffix;
		return executeQuery(session, queryString);
	}

	protected JSONArray getSpouses(Session session, String genID, String... existingSpouseID) {
		String queryString = QueryConstants.fetchSpousesPrefix + QueryConstants.fetchSpousesInfix1 + genID + "\'";
		if (existingSpouseID.length > 0)
			for (String existingID : existingSpouseID)
				queryString += QueryConstants.fetchSpousesInfix2 + existingID + "\'";
		queryString += QueryConstants.fetchSpousesSuffix;
		return executeQuery(session, queryString);
	}

	protected JSONArray getSpouseIds(Session session, String genID) {
		String queryString = QueryConstants.fetchSpousesPrefix + genID + "\'";
		queryString += QueryConstants.fetchSpouseIdsSuffix;
		return executeQuery(session, queryString);
	}

	protected JSONArray getSpouseCount(Session session, String... genID) {
		String queryString = QueryConstants.fetchSpousesPrefix + QueryConstants.fetchSpousesCountInfix
				+ QueryConstants.fetchSpousesInfix1 + genID[0] + "\'";
		for (int i = 1; i < genID.length; i++)
			queryString += " OR person.genID=\'" + genID[i] + "\'";
		queryString += QueryConstants.fetchSpousesCountSuffix;
		return executeQuery(session, queryString);
	}

	protected JSONArray getAliasesByGenID(Session session, String genID) {
		String queryString = QueryConstants.fetchAliasesByGenIDPrefix + genID + "\'"
				+ QueryConstants.fetchAliasesByGenIDSuffix;
		return executeQuery(session, queryString);
	}

	public JSONArray executeQuery(Session session, final String string) {
		final JSONArray results = new JSONArray();
		if (session == null)
			return results;
		session.readTransaction(new TransactionWork<Boolean>() {
			@Override
			public Boolean execute(Transaction transaction) {
				StatementResult statementResult = transaction.run(string);
				while (statementResult.hasNext()) {

					Record queryResults = statementResult.next();
					Map resultMap = queryResults.asMap();
					// Either collection of node in collection or long number
					if (resultMap.get("person") != null) {
						if (resultMap.get("person") instanceof Collection) {
							Collection<Node> unchangedDBResults = (Collection<Node>) resultMap.get("person");
							if (resultMap.get("resultCount") != null) {
								Long count = (Long) resultMap.get("resultCount");
								JSONObject resultCount = new JSONObject();
								resultCount.put("resultCount", count);
								results.put(resultCount);
							}

							for (Node currentNode : unchangedDBResults) {
								JSONObject currentRecord = new JSONObject();
								// Remove unnecessary double quotes, otherwise results have to be changed in
								// frontend
								for (String currentKey : currentNode.keys()) {
									currentRecord.put((String) currentKey,
											currentNode.get(currentKey).toString().replace("\"", ""));
								}

								if (currentRecord.keySet().size() > 0)
									results.put(currentRecord);
							}
						} else {
							// Custom node result to include spouse- child- and sibling-count
							// Only case for getPersonByGenID?
							Map result = null;
							if (resultMap.get("person") instanceof Node) {
								Node singleNode = (Node) resultMap.get("person");
								result = singleNode.asMap();
							} else
								result = (Map) resultMap.get("person");
							JSONObject currentRecord = new JSONObject();
							Iterator keyIterator = result.keySet().iterator();
							while (keyIterator.hasNext()) {
								String nextKey = (String) keyIterator.next();
								currentRecord.put(nextKey, result.get(nextKey));
							}

							if (currentRecord.keySet().size() > 0)
								results.put(currentRecord);
						}

					} else {
						if (resultMap.get("childCount") != null) {
							JSONObject childCount = new JSONObject();
							childCount.put("childCount", resultMap.get("childCount"));
							results.put(childCount);
						}
						if (resultMap.get("spouseCount") != null) {
							JSONObject spouseCount = new JSONObject();
							spouseCount.put("spouseCount", resultMap.get("spouseCount"));
							results.put(spouseCount);
						}
						if (resultMap.get("siblingCount") != null) {
							JSONObject siblingCount = new JSONObject();
							siblingCount.put("siblingCount", resultMap.get("siblingCount"));
							results.put(siblingCount);
						}
						if (resultMap.get("parentCount") != null) {
							JSONObject parentCount = new JSONObject();
							parentCount.put("parentCount", resultMap.get("parentCount"));
							results.put(parentCount);
						}
						if (resultMap.get("parentRelations") != null
								&& resultMap.get("parentRelations") instanceof Collection) {
							JSONObject parentRelations = new JSONObject();
							parentRelations.put("parentRelations", resultMap.get("parentRelations"));
							results.put(parentRelations);
						}
						if (resultMap.get("childRelations") != null
								&& resultMap.get("childRelations") instanceof Collection) {
							JSONObject childRelations = new JSONObject();
							childRelations.put("childRelations", resultMap.get("childRelations"));
							results.put(childRelations);
						}
						if (resultMap.get("spouseRelations") != null
								&& resultMap.get("spouseRelations") instanceof Collection) {
							JSONObject spouseRelations = new JSONObject();
							spouseRelations.put("spouseRelations", resultMap.get("spouseRelations"));
							results.put(spouseRelations);
						}
						if (resultMap.get("relationCounts") != null) {
							Map result = (Map) resultMap.get("relationCounts");
							JSONObject currentRecord = new JSONObject();
							Iterator keyIterator = result.keySet().iterator();
							while (keyIterator.hasNext()) {
								String nextKey = (String) keyIterator.next();
								currentRecord.put(nextKey, result.get(nextKey));
							}
							if (currentRecord.keySet().size() > 0)
								results.put(currentRecord);
						}
					}
				}
				return true;
			}
		});
		return results;

	}

	public JSONArray getSpouseRelations(Session session, String... spouseId) {
		String queryString = String.format(QueryConstants.fetchSpouseRelationsPrefix, spouseId[0]);
		if (spouseId.length > 1)
			for (int i = 1; i < spouseId.length; i++)
				queryString += String.format(QueryConstants.fetchSpouseRelationsInfix, spouseId[i]);
		queryString += QueryConstants.fetchSpouseRelationsSuffix;
		return executeQuery(session, queryString);
	}

	/**
	 * Get the list of children each placeholder contains
	 */
	public Map<Integer, List<String>> getPlaceholderChildMap(JSONObject json) {
		Map<Integer, List<String>> placeholderMap = new HashMap<>();
		List<LinkedHashMap> pathResult = JsonPath.parse(json.toString()).read("$..[?(@.parentIds)]");
		for (int i = 0; i < pathResult.size(); i++) {
			JSONObject currentPlaceholder = mapToJSONObject(pathResult.get(i));
			List<String> currentChildGenIDs = new ArrayList<>();
			if (currentPlaceholder.optJSONArray("children") != null) {
				JSONArray currentChildren = currentPlaceholder.getJSONArray("children");
				for (int j = 0; j < currentChildren.length(); j++)
					// If a placeholder node gets removed as all children were moved, the
					// placeholders need to be moved along the children it belongs to
					if (!currentChildren.getJSONObject(j).optString("genID").isEmpty())
						currentChildGenIDs.add(currentChildren.getJSONObject(j).getString("genID"));
			}
			placeholderMap.put(currentPlaceholder.getInt("id"), currentChildGenIDs);
		}
		return placeholderMap;
	}

	/**
	 * Get the list of parentIds each placeholder contains
	 */
	public Map<Integer, List<String>> getPlaceholderParentIdsMap(JSONObject json) {
		Map<Integer, List<String>> placeholderMap = new HashMap<>();
		List<LinkedHashMap> pathResult = JsonPath.parse(json.toString()).read("$..[?(@.parentIds)]");
		for (int i = 0; i < pathResult.size(); i++) {
			JSONObject currentPlaceholder = mapToJSONObject(pathResult.get(i));
			List<String> currentChildGenIDs = new ArrayList<>();
			if (currentPlaceholder.optJSONArray("children") != null) {
				JSONArray currentChildren = currentPlaceholder.getJSONArray("children");
				for (int j = 0; j < currentChildren.length(); j++)
					// If a placeholder node gets removed as all children were moved, the
					// placeholders need to be moved along the children it belongs to
					if (!currentChildren.getJSONObject(j).optString("genID").isEmpty())
						currentChildGenIDs.add(currentChildren.getJSONObject(j).getString("genID"));
			}
			placeholderMap.put(currentPlaceholder.getInt("id"), currentChildGenIDs);
		}
		return placeholderMap;
	}

	/**
	 * Return id of placeholder that contains all requests parentIds
	 */
	public int getPlaceholderByParentIds(Map<Integer, List<String>> placeholderParentIdMap, Set<String> newParentList) {
		int placeholderId = 0;
		Set<String> newParentIdSet = new HashSet<String>();
		newParentIdSet.addAll(newParentList);
		Iterator<Integer> iterator = placeholderParentIdMap.keySet().iterator();
		while (iterator.hasNext()) {
			int currentPlaceholderId = iterator.next();
			if (placeholderParentIdMap.get(currentPlaceholderId).containsAll(newParentIdSet)
					&& placeholderParentIdMap.get(currentPlaceholderId).size() == newParentIdSet.size()) {
				placeholderId = currentPlaceholderId;
				break;
			}
		}
		return placeholderId;
	}

	/**
	 * Get the list of placeholders that contain no children or its associated
	 * placeholder(s) due to changes in the tree structure
	 */
	public List<Integer> getSuperfluousPlaceholders(Map<Integer, List<String>> placeholderChildMap,
			Map<Integer, Set<String>> dirtyParentSiblingsMap) {
		Map<Integer, Integer> placeholderRemainingChidrenCount = new HashMap<>();
		List<Integer> superfluousList = new ArrayList<>();

		// Amount of children left
		for (Integer currentPlaceholderId : placeholderChildMap.keySet())
			placeholderRemainingChidrenCount.put(currentPlaceholderId,
					placeholderChildMap.get(currentPlaceholderId).size());

		// Reduce the count of remaining children for each occurrence of a moving child
		for (Integer currentDirtyParent : dirtyParentSiblingsMap.keySet())
			placeholderRemainingChidrenCount.put(currentDirtyParent,
					placeholderRemainingChidrenCount.get(currentDirtyParent)
							- dirtyParentSiblingsMap.get(currentDirtyParent).size());
		for (Integer currentDirtyParent : placeholderRemainingChidrenCount.keySet())
			if (placeholderRemainingChidrenCount.get(currentDirtyParent) < 1)
				superfluousList.add(currentDirtyParent);
		return superfluousList;
	}

	/**
	 * @param placeholderChildMap
	 * @return map of placeholder JSONObjects to the genIDs of siblings, that shall
	 *         be added to them
	 */
	public Map<JSONObject, Set<String>> getAdditionalPlaceholderSiblingMap(
			Map<Integer, List<String>> placeholderChildMap, List<Integer> placeholdersToRemove,
			Map<String, Set<String>> siblingParentRelations) {
		Map<JSONObject, Set<String>> updatedPlaceholderSiblingMap = new HashMap<>();
		// TODO continue here
		// Get set of children to add to new placeholders
		Set<String> freeChildren = new HashSet<>();
		for (Integer currentRemovedPlaceholder : placeholdersToRemove)
			freeChildren.addAll(placeholderChildMap.get(currentRemovedPlaceholder));

		// Create map of placeholder JSONObject to siblings that shall be added to it
		Map<Set<String>, Set<String>> parentGenIdSiblingGenIdMap = new HashMap<>();
		for (String currentFreeChild : freeChildren) {
			Set<String> keyResult = null;
			for (Set<String> currentParentSet : parentGenIdSiblingGenIdMap.keySet())
				// Put siblings to same placeholder if their parents' genIDs match
				if (currentParentSet.containsAll(siblingParentRelations.get(currentFreeChild))
						&& currentParentSet.size() == siblingParentRelations.get(currentFreeChild).size()) {
					keyResult = currentParentSet;
					break;
				}
			if (keyResult == null) {
				keyResult = new HashSet<>();
				keyResult.addAll(siblingParentRelations.get(currentFreeChild));
			}
			Set<String> siblings = parentGenIdSiblingGenIdMap.getOrDefault(keyResult, new HashSet<String>());
			siblings.add(currentFreeChild);
			parentGenIdSiblingGenIdMap.put(keyResult, siblings);
		}
		// Mapping is done, create placeholders based on required genIDs
		for (Set<String> parentIds : parentGenIdSiblingGenIdMap.keySet()) {
			JSONObject newPlaceholderNode = getPlaceHolderNode();
			// TODO Reuse freed placeholder ids or count up?
			setNodeID(newPlaceholderNode);
			JSONArray parentIdList = new JSONArray();
			for (String parentId : parentIds)
				parentIdList.put(parentId);
			newPlaceholderNode.put("parentIds", parentIds);
			updatedPlaceholderSiblingMap.put(newPlaceholderNode, parentGenIdSiblingGenIdMap.get(parentIds));
		}

		return updatedPlaceholderSiblingMap;
	}

	public Set<String> getParentIds(String childId, JSONObject requestJSON, Map<Integer, String> idToGenIDMap) {
		Set<String> parentGenIDList = new HashSet<>();
		JSONArray parentRelations = requestJSON.optJSONArray("childRelations");
		if (parentRelations != null) {
			for (int i = 0; i < parentRelations.length(); i++)
				if ((parentRelations.getJSONObject(i).optInt("target", 0) > 0)
						&& idToGenIDMap.get(parentRelations.getJSONObject(i).getInt("target")).equals(childId)) {
					parentGenIDList.add(idToGenIDMap.get(parentRelations.getJSONObject(i).getInt("source")));
				}
		}
		return parentGenIDList;
	}

	public JSONObject moveChildrenToNewNodes(String nodeId, JSONArray childRelations, Session session,
			JSONObject requestJSON, JSONObject node, JSONArray parents, Map<Integer, String> idToGenIdMap,
			Map<String, Integer> genIDToIdMap) {
		// Nodes additional to the selected one can be affected by additional parents
		String[] genIDList = getNonSelectedGenIdList(nodeId, idToGenIdMap, childRelations, requestJSON);

		String[] parentIdList = new String[parents.length()];
		for (int i = 0; i < parents.length(); i++)
			parentIdList[i] = parents.getJSONObject(i).getString("genID");

		JSONArray existingParentChildRelations = requestJSON.getJSONArray("childRelations");
		JSONArray existingSpouseRelations = requestJSON.getJSONArray("spouses");

		// Fetch relations that only affect other nodes and the newly added parents
		Map<String, Set<String>> siblingParentRelations = cleanParentRelationsOfChild(
				getParentChildRelationsOfChildren(session, nodeId, genIDList), idToGenIdMap, childRelations,
				parentIdList);
		Map<String, Set<String>> spouseParentRelations = cleanSpouseRelations(getSpouseRelations(session, genIDList),
				genIDToIdMap, parentIdList);
		// Iterate every relevant parent-sibling-relation and make changes
		for (String currentKey : siblingParentRelations.keySet()) {
			// Move node to new place holder or to existing one if it exists
			JSONObject currentParentNode = getParentNodeByGenId(currentKey, requestJSON);

			Map<Integer, List<String>> placeholderParentIdMap = getPlaceholderParentIdsMap(requestJSON);
			Map<Integer, List<String>> placeholderChildMap = getPlaceholderChildMap(currentParentNode);
			JSONObject currentChild = null;
			Set<String> newParentList = new HashSet<>();
			newParentList.addAll(siblingParentRelations.get(node.getString("genID")));
			for (int i = 0; i < existingParentChildRelations.length(); i++)
				if (idToGenIdMap.get(existingParentChildRelations.getJSONObject(i).getInt("target")).equals(currentKey))
					newParentList.add(idToGenIdMap.get(existingParentChildRelations.getJSONObject(i).getInt("source")));
			// Check for existing placeholder to move to
			int existingPlaceholderId = getPlaceholderByParentIds(placeholderParentIdMap, newParentList);
			if (newParentList.size() > 1) {
				// Only one parent, move child to new placeholder
				if (!currentParentNode.optString("genID").isEmpty()) {

					// 1. Replace parent node with version with a child less
					JSONObject newParentNode = currentParentNode;
					JSONArray children = currentParentNode.getJSONArray("children");
					for (int j = 0; j < children.length(); j++)
						if (children.getJSONObject(j).optString("genID").equals(currentKey)) {
							currentChild = children.getJSONObject(j);
							children.remove(j);
							break;
						}
					newParentNode.put("children", children);
					requestJSON = replaceTreeNode(requestJSON, newParentNode);
					if (existingPlaceholderId > 0) {
						// 2. Replace placeholder node with version that adds child
						JSONObject existingPlaceholder = getNodeById(existingPlaceholderId, requestJSON);
						JSONArray placeholderChildren = existingPlaceholder.optJSONArray("children") != null
								? existingPlaceholder.getJSONArray("children")
								: new JSONArray();
						placeholderChildren.put(currentChild);
						existingPlaceholder.put("children", placeholderChildren);
						requestJSON = replaceTreeNode(requestJSON, existingPlaceholder);
					} else {
						// Add new placeholder that contains child as sibling node to parent

						// 1. Create new placeholder, add child to children
						JSONObject newPlaceholder = getPlaceHolderNode();
						JSONArray newParentIds = new JSONArray();
						JSONArray newChildren = new JSONArray();
						newParentList.forEach(currentParentId -> newParentIds.put(currentParentId));
						setNodeID(newPlaceholder);
						newPlaceholder.put("parentIds", newParentIds);
						newChildren.put(currentChild);
						newPlaceholder.put("children", newChildren);
						// 2. Replace parent node with version with a child less
						JSONObject grandParentNode = getParentNodeById(currentParentNode.getInt("id"), requestJSON);
						// Should not be possible, as the parent has a genID and therefore is 2nd tree
						// level or below
						if (grandParentNode == null) {
							System.err.println("Error retrieving grandparent node of " + currentKey);
							return requestJSON;
							// Replace child directly in the JSON of the post request
						}
						// 3. Replace parent of parent with version that has placeholder as additional
						// child
						JSONArray grandParentChildren = grandParentNode.getJSONArray("children");
						grandParentChildren.put(newPlaceholder);
						grandParentNode.put("children", grandParentChildren);
						requestJSON = replaceTreeNode(requestJSON, grandParentNode);
					}
				}
				// Existing placeholder or root node, move to other placeholder or change
				// parentIds of existing one
				else {

					// Root node
					if (currentParentNode.optJSONArray("parentIds") == null) {

						JSONArray children = currentParentNode.getJSONArray("children");
						for (int j = 0; j < children.length(); j++)
							if (children.getJSONObject(j).optString("genID").equals(currentKey)) {
								currentChild = children.getJSONObject(j);
								children.remove(j);
								break;
							}
						replaceTreeNode(requestJSON, currentParentNode);

						// If no placeholder exists, create new placeholder at root, move child to it
						if (existingPlaceholderId > 0) {
							JSONObject existingPlaceholder = getNodeById(existingPlaceholderId, requestJSON);
							JSONArray existingChildren = existingPlaceholder.optJSONArray("children") != null
									? existingPlaceholder.getJSONArray("children")
									: new JSONArray();
							existingChildren.put(currentChild);
							existingPlaceholder.put("children", existingChildren);
							replaceTreeNode(requestJSON, existingPlaceholder);
						} else {

							JSONObject newPlaceholder = getPlaceHolderNode();
							JSONArray newParentIds = new JSONArray();
							JSONArray newChildren = new JSONArray();
							newParentList.forEach(currentParentId -> newParentIds.put(currentParentId));
							setNodeID(newPlaceholder);
							newChildren.put(currentChild);
							newPlaceholder.put("parentIds", newParentIds);
							newPlaceholder.put("children", newChildren);
							// TODO Place new placeholder as sibling to a new parent?
							JSONObject grandParentNode = getParentNodeByGenId(
									siblingParentRelations.get(currentKey).iterator().next(), requestJSON);
							if (grandParentNode == null) {
								System.err.println("Error retrieving parent node of "
										+ siblingParentRelations.get(currentKey).iterator().next());
								return requestJSON;
							}
							JSONArray grandParentChildren = grandParentNode.getJSONArray("children");
							grandParentChildren.put(newPlaceholder);
							replaceTreeNode(requestJSON, grandParentNode);
						}
					} else {
						// Change parentIds if only child left, move to existing placeholder or create
						// new one
						// Check for children that are left for the current placeholder
						JSONArray children = currentParentNode.getJSONArray("children");
						for (int j = 0; j < children.length(); j++)
							if (children.getJSONObject(j).optString("genID").equals(currentKey)) {
								currentChild = children.getJSONObject(j);
								children.remove(j);
								break;
							}
						if (placeholderChildMap.get(currentParentNode.getInt("id")).size() == 1) {
							// Remove placeholder as it has no children left, not root node as it has
							// parentIds
							replaceTreeNode(requestJSON, null);
						} else
							replaceTreeNode(requestJSON, currentParentNode);

						// If no placeholder exists, create new placeholder at root, move child to it
						if (existingPlaceholderId > 0) {
							JSONObject existingPlaceholder = getNodeById(existingPlaceholderId, requestJSON);
							JSONArray existingChildren = existingPlaceholder.optJSONArray("children") != null
									? existingPlaceholder.getJSONArray("children")
									: new JSONArray();
							existingChildren.put(currentChild);
							existingPlaceholder.put("children", existingChildren);
							replaceTreeNode(requestJSON, existingPlaceholder);
						} else {
							// Create new placeholder as sibling to a new parent
							JSONObject newPlaceholder = getPlaceHolderNode();
							JSONArray newParentIds = new JSONArray();
							JSONArray newChildren = new JSONArray();
							newParentList.forEach(currentParentId -> newParentIds.put(currentParentId));
							setNodeID(newPlaceholder);
							newChildren.put(currentChild);
							newPlaceholder.put("parentIds", newParentIds);
							newPlaceholder.put("children", newChildren);
							// TODO Place new placeholder as sibling to a new parent?
							JSONObject grandParentNode = getParentNodeByGenId(
									siblingParentRelations.get(currentKey).iterator().next(), requestJSON);
							if (grandParentNode == null) {
								System.err.println("Error retrieving parent node of "
										+ siblingParentRelations.get(currentKey).iterator().next());
								return requestJSON;
							}
							JSONArray grandParentChildren = grandParentNode.getJSONArray("children");
							grandParentChildren.put(newPlaceholder);
							replaceTreeNode(requestJSON, grandParentNode);
						}

					}
				}
			}
			// Just move child to to parent if it isn't already
			else {
				JSONArray currentChildren = currentParentNode.getJSONArray("children");
				for (int j = 0; j < currentChildren.length(); j++)
					if (currentChildren.getJSONObject(j).optString("genID").equals(currentKey)) {
						currentChild = currentChildren.getJSONObject(j);
						currentChildren.remove(j);
						break;
					}
				requestJSON = replaceTreeNode(currentParentNode, requestJSON);
				JSONObject newParentNode = getNodeByGenID(newParentList.iterator().next(), requestJSON);
				JSONArray newParentChildren = newParentNode.optJSONArray("children") != null
						? newParentNode.getJSONArray("children")
						: new JSONArray();
				newParentChildren.put(currentChild);
				replaceTreeNode(newParentNode, requestJSON);
			}
			// Add new childRelations to JSON of post request
			if (siblingParentRelations.get(currentKey) != null)
				for (String currentParent : siblingParentRelations.get(currentKey)) {
					JSONObject newRelation = new JSONObject();
					newRelation.put("source", genIDToIdMap.get(currentParent));
					newRelation.put("target", genIDToIdMap.get(currentKey));
					for (int i = 0; i < existingParentChildRelations.length(); i++)
						if (existingParentChildRelations.getJSONObject(i)
								.getInt("source") == (newRelation.getInt("source"))
								&& existingParentChildRelations.getJSONObject(i)
										.getInt("target") == (newRelation.getInt("target"))) {
							newRelation = null;
							break;
						}
					if (newRelation != null)
						existingParentChildRelations.put(newRelation);
				}
			// Add new spouseRelations to JSON of post request
			if (spouseParentRelations.get(currentKey) != null)
				for (String currentSpouse : spouseParentRelations.get(currentKey)) {
					JSONObject newRelation = new JSONObject();
					newRelation.put("source", genIDToIdMap.get(currentSpouse));
					newRelation.put("target", genIDToIdMap.get(currentKey));
					for (int i = 0; i < existingSpouseRelations.length(); i++)
						if ((existingSpouseRelations.getJSONObject(i).getInt("source") == (newRelation.getInt("source"))
								&& existingSpouseRelations.getJSONObject(i)
										.getInt("target") == (newRelation.getInt("target")))
								|| (existingSpouseRelations.getJSONObject(i)
										.getInt("target") == (newRelation.getInt("source"))
										&& existingSpouseRelations.getJSONObject(i)
												.getInt("source") == (newRelation.getInt("target")))) {
							newRelation = null;
							break;
						}
					if (newRelation != null)
						existingSpouseRelations.put(newRelation);
				}
			requestJSON.put("childRelations", existingParentChildRelations);
			requestJSON.put("spouses", existingSpouseRelations);

		}
		return requestJSON;
	}

	protected JSONObject getParentNodeByGenId(String childId, JSONObject node) {
		if (node.optJSONArray("children") != null)
			for (int i = 0; i < node.optJSONArray("children").length(); i++) {
				if (node.optJSONArray("children").getJSONObject(i).opt("genID") != null
						&& node.optJSONArray("children").getJSONObject(i).opt("genID").equals(childId))
					return node;
			}

		JSONArray children = node.optJSONArray("children");
		JSONObject res = null;
		if (children != null)
			for (int i = 0; res == null && i < children.length(); i++) {
				res = getParentNodeByGenId(childId, (JSONObject) children.getJSONObject(i));
			}
		return res;
	}

	protected JSONObject getParentNodeById(int id, JSONObject node) {
		if (node.optJSONArray("children") != null)
			for (int i = 0; i < node.optJSONArray("children").length(); i++) {
				if (node.optJSONArray("children") != null
						&& node.optJSONArray("children").getJSONObject(i).optInt("id", 0) == id) {
					return node;
				}
			}
		JSONArray children = node.optJSONArray("children");
		JSONObject res = null;
		if (children != null)
			for (int i = 0; res == null && i < children.length(); i++) {
				res = getParentNodeById(id, (JSONObject) children.getJSONObject(i));
			}
		return res;
	}

	protected JSONObject getNodeById(int id, JSONObject node) {
		if (node.optJSONArray("children") != null)
			for (int i = 0; i < node.optJSONArray("children").length(); i++) {
				if (node.optJSONArray("children").getJSONObject(i).optInt("id", 0) > 0
						&& node.optJSONArray("children").getJSONObject(i).optInt("id") == id)
					return node.optJSONArray("children").getJSONObject(i);
			}

		JSONArray children = node.optJSONArray("children");
		JSONObject res = null;
		if (children != null)
			for (int i = 0; res == null && i < children.length(); i++) {
				res = getNodeById(id, (JSONObject) children.getJSONObject(i));
			}
		return res;
	}

	protected JSONObject getPlaceHolderNodeByParentIds(Set<String> parentIds, JSONObject node) {
		if (node.optJSONArray("children") != null && node.optString("genID").isEmpty()
				&& node.optJSONArray("parentIds") != null) {
			int matchedIds = 0;
			for (int j = 0; j < node.optJSONArray("parentIds").length(); j++) {
				matchedIds = parentIds.contains(node.optJSONArray("parentIds").getString(j)) ? matchedIds + 1
						: matchedIds;
			}
			if (matchedIds == parentIds.size())
				return node;
		}

		JSONArray children = node.optJSONArray("children");
		JSONObject res = null;
		if (children != null)
			for (int i = 0; res == null && i < children.length(); i++) {
				res = getPlaceHolderNodeByParentIds(parentIds, (JSONObject) children.getJSONObject(i));
			}
		return res;
	}

	/**
	 * Get genIDs linked to auto-generated ids
	 */
	protected Map<Integer, String> getIdToGenIdMap(String jsonString) {
		Map<Integer, String> idToGenIdMap = new HashMap<>();
		List<Map> pathResult = JsonPath.parse(jsonString).read("$..[?(@.id > 0)]");
		for (int i = 0; i < pathResult.size(); i++) {
			Map currentResult = pathResult.get(i);
			if (currentResult.get("id") != null && currentResult.get("genID") != null)
				idToGenIdMap.put((int) (currentResult.get("id")), (String) currentResult.get("genID"));
		}
		return idToGenIdMap;
	}

	/**
	 * Get genIDs linked to auto-generated ids
	 */
	protected Map<String, Integer> getGenIDToIdMap(String jsonString) {
		Map<String, Integer> genIDToIdMap = new HashMap<>();
		List<Map> pathResult = JsonPath.parse(jsonString).read("$..[?(@.id > 0)]");
		for (int i = 0; i < pathResult.size(); i++) {
			Map currentResult = pathResult.get(i);
			if (currentResult.get("id") != null && currentResult.get("genID") != null)
				genIDToIdMap.put((String) currentResult.get("genID"), (int) (currentResult.get("id")));
		}
		return genIDToIdMap;
	}

	public List<JSONObject> getLinkedNodes(JSONObject node, JSONArray spouseRelations, JSONArray parentChildren,
			Map<Integer, String> idToGenIdMap) {
		String nodeId = node.getString("genID");
		List<JSONObject> result = new ArrayList<>();
		List<JSONObject> matchingSpouses = new ArrayList<>();
		List<JSONObject> matchingPlaceholders = new ArrayList<>();

		List<String> spouseIds = new ArrayList<>();
// Search for placeholders
		for (int i = 0; i < parentChildren.length(); i++) {
			if (parentChildren.getJSONObject(i).optJSONArray("parentIds") != null) {
				JSONArray parentIds = parentChildren.getJSONObject(i).optJSONArray("parentIds");
				boolean matchedIds = false;
				Set<String> currentIds = new HashSet<>();
				for (int j = 0; j < parentIds.length(); j++)
					if (parentIds.getString(j).equals(nodeId))
						matchedIds = true;
					else
						currentIds.add(parentIds.getString(j));

				if (matchedIds && currentIds.size() > 0) {
					matchingPlaceholders.add(parentChildren.getJSONObject(i));
				}
				// Search for spouses
			} else if (!parentChildren.getJSONObject(i).optString("genID").isEmpty()) {
				String currentId = parentChildren.getJSONObject(i).getString("genID");
				if (currentId.equals(nodeId))
					continue;
				for (int j = 0; j < spouseRelations.length(); j++) {
					JSONObject currentRelation = spouseRelations.getJSONObject(j);
					if ((idToGenIdMap.getOrDefault(currentRelation.optInt("source"), "").equals(nodeId)
							&& idToGenIdMap.getOrDefault(currentRelation.optInt("target"), "").equals(currentId))
							|| (idToGenIdMap.getOrDefault(currentRelation.optInt("target"), "").equals(nodeId)
									&& idToGenIdMap.getOrDefault(currentRelation.optInt("source"), "")
											.equals(currentId))) {
						matchingSpouses.add(parentChildren.getJSONObject(i));
						spouseIds.add(currentId);
						break;

					}
				}
			}
		}
		// Only return placeholders where all parents are moved as well
		List<JSONObject> placeholdersToRemove = new ArrayList<>();
		for (JSONObject currentPlaceholder : matchingPlaceholders) {
			JSONArray currentParentIds = currentPlaceholder.getJSONArray("parentIds");
			for (int i = 0; i < currentParentIds.length(); i++)
				if (currentParentIds.getString(i).equals(nodeId))
					continue;
				else if (!spouseIds.contains(currentParentIds.getString(i))) {
					placeholdersToRemove.add(currentPlaceholder);
					break;
				}
		}
		for (JSONObject cur : placeholdersToRemove)
			matchingPlaceholders.remove(cur);

		// Only return spouses where no depending placeholder is left behind
		for (int i = 0; i < parentChildren.length(); i++) {
			Set<JSONObject> spousesToRemove = new HashSet<>();
			spouseLoop: for (JSONObject currentSpouse : matchingSpouses)
				if (!matchingPlaceholders.contains(parentChildren.getJSONObject(i))
						&& parentChildren.getJSONObject(i).optJSONArray("parentIds") != null) {
					JSONArray parentIds = parentChildren.getJSONObject(i).getJSONArray("parentIds");
					for (int j = 0; j < parentIds.length(); j++)
						if (currentSpouse.getString("genID").equals(parentIds.getString(j))) {
							spousesToRemove.add(currentSpouse);
							continue spouseLoop;
						}
				}
			for (JSONObject spouseToRemove : spousesToRemove) {
				matchingSpouses.remove(spouseToRemove);
				spouseIds.remove(spouseToRemove.getString("genID"));
			}
		}
		result.addAll(matchingPlaceholders);
		result.addAll(matchingSpouses);
		return result;
	}
}
