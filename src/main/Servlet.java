package main;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.neo4j.driver.v1.AuthTokens;
import org.neo4j.driver.v1.Config;
import org.neo4j.driver.v1.Driver;
import org.neo4j.driver.v1.GraphDatabase;
import org.neo4j.driver.v1.Session;
import org.neo4j.driver.v1.exceptions.ClientException;
import org.neo4j.driver.v1.exceptions.ServiceUnavailableException;

import com.jayway.jsonpath.JsonPath;

public class Servlet extends HttpServlet implements AutoCloseable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2197127016083093451L;

	final String mainServerAddress = "jlu-buster.mni.thm.de:10231";
	final String username = "neo4j";
	final String password = "1234";

	boolean fallbackUsed = false;

	QueryHelper helper = null;
	final Object nullObject = null;
	// Distinguish different query types and their params
	static final List<String> flags = Arrays.asList(new String[] { "search", "aliases", "children", "parent",
			"siblings", "spouses", "fetchTree", "childCount", "siblingCount" });
	static final String searchFlag = flags.get(0);
	protected static final String nameParameter = "name";
	protected static final String ageParameter = "age";
	protected static final String dateOfBirthParameter = "dateOfBirth";
	protected static final String dateOfDeathParameter = "dateOfDeath";
	protected static final String yearOfBirthParameter = "yearOfBirth";
	protected static final String yearOfDeathParameter = "yearOfDeath";
	protected static final String hasSpouseParameter = "hasSpouse";
	protected static final String spouseCountParameter = "spouseNo";
	protected static final String hasChildParameter = "hasChild";
	protected static final String childCountParameter = "childNo";
	protected static final String hasSiblingParameter = "hasSibling";
	protected static final String siblingCountParameter = "siblingNo";
	protected static final String skipOffset = "page";

	private static final String fetchAliasFlag = flags.get(1);
	private static final String aliasIdParameter = "aliasId";

	private static final String fetchChildrenFlag = flags.get(2);
	private static final String fetchChildCountFlag = "childCount";
	private static final String parentIdParameter = "parentId";
	private static final String existingChildIdParameter = "childId";

	private static final String fetchParentFlag = flags.get(3);
	private static final String childIdParameter = "childId";
	private static final String existingParentIdParameter = "existingParentId";

	private static final String fetchSiblingsFlag = flags.get(4);
	private static final String fetchSiblingsCountFlag = "siblingCount";
	private static final String siblingIdParameter = "siblingId";
	private static final String existingSiblingIdParameter = "existingSiblingId";

	private static final String fetchSpousesFlag = flags.get(5);
	private static final String fetchSpousesCountFlag = "spouseCount";
	private static final String spouseIdParameter = "spouseId";
	private static final String existingSpouseIdParameter = "existingSpouseId";

	private static final String fetchInitialTree = "fetchTree";
	// Person selected in the search results is actually not the root but the
	// leftmost child of its person in this tree
	private static final String rootIdParameter = "rootId";

	// Parameters for search
	private String[] parameterList = new String[] { ageParameter, dateOfBirthParameter, yearOfBirthParameter,
			yearOfDeathParameter, dateOfDeathParameter, hasSpouseParameter, spouseCountParameter, fetchAliasFlag,
			aliasIdParameter, fetchSpousesFlag, fetchSpousesCountFlag, spouseIdParameter, existingSpouseIdParameter,
			searchFlag, nameParameter, hasChildParameter, childCountParameter, hasSiblingParameter,
			siblingCountParameter, skipOffset, fetchChildrenFlag, fetchChildCountFlag, parentIdParameter,
			existingChildIdParameter, fetchParentFlag, childIdParameter, existingParentIdParameter, fetchSiblingsFlag,
			fetchSiblingsCountFlag, siblingIdParameter, existingSiblingIdParameter, fetchInitialTree, rootIdParameter };

	Driver driver = null;
	Session session = null;
	// JsonGenerator generator = null;
	Map<String, String[]> queryParams;
	final String boltPrefix = "bolt://";

	public void init() throws ServletException {
		// Do required initialization
		helper = new QueryHelper();
	}

	public Driver getDriver() {
		if (driver == null) {
			try {
				driver = GraphDatabase.driver(boltPrefix + mainServerAddress, AuthTokens.basic(username, password),
						Config.build().withoutEncryption()
								.withTrustStrategy(Config.TrustStrategy.trustAllCertificates()).toConfig());
				fallbackUsed = false;
			}
			// Server unavailable or moved to different address
			catch (ClientException | ServiceUnavailableException e) {
				// No server available
				driver = null;
			}
		}
		return driver;

	}

	public Session getSession() {
		if (session == null && driver != null)
			session = driver.session();
		else
			session = null;
		return session;
	}

	private JSONObject parseQuery(HttpServletRequest request) {
		return parseQuery(request, false);
	}

	private JSONObject parseQuery(HttpServletRequest request, boolean existingTree) {
		queryParams = request.getParameterMap();
		// Values for specific parameters
		Map<String, String[]> valueList = new HashMap<>();
		// Initialized to allow transmission to client even without results
		JSONObject result = new JSONObject();
		String jsonString = "";
		JSONObject requestJSON = null;
		// Get values from query
		for (String currentParam : parameterList) {
			if (queryParams.get(currentParam) != null && (queryParams.get(currentParam).length == 1
					? (!queryParams.get(currentParam)[0].isEmpty() || flags.contains(currentParam))
					: true))
				valueList.put(currentParam, queryParams.get(currentParam));
		}

		if (existingTree) {
			try {
				jsonString = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
				return result;
			}
			try {
				requestJSON = new JSONObject(jsonString);
			} catch (JSONException err) {
				System.out.println(err.toString());
				return result;
			}
		}
		if (valueList.get(fetchChildrenFlag) != null) {
			if (valueList.get(parentIdParameter) != null) {
				JSONObject parent = new JSONObject();
				if (!existingTree) {
					// Only fetch children from a single parent for now
					// TODO add support for multiple parents in query?
					parent.put("genID", valueList.get(parentIdParameter)[0]);
					JSONArray children = null;
					if (valueList.get(existingChildIdParameter) != null) {
						children = helper.getChildren(session, valueList.get(parentIdParameter)[0], null,
								valueList.get(existingChildIdParameter));
					} else {
						children = helper.getChildren(session, valueList.get(parentIdParameter)[0], null);
					}
					result.put("children", children);
				}
				// Add results to existing tree
				else {
					jsonString = requestJSON.toString();
					JSONObject parentNode = helper.getParentNodeByGenId(valueList.get(parentIdParameter)[0],
							requestJSON);
					JSONObject node = helper.getNodeByGenID(valueList.get(parentIdParameter)[0], requestJSON);
					JSONArray childRelations = requestJSON.optJSONArray("childRelations");
					if (parentNode != null) {
						Map<Integer, String> idToGenIdMap = helper.getIdToGenIdMap(jsonString);
						Map<String, Integer> genIDToIdMap = helper.getGenIDToIdMap(jsonString);

						String[] parentList = parentNode.optJSONArray("parentIds") != null
								? new String[parentNode.getJSONArray("parentIds").length()]
								: new String[0];

						// Placeholder node with more than one parent?
						for (int i = 0; i < parentList.length; i++) {
							parentList[i] = parentNode.getJSONArray("parentIds").getString(i);
						}
						int maxId = getMaxId(jsonString);
						helper.setIdCounter(maxId + 1);
						// 1. Get child-genIDs of childRelations that already are present for node
						List<String> existingChildren = new ArrayList<String>();
						for (int i = 0; i < childRelations.length(); i++)
							if (childRelations.getJSONObject(i).getInt("source") == node.getInt("id"))
								existingChildren
										.add(idToGenIdMap.get(childRelations.getJSONObject(i).getInt("target")));
						// 2. Fetch children excluding existing child-genIDs
						JSONArray newChildren = helper.getChildren(session, node.getString("genID"), null,
								(String[]) existingChildren.toArray(new String[0]));
						if (newChildren.length() > 0) {
							// 3. Fetch childRelations between node and new children
							JSONArray newChildRelations = helper.getChildRelationsOfParent(session,
									node.getString("genID"), (String[]) existingChildren.toArray(new String[0]));
							// 3a. Extend child relations with new relations, remove relations for spouses
							// that aren't part of the tree, insert into tree
							Map<String, Set<String>> cleanedChildParentRelations = helper
									.cleanChildRelations(newChildRelations, genIDToIdMap);
							// 4. Search parentIds of placeholder nodes for genIds as detailed in the new
							// relations
							// Regardless if spouse on same level or not, might have been moved up due to
							// fetching of parents, placeholder created on level of node regardless if node
							// is in a placeholder node or directly a child of its parent
							for (int i = 0; i < newChildren.length(); i++) {
								Set<String> currentParentIds = cleanedChildParentRelations
										.get(newChildren.getJSONObject(i).getString("genID"));
								idToGenIdMap.put(helper.getIdCounter(),
										newChildren.getJSONObject(i).getString("genID"));
								genIDToIdMap.put(newChildren.getJSONObject(i).getString("genID"),
										helper.getIdCounter());
								// No need to set parent flag as placeholder node has it already set
								helper.setNodeID(newChildren.getJSONObject(i));
								if (currentParentIds.size() > 1) {
									JSONObject placeholder = helper.getPlaceHolderNodeByParentIds(currentParentIds,
											requestJSON);
									if (placeholder != null) {
										// placeholder can be located anywhere in the tree, add new children to it
										// Only lookup new children if more than one parentId is specified
										JSONArray placeholderChildren = placeholder.optJSONArray("children");
										placeholderChildren = placeholderChildren == null ? new JSONArray()
												: placeholderChildren;
										placeholderChildren.put(newChildren.getJSONObject(i));
										placeholder.remove("children");
										placeholder.put("children", placeholderChildren);
										// Replace existing requestJSON, use copy of old version
										JSONObject placeholderCopy = helper
												.getPlaceHolderNodeByParentIds(currentParentIds, requestJSON);
										JSONObject changedTree = helper.replaceTreeNode(requestJSON, placeholder);
										requestJSON = changedTree;
									} else {
										// Create new placeholder node on the same level as node
										placeholder = new JSONObject();
										helper.setNodeID(placeholder, true);
										JSONArray placeholderChildren = new JSONArray();
										placeholderChildren.put(newChildren.getJSONObject(i));
										placeholder.put("children", placeholderChildren);
										parentNode = helper.getParentNodeByGenId(valueList.get(parentIdParameter)[0],
												requestJSON);
										JSONArray parentNodeChildren = parentNode.getJSONArray("children");
										parentNodeChildren.put(placeholder);
										parentNode.remove("children");
										parentNode.put("children", parentNodeChildren);
//										List parentString = JsonPath.read(jsonString,
//												"$..children[?(@.id == " + parentNode.getInt("id") + ")]");
										JSONObject changedTree = helper.replaceTreeNode(requestJSON, parentNode);
//										JSONObject parentNodeCopy = getNodeById(parentNode.getInt("id"), requestJSON);
//										jsonString = jsonString.replace(parentNodeCopy.toString(),
//												parentNode.toString());
										requestJSON = changedTree;
									}
									// Replace placeholder and insert new relations
								} else {
									JSONArray nodeChildren = node.optJSONArray("children") == null ? new JSONArray()
											: node.getJSONArray("children");
									nodeChildren.put(newChildren.getJSONObject(i));
									node = helper.getNodeByGenID(valueList.get(parentIdParameter)[0], requestJSON);
									node.remove("children");
									node.put("children", nodeChildren);
									jsonString = requestJSON.toString();
//									JSONObject nodeCopy = getNodeById(node.getInt("id"), requestJSON);
									JSONObject changedTree = helper.replaceTreeNode(requestJSON, node);
//									jsonString = jsonString.replace(nodeCopy.toString(), node.toString());
									requestJSON = changedTree;
								}
								for (String currentParentString : currentParentIds) {
									JSONObject newChildRelation = new JSONObject();
									newChildRelation.put("source", genIDToIdMap.get(currentParentString));
									newChildRelation.put("target",
											genIDToIdMap.get(newChildren.getJSONObject(i).getString("genID")));
									childRelations.put(newChildRelation);
								}
							}
							// Add new child-relations to the tree
							requestJSON.remove("childRelations");
							requestJSON.put("childRelations", childRelations);
							// 4a. Add genIDs to parentIds if the spouse exists in the same layer and they
							// weren't present already
							// 5. Insert children into existing placeholders/nodes or newly created
							// placeholders

							requestJSON = helper.addCountInformation(session,
									helper.getGenIdList(newChildren.toString()), requestJSON);
							result = requestJSON;
							// Nodes additional to the selected one can be affected by additional parents

							requestJSON = helper.moveChildrenToNewNodes(node.getString("genID"), childRelations,
									session, requestJSON, node, newChildren, idToGenIdMap, genIDToIdMap);
							result = requestJSON;
						} else {
							System.out.println("No children");
							result = parent;
						}
					}
				}
			}
		} else if (valueList.get(fetchChildCountFlag) != null)

		{
			if (valueList.get(parentIdParameter) != null) {
				JSONObject parent = new JSONObject();
//				parent.put("genID", valueList.get(parentIdParameter)[0]);
				JSONArray childCount = helper.getChildCount(session, valueList.get(parentIdParameter));
				parent.put("childCount", childCount);
				result = parent;
			}
		} else if (valueList.get(fetchParentFlag) != null && valueList.get(childIdParameter) != null) {
			result.put("genID", nullObject);
			JSONArray parents = null;
			if (!existingTree) {
				parents = valueList.get(existingParentIdParameter) != null
						? helper.getParents(session, valueList.get(childIdParameter)[0],
								valueList.get(existingParentIdParameter))
						: helper.getParents(session, valueList.get(childIdParameter)[0]);
				result.put("parents", parents);
			}
			// Add results to existing tree
			// Find matching part, add to json, replace part and return to caller
			else {
				// Search part of JSON to add to
				// Ehepartner spouses anpassen mit neuer Verbindung(Id f�r source und target aus
				// dem JSON-Path Ergebnis f�r die genId von dem Knoten ablesen, von dem
				// Ehepartner gesucht werden sollen): $.['spouses']^
				JSONObject parentNode = helper.getParentNodeByGenId(valueList.get(childIdParameter)[0], requestJSON);
				JSONObject node = helper.getNodeByGenID(valueList.get(childIdParameter)[0], requestJSON);
				JSONArray childRelations = requestJSON.optJSONArray("childRelations");
				if (parentNode != null) {
					Map<Integer, String> idToGenIdMap = helper.getIdToGenIdMap(jsonString);
					Map<String, Integer> genIDToIdMap = helper.getGenIDToIdMap(jsonString);

					JSONArray idSpouseMap = requestJSON.getJSONArray("spouses");
					String[] parentArray = null;

					int maxId = getMaxId(jsonString);
					// Existing placeholder not at root level
					// Get all parents as the node might also be the spouse of a child

					// Is placeholder node in layer above actually related to the selected node?

					// Parents to exclude when fetching new ones
					Set<String> parentList = helper.getParentIds(valueList.get(childIdParameter)[0], requestJSON,
							idToGenIdMap);
					parentArray = new String[parentList.size()];
					parentList.toArray(parentArray);
					int nodeType = getNodeType(node, parentNode, childRelations, genIDToIdMap, idToGenIdMap,
							parentArray, valueList);

					// Java shallow copy<3
					JSONArray parentIds = new JSONArray();
					if (parentNode.optJSONArray("parentIds") != null)
						parentNode.getJSONArray("parentIds")
								.forEach(currentParentId -> parentIds.put((String) currentParentId));

					parents = helper.getParents(session, valueList.get(childIdParameter)[0], parentArray);
					if (parents.length() > 0) {
						Set<String> existingParentSet = new HashSet<>();
						Set<String> newParentSet = new HashSet<>();
						parentIds.forEach(currentParentId -> existingParentSet.add((String) currentParentId));

						// All parentIds of the placeholder node were found in the child relations
						if (nodeType == QueryConstants.PLACEHOLDER_CHILD_NODE) {
							System.out.println("Case 1a: Parent layer exists that belongs to child");
							// parentNode is placeholder

							// Insert parents to layer, add genIDs to parentIds, their ids to spouse
							// relations
							helper.setIdCounter(maxId + 1);

							for (int i = 0; i < parents.length(); i++) {
								// Update maps with new ids
								idToGenIdMap.put(helper.getIdCounter(), parents.getJSONObject(i).getString("genID"));
								genIDToIdMap.put(parents.getJSONObject(i).getString("genID"), helper.getIdCounter());
								// No need to set parent flag as placeholder node has it already set
								helper.setNodeID(parents.getJSONObject(i));
								JSONObject newChildRelation = new JSONObject();
								newChildRelation.put("source", parents.getJSONObject(i).getInt("id"));
								newChildRelation.put("target", genIDToIdMap.get(valueList.get(childIdParameter)[0]));
								childRelations.put(newChildRelation);
								newParentSet.add(parents.getJSONObject(i).getString("genID"));
								parentIds.put(parents.getJSONObject(i).getString("genID"));
							}

							// Get parent relations with id to genID map of the new parent nodes
							JSONArray additionalParentDBRelations = helper.removeRedundantParentRelations(
									helper.getParentSpouseRelationsOfChild(session, valueList.get(childIdParameter)[0]),
									idToGenIdMap, genIDToIdMap);
							// Only add relations that were not already present
							for (int i = 0; i < idSpouseMap.length(); i++) {
								for (int j = 0; j < additionalParentDBRelations.length(); j++) {
									if (additionalParentDBRelations.getJSONObject(j).getInt("source") == idSpouseMap
											.getJSONObject(i).getInt("source")
											&& additionalParentDBRelations.getJSONObject(j)
													.getInt("target") == idSpouseMap.getJSONObject(i).getInt("target")
											|| additionalParentDBRelations.getJSONObject(j)
													.getInt("source") == idSpouseMap.getJSONObject(i).getInt("target")
													&& additionalParentDBRelations.getJSONObject(j)
															.getInt("target") == idSpouseMap.getJSONObject(i)
																	.getInt("source")) {
										additionalParentDBRelations.remove(j);
										break;
									}
								}
							}
							// Put remaining new relations in spouses JSONArray
							for (int k = 0; k < additionalParentDBRelations.length(); k++) {
								// Add new relations
								JSONObject newRelation = new JSONObject();
								newRelation.put("source",
										additionalParentDBRelations.getJSONObject(k).getInt("source"));
								newRelation.put("target",
										additionalParentDBRelations.getJSONObject(k).getInt("target"));
								idSpouseMap.put(newRelation);
							}
							/*
							 * Add new parents on layer of the parentNode, grandparent node is not null as a
							 * node with parentIds is at least on level 2 of the hierarchy
							 */
							JSONObject grandParentNode = helper.getParentNodeById(parentNode.getInt("id"), requestJSON);
							JSONArray grandParentChildren = grandParentNode.getJSONArray("children");
							// Change parentIds of placeholder or move child to new placeholder if not all
							// children affected by new parents
							JSONArray parentChildren = parentNode.getJSONArray("children");
							if (parentChildren.length() == 1) {
								// Change parentIds directly as only one child is affected
								parentNode.put("parentIds", parentIds);
								helper.replaceTreeNode(grandParentNode, parentNode);
							} else {
								List<String> siblingList = new ArrayList<>();
								for (int k = 0; k < parentChildren.length(); k++) {
									if (!parentChildren.getJSONObject(k).optString("genID").isEmpty()
											&& !parentChildren.getJSONObject(k).optString("genID")
													.equals(valueList.get(childIdParameter)[0])) {
										// Check if node is sibling or spouse
										Set<String> siblingParentList = helper.getParentIds(
												parentChildren.getJSONObject(k).optString("genID"), requestJSON,
												idToGenIdMap);
										if (!Collections.disjoint(siblingParentList, parentList)) {
											siblingList.add(parentChildren.getJSONObject(k).optString("genID"));
										}
									}
								}
								Map<String, Set<String>> siblingParentRelations = new HashMap<>();
								if (!siblingList.isEmpty())
									siblingParentRelations = helper
											.cleanChildRelations(
													helper.getParentChildRelationsOfChildren(session, null,
															(String[]) siblingList.toArray(new String[0])),
													genIDToIdMap);

								// Find nodes that shall be moved to new placeholders
								// If nodes shall be moved to multiple targets, ignore them as the underlying
								// file structure is too limited to display the tree in this case <3
								Map<Set<String>, List<List<JSONObject>>> additionalPlaceholderMap = new HashMap<>();

								Set<String> nodePlaceHolderIds = new HashSet<>();
								nodePlaceHolderIds.addAll(existingParentSet);
								nodePlaceHolderIds.addAll(newParentSet);
								List<JSONObject> nodeSpousesAndPlaceholderNodes = helper.getLinkedNodes(node,
										idSpouseMap, parentChildren, idToGenIdMap);
								// Invisible placeholder looks strange otherwise
								nodeSpousesAndPlaceholderNodes.add(0, node);
								List<List<JSONObject>> movingNodes = new ArrayList<>();
								movingNodes.add(nodeSpousesAndPlaceholderNodes);
								additionalPlaceholderMap.put(nodePlaceHolderIds, movingNodes);

								// Fetch parentRelations for siblings of selected node
								// Move siblings (along their spouses and own placeholder nodes <3)
								for (int i = 0; i < parentChildren.length(); i++) {
									int siblingsPosition = 0;
									if (siblingParentRelations
											.containsKey(parentChildren.getJSONObject(i).optString("genID"))) {
										String siblingId = parentChildren.getJSONObject(i).getString("genID");
										List<List<JSONObject>> existingMoveList = new ArrayList<>();
										for (Set<String> currentParentIds : additionalPlaceholderMap.keySet())
											if (siblingParentRelations.get(siblingId).containsAll(currentParentIds)
													&& siblingParentRelations.get(siblingId).size() == currentParentIds
															.size()) {
												existingMoveList = additionalPlaceholderMap.get(currentParentIds);
												additionalPlaceholderMap.remove(currentParentIds);
												break;
											}
										List<JSONObject> siblingSpousesAndPlaceholderNodes = helper.getLinkedNodes(
												parentChildren.getJSONObject(i), idSpouseMap, parentChildren,
												idToGenIdMap);

										siblingSpousesAndPlaceholderNodes.add(0, parentChildren.getJSONObject(i));

										existingMoveList.add(siblingsPosition, siblingSpousesAndPlaceholderNodes);
										siblingsPosition++;

										additionalPlaceholderMap.put(nodePlaceHolderIds, existingMoveList);
										// Reuse existing placeholder list for siblings and its related nodes

									}
								}

								// Remove sibling and related nodes from existing placeholder
								// Move the first set anyway
								Map<Set<String>, Set<JSONObject>> conflictingMoveSets = new HashMap<>();
								for (Set<String> currentKey : additionalPlaceholderMap.keySet()) {
									for (List<JSONObject> currentMoveSet : additionalPlaceholderMap.get(currentKey)) {
										if (currentMoveSet.contains(node))
											continue;
										for (Set<String> currentKey2 : additionalPlaceholderMap.keySet()) {
											if (currentKey.equals(currentKey2)) {
												continue;
											}
											for (List<JSONObject> currentNode : additionalPlaceholderMap
													.get(currentKey2)) {
												if (currentNode.contains(node))
													continue;
												// Remove conflicting moving sets from both new placeholders
												if (currentNode.equals(currentMoveSet)) {
													Set<JSONObject> existingConflicts = conflictingMoveSets
															.getOrDefault(currentKey, new HashSet<>());
													existingConflicts.addAll(currentMoveSet);
													conflictingMoveSets.put(currentKey, existingConflicts);
													existingConflicts = conflictingMoveSets.getOrDefault(currentKey2,
															new HashSet<>());
													existingConflicts.addAll(currentNode);
													conflictingMoveSets.put(currentKey2, existingConflicts);
												}
											}
										}
									}
								}
								for (Set<String> conflictingKey : conflictingMoveSets.keySet()) {
									List<List<JSONObject>> nodesToMove = additionalPlaceholderMap.get(conflictingKey);
									List<List<JSONObject>> changedNodesToMove = new ArrayList<>();
									for (List<JSONObject> additionalNode : nodesToMove) {
										List<JSONObject> changedSet = additionalNode;
										for (JSONObject conflictingNode : conflictingMoveSets.get(conflictingKey))
											changedSet.remove(conflictingNode);
										changedNodesToMove.add(changedSet);
									}
									additionalPlaceholderMap.put(conflictingKey, changedNodesToMove);
								}
								List<JSONObject> finalPlaceholders = new ArrayList<>();
								// Move steps needed for every affected node
								for (Set<String> newParentIds : additionalPlaceholderMap.keySet()) {
									JSONObject newPlaceholder = helper.getPlaceHolderNode();
									JSONArray placeholderParentIds = new JSONArray();
									JSONArray placeholderChildren = new JSONArray();
									newParentIds.forEach(newId -> placeholderParentIds.put(newId));
									newPlaceholder.put("parentIds", newParentIds);
									for (String currentId : newParentIds) {
										if (!newParentSet.contains(currentId))
											continue;
										for (List<JSONObject> currentChildren : additionalPlaceholderMap
												.get(newParentIds)) {
											for (JSONObject currentChild : currentChildren) {
												// Skip placeholders
												if (currentChild.optString("genID").isEmpty())
													continue;
												JSONObject newRelation = new JSONObject();
												newRelation.put("source", genIDToIdMap.get(currentId));
												newRelation.put("target",
														genIDToIdMap.get(currentChild.getString("genID")));
												childRelations.put(newRelation);
											}
										}
									}
									additionalPlaceholderMap.get(newParentIds)
											.forEach(movingNodeSet -> movingNodeSet.forEach(movingNode -> {
												placeholderChildren.put(movingNode);
												for (int i = 0; i < parentChildren.length(); i++)
													if (parentChildren.getJSONObject(i).equals(movingNode)) {
														parentChildren.remove(i);
														break;
													}

											}));
									newPlaceholder.put("children", placeholderChildren);
									finalPlaceholders.add(newPlaceholder);
								}

								if (parentChildren.length() == 0)
									grandParentChildren = helper.removeChild(grandParentChildren,
											parentNode.getInt("id"));
								// Add new parents and placeholders to grandparent node
								for (JSONObject newPlaceholder : finalPlaceholders)
									grandParentChildren.put(newPlaceholder);
								for (int i = 0; i < parents.length(); i++)
									grandParentChildren.put(parents.getJSONObject(i));
								grandParentNode.put("children", grandParentChildren);
								requestJSON = helper.replaceTreeNode(requestJSON, grandParentNode);

							}

							requestJSON.put("spouses", idSpouseMap);
							requestJSON.put("childRelations", childRelations);
							result = requestJSON;
							result = helper.addCountInformation(session, helper.getGenIdList(parents.toString()),
									result);
						}
						// Case 1b: Create new subtree for spouse node, add new parentRelations
						else if (nodeType == QueryConstants.PLACEHOLDER_SPOUSE_NODE) {
							System.out.println("Case 1b: spouse without parent layer");
							// Fetch parent relations

							// Only create new parentnode
							if (parents.length() == 1) {
								// Update maps with new ids
								idToGenIdMap.put(helper.getIdCounter(), parents.getJSONObject(0).getString("genID"));
								genIDToIdMap.put(parents.getJSONObject(0).getString("genID"), helper.getIdCounter());
								// No need to set parent flag as placeholder node has it already set
								JSONObject newNode = helper.setNodeID(parents.getJSONObject(0));
								JSONObject newRelation = new JSONObject();
								newRelation.put("source", newNode.getInt("id"));
								newRelation.put("target", node.getInt("id"));
								childRelations.put(newRelation);
								// Remove node from the placeholder
								JSONArray parentChildren = parentNode.getJSONArray("children");
								for (int i = 0; i < parentChildren.length(); i++)
									if (
//											!parentChildren.getJSONObject(i).optString("genID").isEmpty()&&
									parentChildren.getJSONObject(i).optString("genID")
											.equals(node.getString("genID"))) {
										parentChildren.remove(i);
										break;
									}
								JSONArray newChildren = new JSONArray();
								newChildren.put(node);
								newNode.put("children", newChildren);
								// Put new parent at root of tree
								JSONArray existingChildren = requestJSON.getJSONArray("children");
								existingChildren.put(newNode);
								requestJSON.remove("children");
								requestJSON.put("children", existingChildren);

								JSONObject changedTree = helper.replaceTreeNode(requestJSON, parentNode);
								result = new JSONObject(changedTree);
								result = helper.addCountInformation(session, helper.getGenIdList(parents.toString()),
										result);
							}
							// Create placeholder node besides new parentnodes
							else {
								helper.setIdCounter(maxId + 1);
								JSONArray newParentIds = new JSONArray();
								// Update maps with new ids
								for (int i = 0; i < parents.length(); i++) {
									idToGenIdMap.put(helper.getIdCounter(),
											parents.getJSONObject(i).getString("genID"));
									genIDToIdMap.put(parents.getJSONObject(i).getString("genID"),
											helper.getIdCounter());
									JSONObject newRelation = new JSONObject();
									newRelation.put("source", helper.getIdCounter());
									newRelation.put("target", node.getInt("id"));
									childRelations.put(newRelation);
									newParentIds.put(parents.getJSONObject(i).getString("genID"));
									helper.setNodeID(parents.getJSONObject(i));

								}

								// Set parent flag
								JSONObject placeholderNode = new JSONObject();
								helper.setNodeID(placeholderNode, true);
								placeholderNode.put("parentIds", newParentIds);
								// Remove node from the placeholder
								JSONArray parentChildren = parentNode.getJSONArray("children");
								for (int i = 0; i < parentChildren.length(); i++)
									if (
//											parentChildren.getJSONObject(i).optString("genID") != null &&
									parentChildren.getJSONObject(i).optString("genID")
											.equals(node.getString("genID"))) {
										parentChildren.remove(i);
										break;
									}
								JSONArray newChildren = new JSONArray();
								newChildren.put(node);
								placeholderNode.put("children", newChildren);
								// Put new parent at root of tree
								JSONArray existingChildren = requestJSON.getJSONArray("children");
								JSONArray existingSpouses = requestJSON.getJSONArray("spouses");
								// Get parent relations with id to genID map of the new parent nodes
								JSONArray additionalParentDBRelations = helper
										.removeRedundantParentRelations(
												helper.getParentSpouseRelationsOfChild(session,
														valueList.get(childIdParameter)[0]),
												idToGenIdMap, genIDToIdMap);
								for (int i = 0; i < additionalParentDBRelations.length(); i++)
									existingSpouses.put(additionalParentDBRelations.getJSONObject(i));

								existingChildren.put(parents.getJSONObject(0));
								existingChildren.put(placeholderNode);
								for (int i = 1; i < parents.length(); i++)
									existingChildren.put(parents.getJSONObject(i));

								requestJSON.remove("children");
								requestJSON.put("children", existingChildren);
								requestJSON.remove("spouses");
								// Add new spouse relations
								requestJSON.put("spouses", existingSpouses);

								JSONObject changedTree = helper.replaceTreeNode(requestJSON, parentNode);
								result = changedTree;
								result = helper.addCountInformation(session, helper.getGenIdList(parents.toString()),
										result);
							}

						}
						// Case 2: Single node contains children, children need to be moved to
						// placeholder
						// TODO What to do with spouses in same children layer? Does D3 connect children
						// of parents with the spouse relations?

						else if (nodeType == QueryConstants.CHILD_NODE) {
							// Case 2a: Child with parent but child is that node's child instead of being
							// part of a placeholder
							System.out.println("Case 2a: Child with parent but no parent layer");
							JSONObject grandParentNode = helper.getParentNodeById(parentNode.optInt("id"), requestJSON);

							// add root level placeholder, move children if only one parent is returned

							// Add relations to existing parent or between added parents
							JSONObject placeholderNode = helper.getPlaceHolderNode();
							JSONArray newParentIds = new JSONArray();

							newParentIds.put(parentNode.getString("genID"));
							helper.setIdCounter(maxId + 1);
							helper.setNodeID(placeholderNode, true);
							for (int i = 0; i < parents.length(); i++) {
								helper.setNodeID(parents.getJSONObject(i));
								newParentIds.put(parents.getJSONObject(i).getString("genID"));
								JSONObject newChildRelation = new JSONObject();
								newChildRelation.put("source", parents.getJSONObject(i).getInt("id"));
								newChildRelation.put("target", genIDToIdMap.get(valueList.get(childIdParameter)[0]));
								childRelations.put(newChildRelation);
								idToGenIdMap.put(parents.getJSONObject(i).getInt("id"),
										parents.getJSONObject(i).getString("genID"));
								genIDToIdMap.put(parents.getJSONObject(i).getString("genID"),
										parents.getJSONObject(i).getInt("id"));
							}
							placeholderNode.put("parentIds", newParentIds);
							// Get parent relations with id to genID map of the new parent nodes
							JSONArray additionalParentDBRelations = helper.removeRedundantParentRelations(
									helper.getParentSpouseRelationsOfChild(session, valueList.get(childIdParameter)[0]),
									idToGenIdMap, genIDToIdMap);
							// Add all new relations to the spousemap
							for (int j = 0; j < additionalParentDBRelations.length(); j++) {
								// Add new relations
								JSONObject newRelation = new JSONObject();
								newRelation.put("source",
										additionalParentDBRelations.getJSONObject(j).getInt("source"));
								newRelation.put("target",
										additionalParentDBRelations.getJSONObject(j).getInt("target"));
								idSpouseMap.put(newRelation);
							}
							// Remove selected child and move to placeholder
							JSONArray children = parentNode.optJSONArray("children");
							if (children != null) {
								children = helper.removeChild(children, node.getInt("id"));
								parentNode.remove("children");
								parentNode.put("children", children);
							}
							JSONArray placeholderChildren = new JSONArray();
							placeholderChildren.put(node);
							placeholderNode.put("children", placeholderChildren);
							// Remove single parent with outdated info and replace with new parents
							JSONArray grandParentChildren = grandParentNode.getJSONArray("children");
							grandParentChildren = helper.removeChild(grandParentChildren, parentNode.getInt("id"));
							grandParentChildren.put(parentNode);
							grandParentChildren.put(placeholderNode);
							for (int i = 0; i < parents.length(); i++)
								grandParentChildren.put(parents.getJSONObject(i));
							grandParentNode.remove("children");
							grandParentNode.put("children", grandParentChildren);
							// Update relations
							requestJSON.remove("childRelations");
							requestJSON.put("childRelations", childRelations);
							requestJSON.remove("spouses");
							requestJSON.put("spouses", idSpouseMap);

							result = helper.replaceTreeNode(requestJSON, grandParentNode);
							result = helper.addCountInformation(session, helper.getGenIdList(parents.toString()),
									result);

						} else if (nodeType == QueryConstants.SPOUSE_NODE) {
							// Case 2b: Spouse with no parent or parent layer
							System.out.println(
									"Case 2b: Spouse is contained in other node but has no parent or parent layer");
							// Remove spouse from parentNode, otherwise same as case 1b

							// Only create new parentnode
							if (parents.length() == 1) {
								helper.setIdCounter(maxId + 1);
								// Update maps with new ids
								idToGenIdMap.put(helper.getIdCounter(), parents.getJSONObject(0).getString("genID"));
								genIDToIdMap.put(parents.getJSONObject(0).getString("genID"), helper.getIdCounter());
								// No need to set parent flag as placeholder node has it already set
								JSONObject newNode = helper.setNodeID(parents.getJSONObject(0));
								JSONObject newRelation = new JSONObject();
								newRelation.put("source", newNode.getInt("id"));
								newRelation.put("target", node.getInt("id"));
								childRelations.put(newRelation);
								// Remove node from the placeholder
								JSONArray parentChildren = parentNode.getJSONArray("children");
								for (int i = 0; i < parentChildren.length(); i++)
									if (
//											parentChildren.getJSONObject(i).optString("genID") != null &&
									parentChildren.getJSONObject(i).optString("genID")
											.equals(node.getString("genID"))) {
										parentChildren.remove(i);
										break;
									}
								JSONArray newChildren = new JSONArray();
								newChildren.put(node);
								newNode.put("children", newChildren);
								// Put new parent at root of tree
								JSONArray existingChildren = requestJSON.getJSONArray("children");
								existingChildren.put(newNode);
								requestJSON.remove("children");
								requestJSON.put("children", existingChildren);

								JSONObject changedTree = helper.replaceTreeNode(requestJSON, parentNode);
								changedTree = helper.addCountInformation(session,
										helper.getGenIdList(parents.toString()), changedTree);
								result = changedTree;
							}
							// Create placeholder node besides new parentnodes
							else {
								helper.setIdCounter(maxId + 1);
								JSONArray newParentIds = new JSONArray();
								// Update maps with new ids
								for (int i = 0; i < parents.length(); i++) {
									idToGenIdMap.put(helper.getIdCounter(),
											parents.getJSONObject(i).getString("genID"));
									genIDToIdMap.put(parents.getJSONObject(i).getString("genID"),
											helper.getIdCounter());
									JSONObject newRelation = new JSONObject();
									newRelation.put("source", helper.getIdCounter());
									newRelation.put("target", node.getInt("id"));
									childRelations.put(newRelation);
									newParentIds.put(parents.getJSONObject(i).getString("genID"));
									helper.setNodeID(parents.getJSONObject(i));

								}

								// Set parent flag
								JSONObject placeholderNode = new JSONObject();
								helper.setNodeID(placeholderNode, true);
								placeholderNode.put("parentIds", newParentIds);
								// Remove node from the placeholder
								JSONArray parentChildren = parentNode.getJSONArray("children");
								for (int i = 0; i < parentChildren.length(); i++)
									if (
//											parentChildren.getJSONObject(i).optString("genID") != null && 
									parentChildren.getJSONObject(i).optString("genID")
											.equals(node.getString("genID"))) {
										parentChildren.remove(i);
										break;
									}
								JSONArray newChildren = new JSONArray();
								newChildren.put(node);
								placeholderNode.put("children", newChildren);
								// Put new parent at root of tree
								JSONArray existingChildren = requestJSON.getJSONArray("children");
								JSONArray existingSpouses = requestJSON.getJSONArray("spouses");
								// Get parent relations with id to genID map of the new parent nodes
								JSONArray additionalParentDBRelations = helper
										.removeRedundantParentRelations(
												helper.getParentSpouseRelationsOfChild(session,
														valueList.get(childIdParameter)[0]),
												idToGenIdMap, genIDToIdMap);
								for (int i = 0; i < additionalParentDBRelations.length(); i++)
									existingSpouses.put(additionalParentDBRelations.getJSONObject(i));

								existingChildren.put(parents.get(0));
								existingChildren.put(placeholderNode);
								for (int i = 1; i < parents.length(); i++)
									existingChildren.put(parents.getJSONObject(i));

								requestJSON.remove("children");
								requestJSON.put("children", existingChildren);
								requestJSON.remove("spouses");
								// Add new spouse relations
								requestJSON.put("spouses", existingSpouses);

								JSONObject changedTree = helper.replaceTreeNode(requestJSON, parentNode);
								changedTree = helper.addCountInformation(session,
										helper.getGenIdList(parents.toString()), changedTree);
								result = changedTree;
							}
						}
						// Case 3: root Node parent
						// Create new parent layer with single or multiple parents and placeholder
						else if (nodeType == QueryConstants.ROOT_NODE) {
							// Move layer of root node to be child of new node/placeholder
							System.out.println("Case 3: root node");
							// Create new parentnode
							if (parents.length() == 1) {
								helper.setIdCounter(maxId + 1);
								// Update maps with new ids
								idToGenIdMap.put(helper.getIdCounter(), parents.getJSONObject(0).getString("genID"));
								genIDToIdMap.put(parents.getJSONObject(0).getString("genID"), helper.getIdCounter());
								// No need to set parent flag as placeholder node has it already set
								JSONObject newNode = helper.setNodeID(parents.getJSONObject(0));
								JSONObject newRelation = new JSONObject();
								newRelation.put("source", newNode.getInt("id"));
								newRelation.put("target", node.getInt("id"));
								childRelations.put(newRelation);
								// Remove node from the placeholder
								JSONArray parentChildren = parentNode.getJSONArray("children");
								JSONArray newParentChildren = new JSONArray();
//								for (int i = 0; i < parentChildren.length(); i++)
//									if (parentChildren.getJSONObject(i).optString("genID")
//											.equals(node.getString("genID"))) {
//										parentChildren.remove(i);
//										break;
//									}
//								JSONArray newChildren = new JSONArray();
//								newChildren.put(node);
//								JSONArray newChildren = parentNode.getJSONArray("children");
								newNode.put("children", parentChildren);
								newParentChildren.put(newNode);
								parentNode.put("children", newParentChildren);
								// Put new parent at root of tree
//								JSONArray existingChildren = requestJSON.getJSONArray("children");
//								existingChildren.put(newNode);
//								requestJSON.remove("children");
//								requestJSON.put("children", existingChildren);

								JSONObject changedTree = helper.replaceTreeNode(requestJSON, parentNode);
								changedTree = helper.addCountInformation(session,
										helper.getGenIdList(parents.toString()), changedTree);
								result = changedTree;
							}
							// Create placeholder node besides new parentnodes
							else {
								// Move all root children to new placeholder
								JSONArray newParentIds = new JSONArray();
								helper.setIdCounter(maxId + 1);
								// Update maps with new ids
								for (int i = 0; i < parents.length(); i++) {
									idToGenIdMap.put(helper.getIdCounter(),
											parents.getJSONObject(i).getString("genID"));
									genIDToIdMap.put(parents.getJSONObject(i).getString("genID"),
											helper.getIdCounter());
									JSONObject newRelation = new JSONObject();
									newRelation.put("source", helper.getIdCounter());
									newRelation.put("target", node.getInt("id"));
									newParentIds.put(parents.getJSONObject(i).getString("genID"));
									childRelations.put(newRelation);
									helper.setNodeID(parents.getJSONObject(i));
								}

								// Set parent flag
								JSONObject placeholderNode = helper.getPlaceHolderNode();
								helper.setNodeID(placeholderNode, true);
								placeholderNode.put("parentIds", newParentIds);
								// Remove node from the placeholder
								JSONArray parentChildren = parentNode.getJSONArray("children");
//								for (int i = 0; i < parentChildren.length(); i++)
//									if (parentChildren.getJSONObject(i).optString("genID")
//											.equals(node.getString("genID"))) {
//										parentChildren.remove(i);
//										break;
//									}
								placeholderNode.put("children", parentChildren);
								JSONArray newChildren = new JSONArray();
//								newChildren.put(node);

//								placeholderNode.put("children", newChildren);

								// Put new parent at root of tree
//								JSONArray existingChildren = requestJSON.getJSONArray("children");
								JSONArray existingSpouses = requestJSON.getJSONArray("spouses");
								// Get parent relations with id to genID map of the new parent nodes
								JSONArray additionalParentDBRelations = helper
										.removeRedundantParentRelations(
												helper.getParentSpouseRelationsOfChild(session,
														valueList.get(childIdParameter)[0]),
												idToGenIdMap, genIDToIdMap);
								for (int i = 0; i < additionalParentDBRelations.length(); i++)
									existingSpouses.put(additionalParentDBRelations.getJSONObject(i));
								newChildren.put(parents.get(0));
								newChildren.put(placeholderNode);
//								existingChildren.put(parents.get(0));
//								existingChildren.put(placeholderNode);
								for (int i = 1; i < parents.length(); i++)
//									existingChildren.put(parents.get(i));
									newChildren.put(parents.getJSONObject(i));
								parentNode.put("children", newChildren);
//								requestJSON.remove("children");
//								requestJSON.put("children", existingChildren);
//								requestJSON.remove("spouses");
								requestJSON = helper.replaceTreeNode(requestJSON, parentNode);
								// Add new spouse relations
								requestJSON.put("spouses", existingSpouses);
								requestJSON = helper.addCountInformation(session,
										helper.getGenIdList(parents.toString()), requestJSON);
								result = requestJSON;

							}
							// Move existing siblings to new placeholder if same new parentRelations
							result = requestJSON;
						}

						else {
							// Unknown case
							System.err.println("Unknown node type case");
						}

					}
					// All parents are already fetched, should not happen with buttons grayed out
					// Just return the input tree?
					else {
						System.out.println("No new parents");
						result = requestJSON;
					}

					/*
					 * Check for nodes affected by the additional parents Get all siblings and check
					 * if they need to be moved to new placeholder nodes. For each node check if
					 * parentIds are affected by change. Check tree after every change.
					 */

//					requestJSON = helper.moveChildrenToNewNodes(node.getString("genID"), childRelations, session,
//							requestJSON, node, parents, idToGenIdMap, genIDToIdMap);

				} else {
					// Shouldn't be possible
					System.err.println("No parentNode of node with genID " + valueList.get(childIdParameter)[0]);
				}

			}
			result = requestJSON;

		} else if (valueList.get(fetchSiblingsFlag) != null && valueList.get(siblingIdParameter) != null)

		{
			String siblingId = valueList.get(siblingIdParameter)[0];
			if (!existingTree) {
				if (valueList.get(siblingIdParameter) != null) {
					// Person the siblings are related to
					JSONObject person = new JSONObject();
					person.put("genID", siblingId);
					JSONArray siblings = null;
					String[] existingSiblingId = valueList.get(existingSiblingIdParameter);
					String[] existingParentId = valueList.get(existingParentIdParameter);
					// Case 2
					if (existingSiblingId != null)
						siblings = helper.getSiblings(session, siblingId, null, existingSiblingId);

					// Case 1
					else {
						if (existingParentId != null)
							siblings = helper.getSiblings(session, siblingId, existingParentId, true);
						// Case 2 w/o excluding siblings
						else
							siblings = helper.getSiblings(session, siblingId, null);
					}
					person.put("siblings", siblings);
					result = person;
				}
			} else {
				// Existing siblings: Exclude genIds from childRelations with same origin as
				// selected node
				// Fetch childRelations, put new nodes in appropriate children JSONArray
				// Fetch spouseRelations for new nodes, add to idSpouseMap if one of spouses
				// exists in tree
				JSONObject parentNode = helper.getParentNodeByGenId(valueList.get(siblingIdParameter)[0], requestJSON);
				JSONObject node = helper.getNodeByGenID(valueList.get(siblingIdParameter)[0], requestJSON);
				JSONArray childRelations = requestJSON.optJSONArray("childRelations");
				if (parentNode != null) {
					Map<Integer, String> idToGenIdMap = helper.getIdToGenIdMap(jsonString);
					Map<String, Integer> genIDToIdMap = helper.getGenIDToIdMap(jsonString);

//					Using this in the queries would limit results
//					String[] parentList = parentNode.optJSONArray("parentIds") != null
//							? new String[parentNode.getJSONArray("parentIds").length()]
//							: new String[0];
					int maxId = getMaxId(jsonString);
					helper.setIdCounter(maxId + 1);
					List<String> existingParents = new ArrayList<>();
					List<String> existingSiblings = new ArrayList<>();
					if (childRelations != null && childRelations.length() > 0) {
						for (int i = 0; i < childRelations.length(); i++) {
							if (childRelations.getJSONObject(i).optInt("target") == (node.getInt("id")))
								existingParents.add(idToGenIdMap.get(childRelations.getJSONObject(i).optInt("source")));
						}
						for (int i = 0; i < childRelations.length(); i++) {
							if (existingParents
									.contains(idToGenIdMap.get(childRelations.getJSONObject(i).optInt("source")))
									&& childRelations.getJSONObject(i).optInt("target") != node.getInt("id"))
								existingSiblings
										.add(idToGenIdMap.get(childRelations.getJSONObject(i).optInt("target")));
						}
					}
					JSONArray newSiblings = helper.getSiblings(session, siblingId, null,
							(String[]) existingSiblings.toArray(new String[0]));
					if (newSiblings.length() > 0) {
						for (int i = 0; i < newSiblings.length(); i++) {
							String currentSiblingId = newSiblings.getJSONObject(i).getString("genID");

							Map<String, Set<String>> currentParentRelations = helper.cleanParentRelationsOfChild(
									helper.getParentChildRelationsOfChildren(session, currentSiblingId), idToGenIdMap,
									childRelations);
							idToGenIdMap.put(helper.getIdCounter(), newSiblings.getJSONObject(i).getString("genID"));
							genIDToIdMap.put(newSiblings.getJSONObject(i).getString("genID"), helper.getIdCounter());
							// No need to set parent flag as placeholder node has it already set
							helper.setNodeID(newSiblings.getJSONObject(i));
							if (currentParentRelations.get(currentSiblingId) != null
									&& currentParentRelations.get(currentSiblingId).size() > 1) {
								JSONObject placeholder = helper.getPlaceHolderNodeByParentIds(
										currentParentRelations.get(currentSiblingId), requestJSON);
								if (placeholder != null) {
									JSONArray placeholderChildren = placeholder.optJSONArray("children");
									placeholderChildren = placeholderChildren == null ? new JSONArray()
											: placeholderChildren;
									placeholderChildren.put(newSiblings.getJSONObject(i));
									placeholder.remove("children");
									placeholder.put("children", placeholderChildren);
									// Replace existing requestJSON, use copy of old version
									JSONObject placeholderCopy = helper.getPlaceHolderNodeByParentIds(
											currentParentRelations.get(currentSiblingId), requestJSON);
									JSONObject changedTree = helper.replaceTreeNode(requestJSON, placeholder);
									changedTree = helper.addCountInformation(session,
											helper.getGenIdList(newSiblings.toString()), changedTree);
									requestJSON = changedTree;
								} else {
									placeholder = new JSONObject();
									helper.setNodeID(placeholder, true);
									JSONArray placeholderChildren = new JSONArray();
									placeholderChildren.put(newSiblings.getJSONObject(i));
									placeholder.put("children", placeholderChildren);
									parentNode = helper.getParentNodeByGenId(siblingId, requestJSON);

									JSONArray parentNodeChildren = parentNode.getJSONArray("children");
									parentNodeChildren.put(placeholder);
									parentNode.remove("children");
									parentNode.put("children", parentNodeChildren);
									JSONObject changedTree = helper.replaceTreeNode(requestJSON, parentNode);
									changedTree = helper.addCountInformation(session,
											helper.getGenIdList(newSiblings.toString()), changedTree);
									requestJSON = changedTree;
								}
								// Add new childRelations
								for (String currentParent : currentParentRelations.get(currentSiblingId)) {
									JSONObject newRelation = new JSONObject();
									newRelation.put("source", genIDToIdMap.get(currentParent));
									newRelation.put("target", newSiblings.getJSONObject(i).getInt("id"));
									childRelations.put(newRelation);
								}
								requestJSON.remove("childRelations");
								requestJSON.put("childRelations", childRelations);
							} else {
								String[] singleParent = currentParentRelations != null
										&& currentParentRelations.get(currentSiblingId) != null
												? Arrays.copyOf(currentParentRelations.get(currentSiblingId).toArray(),
														currentParentRelations.get(currentSiblingId).size(),
														String[].class)
												: new String[0];
								JSONObject singleParentJSON = singleParent.length > 0
										? helper.getNodeByGenID(singleParent[0], requestJSON)
										: null;
								if (singleParentJSON != null) {
									JSONArray nodeChildren = singleParentJSON.optJSONArray("children") == null
											? new JSONArray()
											: singleParentJSON.getJSONArray("children");
									nodeChildren.put(newSiblings.getJSONObject(i));
									singleParentJSON = helper.getNodeByGenID(singleParent[0], requestJSON);
									singleParentJSON.remove("children");
									singleParentJSON.put("children", nodeChildren);
									jsonString = requestJSON.toString();
									JSONObject changedTree = helper.replaceTreeNode(requestJSON, singleParentJSON);
									// Add new childRelations
									JSONObject newRelation = new JSONObject();
									newRelation.put("source", genIDToIdMap.get(singleParentJSON.getString("genID")));
									newRelation.put("target", newSiblings.getJSONObject(i).getInt("id"));
									childRelations.put(newRelation);
									changedTree.remove("childRelations");
									changedTree.put("childRelations", childRelations);
									changedTree = helper.addCountInformation(session,
											helper.getGenIdList(newSiblings.toString()), changedTree);
									requestJSON = changedTree;
								} else {
									// Sibling of node that is at the root, put in the same children JSONArray, it
									// has no parentIds anyway
									JSONArray nodeChildren = requestJSON.optJSONArray("children");
									nodeChildren.put(newSiblings.getJSONObject(i));
									requestJSON.remove("children");
									requestJSON.put("children", nodeChildren);
									requestJSON = helper.addCountInformation(session,
											helper.getGenIdList(newSiblings.toString()), requestJSON);
									jsonString = requestJSON.toString();
								}
							}
						}
						// Nodes additional to the selected one can be affected by additional parents
						requestJSON = helper.moveChildrenToNewNodes(node.getString("genID"), childRelations, session,
								requestJSON, node, newSiblings, idToGenIdMap, genIDToIdMap);
						result = requestJSON;
					} else
						System.out.println("No new siblings");
				} else
					System.out.println("No parentNode of node with genID");
			}

		} else if (valueList.get(fetchSiblingsCountFlag) != null)

		{
			if (valueList.get(siblingIdParameter) != null) {
				JSONObject person = new JSONObject();
//				person.put("genID", valueList.get(siblingIdParameter)[0]);
				JSONArray siblingCount = helper.getSiblingCount(session, valueList.get(siblingIdParameter));
				person.put("siblingCount", siblingCount);
				result = person;
			}
		} else if (valueList.get(fetchSpousesFlag) != null && valueList.get(spouseIdParameter) != null) {
			if (!existingTree) {
				JSONObject spouse = new JSONObject();
				JSONArray spouses = null;
				spouse.put("genID", valueList.get(spouseIdParameter)[0]);
				if (valueList.get(existingSpouseIdParameter) != null) {
					spouses = helper.getSpouses(session, valueList.get(spouseIdParameter)[0],
							valueList.get(existingSpouseIdParameter));
				} else {
					spouses = helper.getSpouses(session, valueList.get(spouseIdParameter)[0]);
				}
				spouse.put("spouses", spouses);
				result = spouse;
			} else {
				// Existing spouses: exclude their genIDs from queries
				JSONObject parentNode = helper.getParentNodeByGenId(valueList.get(spouseIdParameter)[0], requestJSON);
				JSONObject node = helper.getNodeByGenID(valueList.get(spouseIdParameter)[0], requestJSON);
				JSONArray childRelations = requestJSON.optJSONArray("childRelations");
				if (parentNode != null) {
					Map<Integer, String> idToGenIdMap = helper.getIdToGenIdMap(jsonString);
					Map<String, Integer> genIDToIdMap = helper.getGenIDToIdMap(jsonString);

					JSONArray idSpouseMap = requestJSON.getJSONArray("spouses");
					int maxId = getMaxId(jsonString);
					helper.setIdCounter(maxId + 1);
					List<String> existingSpouses = new ArrayList<>();
					// Get list of spouses to exclude from query
					if (idSpouseMap != null && idSpouseMap.length() > 0) {
						for (int i = 0; i < idSpouseMap.length(); i++) {
							if (idSpouseMap.getJSONObject(i).optInt("target") == (node.getInt("id")))
								existingSpouses.add(idToGenIdMap.get(idSpouseMap.getJSONObject(i).optInt("source")));
							else if (idSpouseMap.getJSONObject(i).optInt("source") == (node.getInt("id")))
								existingSpouses.add(idToGenIdMap.get(idSpouseMap.getJSONObject(i).optInt("target")));
						}
					}
					// Add new spouses to children JSONArray the selected node lives in
					JSONArray newSpouses = helper.getSpouses(session, valueList.get(spouseIdParameter)[0],
							(String[]) existingSpouses.toArray(new String[0]));
					if (newSpouses.length() > 0) {
						for (int i = 0; i < newSpouses.length(); i++) {
							String currentSpouseId = newSpouses.getJSONObject(i).getString("genID");

							Map<String, Set<String>> currentParentRelations = helper.cleanParentRelationsOfChild(
									helper.getParentChildRelationsOfChildren(session, currentSpouseId), idToGenIdMap,
									childRelations);
							Map<String, Set<String>> currentSpouseRelations = helper.cleanSpouseRelations(
									helper.getSpouseRelations(session, currentSpouseId), genIDToIdMap);
							// Add childRelation and spouseRelation between existing nodes and the new
							// spouses
							idToGenIdMap.put(helper.getIdCounter(), currentSpouseId);
							genIDToIdMap.put(currentSpouseId, helper.getIdCounter());
							// TODO Has parentflag to be set?
							helper.setNodeID(newSpouses.getJSONObject(i), true);

							JSONArray placeholderChildren = parentNode.optJSONArray("children");
							placeholderChildren.put(newSpouses.getJSONObject(i));
							parentNode.remove("children");
							parentNode.put("children", placeholderChildren);
							// Replace existing requestJSON, use copy of old version
							JSONObject changedTree = helper.replaceTreeNode(requestJSON, parentNode);
							changedTree = helper.addCountInformation(session,
									helper.getGenIdList(newSpouses.toString()), changedTree);
							requestJSON = changedTree;

							// Add new childRelations
							if (currentParentRelations.get(currentSpouseId) != null)
								for (String currentParent : currentParentRelations.get(currentSpouseId)) {
									JSONObject newRelation = new JSONObject();
									newRelation.put("source", genIDToIdMap.get(currentParent));
									newRelation.put("target", newSpouses.getJSONObject(i).getInt("id"));
									childRelations.put(newRelation);
								}
							// Add new spouseRelations
							for (String currentSpouse : currentSpouseRelations.get(currentSpouseId)) {
								JSONObject newRelation = new JSONObject();
								newRelation.put("source", node.getInt("id"));
								newRelation.put("target", genIDToIdMap.get(currentSpouse)
//										newSpouses.getJSONObject(i).getInt("id")
								);
								idSpouseMap.put(newRelation);
							}
							requestJSON.remove("childRelations");
							requestJSON.put("childRelations", childRelations);
							requestJSON.remove("spouses");
							requestJSON.put("spouses", idSpouseMap);
						}
						// Nodes additional to the selected one can be affected by additional parents
//						String[] genIDList = helper.getNonSelectedGenIdList(valueList.get(spouseIdParameter)[0],
//								idToGenIdMap, childRelations, requestJSON);
//
//						String[] parentIdList = new String[newSpouses.length()];
//						for (int i = 0; i < newSpouses.length(); i++)
//							parentIdList[i] = newSpouses.getJSONObject(i).getString("genID");
//
//						JSONArray existingParentChildRelations = requestJSON.getJSONArray("childRelations");
//						JSONArray existingSpouseRelations = requestJSON.getJSONArray("spouses");
//						// Fetch relations that only affect other nodes and the newly added parents
//						Map<String, Set<String>> siblingParentRelations = helper
//								.cleanParentRelationsOfChild(
//										helper.getParentChildRelationsOfChildren(session,
//												valueList.get(spouseIdParameter)[0], genIDList),
//										genIDToIdMap, parentIdList);
//						Map<String, Set<String>> spouseParentRelations = helper.cleanSpouseRelations(
//								helper.getSpouseRelations(session, genIDList), genIDToIdMap, parentIdList);
//						requestJSON = helper.moveChildrenToNewNodes(siblingParentRelations, requestJSON, node,
//								existingParentChildRelations, existingSpouseRelations, spouseParentRelations,
//								idToGenIdMap, genIDToIdMap);
						requestJSON = helper.moveChildrenToNewNodes(node.getString("genID"), childRelations, session,
								requestJSON, node, newSpouses, idToGenIdMap, genIDToIdMap);
						result = requestJSON;
					} else
						System.out.println("No new spouses");
				} else
					System.out.println("No parentNode of node with genID");

			}
		} else if (valueList.get(fetchSpousesCountFlag) != null) {
			if (valueList.get(spouseIdParameter) != null) {
				if (valueList.get(spouseIdParameter) != null) {
					JSONObject person = new JSONObject();
//					person.put("genID", valueList.get(spouseIdParameter)[0]);
					JSONArray spouseCount = helper.getSpouseCount(session, valueList.get(spouseIdParameter));
					person.put("spouseCount", spouseCount);
					result = person;
				}
			}
		} else if (valueList.get(fetchAliasFlag) != null) {
			if (valueList.get(aliasIdParameter) != null) {
				JSONObject person = new JSONObject();
				JSONArray aliases = null;
				person.put("genID", valueList.get(aliasIdParameter)[0]);
				aliases = helper.getAliasesByGenID(session, valueList.get(aliasIdParameter)[0]);
				person.put("aliases", aliases);
				result = person;
			}
		} else if (valueList.get(searchFlag) != null) {
			if (QueryHelper.parametersExist(searchFlag, valueList)) {
				JSONArray searchResults = null;
				if (valueList.get(skipOffset) != null && valueList.get(skipOffset).length == 1) {
					try {
						int offset = Integer.parseInt(valueList.get(skipOffset)[0]);
						searchResults = helper.searchPerson(session, valueList, offset);
					} catch (NumberFormatException e) {
						searchResults = helper.searchPerson(session, valueList);
					}
				} else
					searchResults = helper.searchPerson(session, valueList);

				if (searchResults != null && searchResults.length() > 0) {

					result.put("genID", nullObject);
					// Fetch metadata
					result.put("resultCount", searchResults.get(0));
					searchResults.remove(0);
					result.put("results", searchResults);
				}
			}
			// Get relatives of a list of specified genIDs
		} else if (valueList.get(fetchInitialTree) != null) {
			if (valueList.get(rootIdParameter) != null) {
				result = helper.fetchInitialTree(session, valueList.get(rootIdParameter)[0]);
			}
		}
		return result;
	}

//	 Adding new parent layers with ids above 1 prevents using that for a root node check
	protected boolean isParentLayer(String parentId, JSONObject node) {
		if (node.optJSONArray("children") != null)
			for (int i = 0; i < node.optJSONArray("children").length(); i++) {
				if (node.optJSONArray("children").getJSONObject(i).opt("genID") != null
						&& node.optJSONArray("children").getJSONObject(i).opt("genID").equals(parentId))
					return true;
			}
		return false;
	}

	/**
	 * True if node has parent node in tree
	 */
	protected boolean hasParentNode(JSONObject node, JSONArray childRelations, String genID) {
		// Search child relations for existing parent relation to selected node
		int nodeId = node.optInt("id", 0);
		// Should always be the case
		if (nodeId > 0) {
			if (childRelations != null) {
				for (int i = 0; i < childRelations.length(); i++) {
					// Node is target of child relation so it has to be in a subtree of its parents
					if (childRelations.getJSONObject(i).optInt("target") == nodeId)
						return true;
				}
			}
		}
		return false;
	}

	protected int getMaxId(String jsonString) {
		List<Integer> list = JsonPath.parse(jsonString).read("$..[?(@.id > 0)].id");
		// Find max id
		int max = 0;
		if (list.size() > 0) {
			for (int currentId : list) {
				if ((currentId) > max)
					max = currentId;
			}
		}
		return max;
	}

	protected int getNodeType(JSONObject node, JSONObject parentNode, JSONArray childRelations,
			Map<String, Integer> genIDToIdMap, Map<Integer, String> idToGenIDMap, String[] parentList,
			Map<String, String[]> valueList) {
		// TODO continue here 29.5 fix missing childRelations leading to false node type
		// Cases 1A, 1B, 2A and 2B
		if (childRelations != null) {
			if (parentNode.optJSONArray("parentIds") != null) {
				JSONArray parentIds = parentNode.getJSONArray("parentIds");
				String nodeId = node.getString("genID");
				for (int i = 0; i < parentIds.length(); i++) {
					boolean matchedRelation = false;
					for (int j = 0; j < childRelations.length(); j++)
						if (idToGenIDMap.get(childRelations.getJSONObject(j).getInt("source"))
								.equals(parentIds.getString(i))
								&& idToGenIDMap.get(childRelations.getJSONObject(j).getInt("target")).equals(nodeId)) {
							matchedRelation = true;
							break;
						}
					if (!matchedRelation)
						return QueryConstants.PLACEHOLDER_SPOUSE_NODE;
				}
				return QueryConstants.PLACEHOLDER_CHILD_NODE;
			} else if (!parentNode.optString("genID").isEmpty()) {
				for (int i = 0; i < childRelations.length(); i++)
					if (idToGenIDMap.get(childRelations.getJSONObject(i).getInt("source"))
							.equals(parentNode.getString("genID"))
							&& idToGenIDMap.get(childRelations.getJSONObject(i).getInt("target"))
									.equals(node.getString("genID")))
						return QueryConstants.CHILD_NODE;
				return QueryConstants.SPOUSE_NODE;
			}
		}
		return QueryConstants.ROOT_NODE;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		JSONObject json = new JSONObject();
		driver = getDriver();
		if (driver == null)
			return;
		if (session == null || !session.isOpen())
			session = getSession();
		if (session != null && session.isOpen()) {
			json = parseQuery(request);
			response.setStatus(200);
		} else
			response.setStatus(503);
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		// generator = Json.createGenerator(out);
		out.write(json.toString());
		out.flush();

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Tree data is parsed and returned with search results
		JSONObject json = new JSONObject();
		driver = getDriver();
		if (driver == null)
			return;
		if (session == null || !session.isOpen())
			session = getSession();
		if (session != null && session.isOpen()) {
			json = parseQuery(request, true);
			response.setStatus(200);
		} else
			response.setStatus(503);
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		// generator = Json.createGenerator(out);
		out.write(json.toString());
		out.flush();
	}

	public void destroy() {
		try {
			close();
		} catch (Exception e) {
			// TODO
		}
	}

	@Override
	public void close() throws Exception {
		if (session != null && session.isOpen())
			session.close();
		if (driver != null)
			driver.close();
	}
}
