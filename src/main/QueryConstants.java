package main;

public class QueryConstants {

	protected static final int defaultChildrenFetchLimit = 30;

	protected static final String returnSuffix1 = "";

	protected static final String searchPersonPrefix = "OPTIONAL MATCH (m)-[:CHILD_OF]->(person) "
			+ "OPTIONAL MATCH (o)-[:SPOUSE]->(person) "
			+ "OPTIONAL MATCH (person)-[:CHILD_OF]->()<-[:CHILD_OF]-(s) WITH person, count(distinct(m)) as childCount, count(distinct(o)) as spouseCount, count(distinct(s)) as siblingCount "
			+ "WHERE person IS NOT NULL";
	protected static final String searchPersonLabelInfix1 = " AND person.name=~\'.*(?i)";
	protected static final String searchPersonLabelInfix2 = ".*\'";
	// TODO not working as detection mechanism for gender not implemented
	protected static final String searchPersonGenderInfix = "";
	protected static final String searchPersonYearOfBirthInfix1 = " AND toInt(person.yearOfBirth)>=";
	protected static final String searchPersonYearOfBirthInfix2 = " AND toInt(person.yearOfBirth)<=";
	protected static final String searchPersonYearOfDeathInfix1 = " AND toInt(person.yearOfDeath)>=";
	protected static final String searchPersonYearOfDeathInfix2 = " AND toInt(person.yearOfDeath)<=";
	protected static final String searchPersonDateOfBirthInfix1 = " AND person.dateOfBirth>=\'";
	protected static final String searchPersonDateOfBirthInfix2 = " AND person.dateOfBirth<=\'";
	protected static final String searchPersonDateOfDeathInfix1 = " AND person.dateOfDeath>=\'";
	protected static final String searchPersonDateOfDeathInfix2 = " AND person.dateOfDeath<=\'";
	protected static final String searchPersonAgeInfix = " AND person.age=";
	protected static final String searchPersonChildrenExistInfix = " AND childCount>0";
	protected static final String searchPersonChildrenCountInfix = " AND childCount=";
	protected static final String searchPersonSpousesExistInfix = " AND spouseCount>0";
	protected static final String searchPersonSpousesCountInfix = " AND spouseCount=";
	protected static final String searchPersonSiblingsExistInfix = " AND siblingCount>0";
	protected static final String searchPersonSiblingsCountInfix = " AND siblingCount=";
	// TODO implement changing the fetch limits
	protected static final String searchPersonSuffix = " return count(person) as resultCount, collect(DISTINCT(person))[..30] as person LIMIT "
			+ defaultChildrenFetchLimit + ";";
	protected static final String searchPersonSkipSuffix1 = " return count(person) as resultCount, collect(DISTINCT(person))[";
	protected static final String searchPersonSkipSuffix2 = "..";
	protected static final String searchPersonSkipSuffix3 = "] as person LIMIT " + +defaultChildrenFetchLimit + ";";

	protected static final String fetchPersonByGenIDNoCountPrefix = "OPTIONAL MATCH (person:Person) WHERE person.genID=\'";
	protected static final String fetchPersonByGenIDNoCountSuffix = " RETURN person;";

	protected static final String fetchAliasesByGenIDPrefix = "OPTIONAL MATCH (aliasTarget:Person)<-[:SAME_AS]-(person:Person) WHERE aliasTarget.genID=\'";
	protected static final String fetchAliasesByGenIDSuffix = " RETURN collect(DISTINCT(person)) as person;";

	protected static final String fetchChildrenPrefix = "OPTIONAL MATCH (person:Person)-[:CHILD_OF]->(parent1:Person)";
	protected static final String fetchChildrenPrefix1 = "OPTIONAL MATCH (parent1:Person)<-[:CHILD_OF]-(person:Person)-[:CHILD_OF]->(parent2:Person)";
	protected static final String fetchChildrenDynamicPrefix = ", (parent%d:Person)<-[:CHILD_OF]-(person:Person)";
	protected static final String fetchChildrenDynamicInfix = " AND parent%d.genID=\'";
	protected static final String fetchChildrenInfix1 = " WHERE parent1.genID=\'";
	// For every existing child exclude the following nodes
	protected static final String fetchChildrenInfix2 = " AND parent2.genID=\'";
	protected static final String fetchChildrenInfix3 = " AND person.genID<>\'";
	protected static final String fetchChildrenSuffix = " RETURN collect(distinct(person))[..2] as person;";
	protected static final String fetchChildrenCountInfix = " WITH parent1, count(distinct(person)) as childCount";
	protected static final String fetchChildrenCountSuffix = " RETURN collect(parent1{.genID, childCount:childCount}) as childCount;";
	protected static final String fetchChildRelationsPrefix = "OPTIONAL MATCH (parent2:Person)<-[:CHILD_OF]-(person:Person)-[:CHILD_OF]->(parent1:Person) WHERE parent1.genID=\'";
	protected static final String fetchChildRelationsInfix1 = " AND(";
	protected static final String fetchChildRelationsInfix2 = " person.genID<>\'";
	protected static final String fetchChildRelationsInfix3 = " OR";
	protected static final String fetchChildRelationsSuffix = " RETURN collect({person: person.genID, parent1: parent1.genID, parent2: parent2.genID}) as childRelations";
	protected static final String fetchParentRelationsOfChildPrefix = "OPTIONAL MATCH (person:Person)-[:CHILD_OF]->(parent:Person) WHERE person.genID=\'%s\'";
	protected static final String fetchParentRelationsOfChildInfix = " OR person.genID=\'%s\'";
	protected static final String fetchParentRelationsOfChildSuffix = " RETURN collect({person: person.genID, parent: parent.genID}) as childRelations";

	protected static final String fetchSiblingsPrefix = "OPTIONAL MATCH (child:Person)-[:CHILD_OF]->(parent1:Person)<-[:CHILD_OF]-(person:Person)";
	protected static final String fetchSiblingsParentDependentPrefix = "OPTIONAL MATCH (parent:Person)<-[:CHILD_OF]-(person:Person)";
	protected static final String fetchSiblingsParentDependentInfix1 = " WHERE parent.genID in [\'%s\'";
	protected static final String fetchSiblingsParentDependentInfix2 = "  and person.genID<>\'%s\'";
	protected static final String fetchSiblingsDynamicPrefix = "OPTIONAL MATCH (child:Person)-[:CHILD_OF]->(parent%d:Person)<-[:CHILD_OF]-(person:Person)";
	protected static final String fetchSiblingsEndPrefix = " WHERE child.genID=\'%s\'";
	// For every existing sibling exclude the following nodes
	protected static final String fetchSiblingsInfix = " AND person.genID not in [\'%s\'";
	protected static final String fetchSiblingsInfix2 = " AND parent1.genID=\'";
	protected static final String fetchSiblingsInfix3 = ", \'%s\'";
	// For every parent to include create a dynamic string
	protected static final String fetchSiblingsDynamicInfix = " AND parent%d.genID=\'%s\'";
	protected static final String fetchSiblingsSuffix = " RETURN collect(DISTINCT(person))[..2] as person;";
	protected static final String fetchSiblingsCountInfix = " WITH child, count(distinct(person)) as siblingCount";

	protected static final String fetchSiblingsCountSuffix = " RETURN collect(child{.genID, siblingCount:siblingCount}) as siblingCount;";

	protected static final String fetchParentsPrefix = "OPTIONAL MATCH (child:Person)-[:CHILD_OF]->(person:Person)";
	protected static final String fetchParentsInfix = " WHERE child.genID='";
	// TODO
	protected static final String fetchParentCountInfix = " WITH child, count(distinct(person)) as parentCount";
	protected static final String fetchParentsSuffix1 = " AND person.genID<>'";
	protected static final String fetchParentsSuffix2 = " RETURN collect(DISTINCT(person))[..2] as person;";
	protected static final String fetchParentCountSuffix = " RETURN collect(distinct(child{.genID, parentCount:parentCount})) as parentCount;";
	protected static final String fetchParentRelationsPrefix = "OPTIONAL MATCH (child:Person)-[:CHILD_OF]->(person:Person)<-[:SPOUSE]-(otherSpouse:Person)<-[:CHILD_OF]-(child) with child, person, person.genID as spouse1, otherSpouse.genID as spouse2 ";
	protected static final String fetchParentRelationSuffix = "and spouse1<>spouse2 RETURN collect({spouse1: spouse1, spouse2: spouse2}) as parentRelations";

	protected static final String fetchSpousesPrefix = "OPTIONAL MATCH (person:Person)-[:SPOUSE]-(spouseTarget)";
	protected static final String fetchSpousesInfix1 = " WHERE person.genID='";
	// For every existing spouse exclude the following nodes
	protected static final String fetchSpousesInfix2 = " AND spouseTarget.genID<>\'";
	protected static final String fetchSpousesSuffix = " RETURN DISTINCT collect(distinct(spouseTarget)) as person LIMIT 2;";
	protected static final String fetchSpouseIdsSuffix = " RETURN DISTINCT collect(distinct(spouseTarget.genID)) as person;";
	protected static final String fetchSpousesCountInfix = " WITH person, count(distinct(spouseTarget)) as spouseCount";
	protected static final String fetchSpousesCountSuffix = " RETURN collect(person{.genID, spouseCount:spouseCount}) as spouseCount;";
	protected static final String fetchSpouseRelationsPrefix = "OPTIONAL MATCH (person:Person)-[:SPOUSE]->(spouse:Person) WHERE person.genID=\'%s\'";
	protected static final String fetchSpouseRelationsInfix = " OR person.genID=\'%s\'";
	protected static final String fetchSpouseRelationsSuffix = " RETURN collect({spouse1: person.genID, spouse2: spouse.genID}) as spouseRelations";

	protected static final int PLACEHOLDER_CHILD_NODE = 0x1A;
	protected static final int PLACEHOLDER_SPOUSE_NODE = 0x1B;
	protected static final int CHILD_NODE = 0x2A;
	protected static final int SPOUSE_NODE = 0x2B;
	protected static final int ROOT_NODE = 0x30;
}
