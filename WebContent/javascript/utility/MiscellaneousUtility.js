/**
 * Contains miscellaneous utility functions
 */

/**
 * Shortens a given string to a given length and adds "..." add the end
 * @param givenString the given string
 * @param maxLength the maximum length
 * @returns the resulting string that is shortened
 */
function shortenString(givenString, maxLength) {
	let shortened = '';
	if (givenString.length > maxLength) {
		shortened = givenString.substring(0, maxLength) + '...'
	} else {
		shortened = givenString;
	}
	return shortened;
}

/**
 * Used for empty json fields, show "-" instead of "undefined"
 * @param value
 * @returns
 */
function replaceUndefinedValue(value) {
	if (value === undefined) {
		return "-";
	} else {
		return value;
	}
}

// https://techoverflow.net/2019/05/03/string-in-die-zwischenablage-kopieren-mit-javascript-ohne-bibliotheken/?lang=de
function copyStringToClipboard(str) {
	// Temporäres Element erzeugen
	var el = document.createElement('textarea');
	// Den zu kopierenden String dem Element zuweisen
	el.value = str;
	// Element nicht editierbar setzen und aus dem Fenster schieben
	el.setAttribute('readonly', '');
	el.style = {
		position : 'absolute',
		left : '-9999px'
	};
	document.body.appendChild(el);
	// Text innerhalb des Elements auswählen
	el.select();
	// Ausgewählten Text in die Zwischenablage kopieren
	document.execCommand('copy');
	// Temporäres Element löschen
	document.body.removeChild(el);
}

/**
 * Show a notification to the user that indicates that something was successful. The notification disposes itself.
 * @param message the message that is shown to the user
 * @returns nothing
 */
function notifySuccessful(message) {
	$.notify({
		message: message
	}, {
		type: 'success',
		z_index: 10001,
		offset: 8,
		placement: {
			from: "top",
			align: "center"
		},
		newest_on_top: true
	});
}

/**
 * Show a notification to the user that indicates that something went wrong. The notification disposes itself.
 * @param message the message that is shown to the user
 * @returns nothing
 */
function notifyError(message) {
	$.notify({
		message: message
	}, {
		type: 'danger',
		z_index: 10001,
		offset: 8,
		placement: {
			from: "top",
			align: "center"
		},
		newest_on_top: true
	});
}

function offsetRelativeToDocument(el) {
    var rect = el.getBoundingClientRect(),
    scrollLeft = window.pageXOffset || document.documentElement.scrollLeft,
    scrollTop = window.pageYOffset || document.documentElement.scrollTop;
    return { top: rect.top + scrollTop, left: rect.left + scrollLeft }
}