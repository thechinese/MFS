/**
 * Contains functions that are concerned with the navigation elements of the app, 
 * e. g. basic GUI, display of search results etc.
 */

/***
 * Slider Values
 */
var sliderBirth = document.getElementById("timeRangeBirth");
var outputBirth = document.getElementById("BirthValue");
outputBirth.innerHTML = sliderBirth.value;
sliderBirth.oninput = function() {
	outputBirth.innerHTML = this.value;
}

var sliderDeath = document.getElementById("timeRangeDeath");
var outputDeath = document.getElementById("DeathValue");
outputDeath.innerHTML = outputDeath.value;
sliderDeath.oninput = function() {
	outputDeath.innerHTML = this.value;
}
outputDeath.innerHTML = "2101"; // otherwise 'undefined' at start for some reason

/**
 * Sets the tab with the given id as active
 * @param tabId the given id of the tab that should be set to active
 * @returns
 */
function activateTab(tabId) {
    $('.nav-tabs a[href="' + tabId + '"]').tab('show');
};

function setName(count, name, maxLength) {
	// limit length
	var shortened = shortenString(name, maxLength);
	
	var divName = document.createElement("div");
	divName.id = "result" + count + "name"
	divName.className = "row";
	divName.innerHTML = "<b>Name:&nbsp</b>" + shortened;
	divName.title = name;
	divName.style = "cursor: pointer;";
	
	let copiedMessage = "Zur Zwischenablage hinzugefügt";
	divName.addEventListener("click", function() {
		copyStringToClipboard(name);
		notifySuccessful(copiedMessage);
	})
	
	return divName;
}

function setFamilyName(count, familyName) {
	var divName = document.createElement("div");
	divName.id = "result" + count + "familyName"
	divName.className = "row";
	divName.innerHTML = "<b>Familienname:&nbsp</b>" + familyName;
	return divName;
}

function setGenID(count, genID) {
	var divName = document.createElement("div");
	divName.id = "result" + count + "genID"
	divName.className = "row";
	divName.innerHTML = "<b>GenID:&nbsp</b>" + genID;
	return divName;
}

function setFile(count, file) {
	var divName = document.createElement("div");
	divName.id = "result" + count + "file"
	divName.className = "row";
	divName.innerHTML = "<b>File:&nbsp</b>" + file;
	return divName;
}

function setBirth(count, birth) {
	var divName = document.createElement("div");
	divName.id = "result" + count + "birth"
	divName.className = "row";
	divName.innerHTML = "<b>Geburt:&nbsp</b>" + birth; // not Geburtsjahr/-datum because also random info in this field related to birth
	return divName;
}

function setDeath(count, death) {
	var divName = document.createElement("div");
	divName.id = "result" + count + "death"
	divName.className = "row";
	divName.innerHTML = "<b>Tod:&nbsp</b>" + death; // not Todesjahr/-datum because also random info in this field related to death
	return divName;
}

function setAge(count, age) {
	var divName = document.createElement("div");
	divName.id = "result" + count + "age"
	divName.className = "row";
	divName.innerHTML = "<b>Alter:&nbsp</b>" + age;
	return divName;
}

function setWikiSearch(count, name) {
	var divWiki = document.createElement("div");
	divWiki.id = "result" + count + "wiki";
	divWiki.align = "right";

	var divLink = document.createElement("a");
	divLink.href = "https://de.wikipedia.org/w/index.php?search=" + name;
	divLink.target = "_blank";
	divLink.innerHTML = "Wikipedia-Suche";

	divWiki.appendChild(divLink);
	return divWiki;
}

function setGraph(resultNumber, name, genID, file, birth, death, age) {
	var divGraphLink = document.createElement("div");

	var divLink = document.createElement("button");
//	divLink.href = genID;
	divLink.innerHTML = "Stammbaum öffnen";
	divLink.className = "openFamilyTreeButton";
	$(divLink).on("click", function () {
		// open sidebar
		$(".resultbar").removeClass("active");
		// fade in the overlay
		$(".overlay").removeClass("active");
		$(".collapse.in").toggleClass("in");
		$("a[aria-expanded=false]").attr("aria-expanded", "true");
		
		openNewResultTab(resultNumber, name, genID, file, birth, death, age);
	});

	divGraphLink.appendChild(divLink);
	return divGraphLink;
}