/**
 * This contains the server ip address where this application is deployed
 */

/**
 * This should be used to concatenate the needed URLs for the servlet queries.
 * 
 * Example:
 * let requestURL = SERVER_IP + '/Servlet?fetchTree&rootId=' + genID;
 */
// var SERVER_IP = 'http://localhost:8080/MFS';
//var SERVER_IP = 'http://129.69.215.60/MFS';
var SERVER_IP = '../MFS';