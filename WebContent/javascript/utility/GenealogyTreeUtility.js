/**
 * Helper functions for the family tree
 * mostly for calculating positions of various elements
 */

/**
 * Calculates fitting dimensions for the tree that would result from the given json
 * @param json the json containing the data for the tree
 * @returns the fitting x- and y-dimensions
 */
function treeDimensionForJSON(json) {
	let xDimPerNode = 370 + 180;
	let yDimPerNode = 141 + 150;
	
	let getNumNodesAtLevel = function(node, curr, desired) {
		if (curr === (desired - 1)) {
			if (node.children) {
				return node.children.length;
			} else {
				return 0;
			}
		}

		var count = 0;
		if (node.children !== undefined && node.children !== null) {
			node.children.forEach(function(child) {
				count += getNumNodesAtLevel(child, curr + 1, desired);
			});
		}
		return count;
	};
	
	let getDepth = function (obj) {
	    var depth = 0;
	    if (obj.children) {
	        obj.children.forEach(function (d) {
	            var tmpDepth = getDepth(d)
	            if (tmpDepth > depth) {
	                depth = tmpDepth
	            }
	        })
	    }
	    return 1 + depth
	}
	
	let yLevelCount = getDepth(json);
	let xLevelCount = -1;
	for (let i = 0; i < yLevelCount; i++) {
		let currentLevelSize = getNumNodesAtLevel(json, 0, i);
		if (xLevelCount < currentLevelSize) {
			xLevelCount = currentLevelSize;
		}
	}
	
	let resultX = (xDimPerNode * xLevelCount) + Math.pow(2, Math.min((yLevelCount + 5), (7 + 5)));
	// add a exponentially growing element, so that nodes don't get to crowded in special cases
	// (left/right-linear, imbalanced trees)
	
	let resultY = yDimPerNode * yLevelCount;
	
	return {x: resultX, y: resultY};
}

var kx = function(d) {
	return d.x - 100;
};
var ky = function(d) {
	return d.y - 15;
};
// the place the text x axis adjust this to center align the text
var tx = function(d) {
	return d.x - 92;
};
// the place the text y axis adjust this to center align the text
var ty = function(d) {
	return d.y + 5;
};
// position for buttons for further interactions in nodes
var kxButton1 = function(d) {
	return d.x + 167;
};
var kxButton2 = function(d) {
	return d.x + 192;
};
var kxButton3 = function(d) {
	return d.x + 217;
};
var kxButton4 = function(d) {
	return d.x + 242;
};
var kyButton = function(d) {
	return d.y + 99;
};
// position for button labels
var kxButtonLabel1 = function(d) {
	return d.x + 167 + 6;
};
var kxButtonLabel2 = function(d) {
	return d.x + 192 + 5;
};
var kxButtonLabel3 = function(d) {
	return d.x + 217 + 6;
};
var kxButtonLabel4 = function(d) {
	return d.x + 242 + 6;
};
var kyButtonLabel = function(d) {
	return d.y + 99 + 16;
};