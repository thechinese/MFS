/**
 * Here the basic GUI elements are handled, e. g. buttons, search bars, tabs etc.
 */

/**
 * counter for all tabs that were ever opened, needed for correct management and display of tab headers and tab contents
 */
var ALL_TABS_EVER_COUNT = 0;

// Sidebar Toggle
$(document).ready(function () {
	$("#sidebarCollapse").on("click", function () {
		$("#sidebar").toggleClass("active");
		$(this).toggleClass("active");
	});
});

$(document).ready(function () {
	$(".resultbar").mCustomScrollbar({
		theme: "minimal"
	});

	$("#dismiss, .overlay").on("click", function () {
		// hide sidebar
		$(".resultbar").removeClass("active");
		// hide overlay
		$(".overlay").removeClass("active");
	});

	// Sidebar Results
	$("#resultbarCollapse").on("click", function () {
		// open sidebar
		$(".resultbar").addClass("active");
		// fade in the overlay
		$(".overlay").addClass("active");
		$(".collapse.in").toggleClass("in");
		$("a[aria-expanded=true]").attr("aria-expanded", "false");
	});
});


/**
 * Listener for the search button: Take search parameters, execute search and display results in new tab
 * @returns
 */
$('#button_search').click(function() {
	// feedback for the user that search is running
	$("#sidebar").addClass("disabled");
	$("#button_search").html("Suche läuft... ⏳");
	
	// the search parameters
	var search_name = document.getElementById("search_name").value;
	var search_childrencount = document.getElementById("search_childrencount").value;
	var search_siblingcount = document.getElementById("search_siblingcount").value;

	// Parameters used when querying with year data  
	var search_birthdate = sliderBirth.value;
	var search_deathdate = sliderDeath.value;
		
	// build the search query
	var requestURLPrefix = SERVER_IP + '/Servlet?search';
	var requestURL = requestURLPrefix;
	if (search_name !== '') {
		requestURL += "&name=" + search_name;
	}
	if (document.getElementById("SearchFilter").className==("collapse show")) {
		if (search_childrencount !== '' && !isNaN(search_childrencount)) {
			requestURL += "&childNo=" + search_childrencount;
		}
		if (search_siblingcount !== '' && !isNaN(search_siblingcount)) {
			requestURL += "&sibilingNo=" + search_siblingcount;
		}
		if (!isNaN(search_birthdate) && !isNaN(search_deathdate) && search_birthdate !== '' && search_deathdate !== '') {
			requestURL += "&yearOfBirth=" + search_birthdate + "&yearOfDeath=" + search_deathdate;
		}
	}
	if (requestURL == requestURLPrefix) {
		// Nothing to search for
		return;
	}
	
	// for testing
	console.log(requestURL);
	
	// send the request
	var currentResults = [];
	var resultPageNr = 0;
	var dataJSON;
	let xmlhttp = new XMLHttpRequest();
	xmlhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			dataJSON = JSON.parse(this.responseText);
			
			$("#searchresults_content").empty();
			
			// the JSON shouldn't be empty, it should contain at least the resultCount information
			// if empty, something went wrong
			if (jQuery.isEmptyObject(dataJSON)) {
				throw "The received JSON from the server is empty!";
				notifyError("Da ist etwas schiefgelaufen! Leere Antwort vom Server... versuchen Sie es später noch einmal.");
				
				enableSearchBarAndResetSearchButton();
				
				return;
			}
			
			// resultCount is bigger than 0 (normal case)
			if (dataJSON.resultCount.resultCount > 0) {
				document.getElementById("searchresults_number").innerHTML = 
					"Anzahl der Treffer: " + dataJSON.resultCount.resultCount;
				var currentResultCounter = 1;
				
				// initial recursive helper function call
				recursiveAddSearchResultsToGUI(dataJSON, currentResults, currentResultCounter, resultPageNr, requestURL);
				
				// close the search bar and open the sidebar with the search results
				document.getElementById("sidebarCollapse").click();
				document.getElementById("resultbarCollapse").click();
				
				enableSearchBarAndResetSearchButton();
			
			// resultCount is 0, so no results found, show according hint to user
			} else {
				document.getElementById("searchresults_number").innerHTML = "Anzahl der Treffer: 0";
				
				var noResultDiv = document.createElement("div");
				noResultDiv.innerHTML = "Ihre Suche ergab keine Treffer!";
				noResultDiv.className = "card card-body";

				document.getElementById("searchresults_content").appendChild(noResultDiv);
				document.getElementById("sidebarCollapse").click();
				document.getElementById("resultbarCollapse").click();
				
				enableSearchBarAndResetSearchButton();
			}
		
		// the server could not be reached, or some other server/connection problem
		// https://www.restapitutorial.com/httpstatuscodes.html
		} else if (this.status >= 300) {
			notifyError("Keine Antwort vom Server... aktualisieren Sie die Seite mit STRG + F5 und versuchen Sie es noch einmal.");
			enableSearchBarAndResetSearchButton();
		}
	};
	xmlhttp.open("GET", requestURL, true);
	xmlhttp.send();
	
	// helper function that enables the search bar again, and resets the label for the search button
	// to show to the user that web app is ready for new search
	function enableSearchBarAndResetSearchButton() {
		// enable search bar again and make search button normal again
		$("#sidebar").removeClass("disabled");
		$("#button_search").html("Suche starten");
	}
});

// recursive helper function to add the search results to the results sidebar
function recursiveAddSearchResultsToGUI(dataJSON, currentResults, currentResultCounter, resultPageNr, requestURL) {
	// remove the button for getting more results
	// if there are still more results, then it is added again later (see further down)
	$('.expandSearchResultsButton').remove();
	
	dataJSON.results.forEach(result => currentResults.push(result));
	
	for (let i = 0; i < dataJSON.results.length; i++) {
		let name 	= replaceUndefinedValue(dataJSON.results[i].name);
		let genID 	= dataJSON.results[i].genID; // in this case show "undefined" in UI, if really undefined
		let file 	= replaceUndefinedValue(dataJSON.results[i].file);
		let birth 	= replaceUndefinedValue(dataJSON.results[i].born);
		let death 	= replaceUndefinedValue(dataJSON.results[i].died);
		let age 	= replaceUndefinedValue(dataJSON.results[i].age);
		let tempResult = newResult(currentResultCounter, name, genID, file, birth, death, age);
		
		document.getElementById("searchresults_content").appendChild(tempResult);
		currentResultCounter++;
	}
	
	// if there are more results, show a button to get more results
	if (currentResults.length < dataJSON.resultCount.resultCount) {
		let expandSearchResultsButton = document.createElement("button");
		expandSearchResultsButton.className = "expandSearchResultsButton";
		expandSearchResultsButton.innerHTML = "Weitere Suchergebnisse laden";
		document.getElementById("searchresults_content").appendChild(expandSearchResultsButton);
		
		// button listener
		$(expandSearchResultsButton).on("click", function() {
			// immediately disable the button after click, so it can only be clicked once
			$(expandSearchResultsButton).attr("disabled", true);
			$(expandSearchResultsButton).html("Wird geladen... ⏳");
			
			// send the request to get more results and add them to the result sidebar
			resultPageNr++;
			let newRequestURL = requestURL + "&page=" + resultPageNr;
			
			// for testing
			console.log(newRequestURL);
			
			var newDataJSON;
			let xmlhttp = new XMLHttpRequest();
			xmlhttp.onreadystatechange = function () {
				if (this.readyState == 4 && this.status == 200) {
					newDataJSON = JSON.parse(this.responseText);
					
					if (jQuery.isEmptyObject(newDataJSON) == false) {
						// recursive call of function (with according new/old parameters)
						recursiveAddSearchResultsToGUI(newDataJSON, currentResults, currentResultCounter, resultPageNr, requestURL);
					} else {
						throw "The received JSON from the server is empty!";
						notifyError("Da ist etwas schiefgelaufen! Leere Antwort vom Server... versuchen Sie es später noch einmal.");

						// restore the button label and enable it again
						$(expandSearchResultsButton).attr("disabled", false);
						$(expandSearchResultsButton).html("Weitere Suchergebnisse laden");
						
						// take back the incrementation, so if the button is clicked again in this error case
						// no results are skipped
						resultPageNr--;
					}
					
				} else if (this.status >= 300) {
					notifyError("Keine Antwort vom Server... versuchen Sie es später noch einmal.");

					// restore the button label and enable it again
					$(expandSearchResultsButton).attr("disabled", false);
					$(expandSearchResultsButton).html("Weitere Suchergebnisse laden");
					
					// take back the incrementation, so if the button is clicked again in this error case
					// no results are skipped
					resultPageNr--;
				}
			};
			xmlhttp.open("GET", newRequestURL, true);
			xmlhttp.send();
		});
	}
}

// Zwischenergebnisse 
function newResult(count, name, genID, file, birth, death, age) {
	var resultDiv = document.createElement("div");
	resultDiv.id = "result" + count;
//	resultDiv.style = ""

	resultDiv.innerHTML = '<h6 style="margin-left:auto;margin-right:auto;font-weight: bold;">Suchergebnis ' + count + '</h6>';
	resultDiv.className = "card card-body intermediateSearchResult";

	var divName  = setName(count, name, 80);
	var divGenID = setGenID(count, genID);
	var divFile  = setFile(count, file);
	var divBirth = setBirth(count, birth);
	var divDeath = setDeath(count, death);
	var divAge 	 = setAge(count, age);
	var divWiki  = setWikiSearch(count, name);
	var divGraphLink = setGraph(count, name, genID, file, birth, death, age);

	resultDiv.appendChild(document.createElement("hr"));
	
	resultDiv.appendChild(divName);
	resultDiv.appendChild(divGenID);
	resultDiv.appendChild(divFile);
	resultDiv.appendChild(divBirth);
	resultDiv.appendChild(divDeath);
	resultDiv.appendChild(divAge);
	
	resultDiv.appendChild(document.createElement("hr"));
	
	resultDiv.appendChild(divWiki);
	resultDiv.appendChild(divGraphLink);
	return resultDiv;
}

/**
 * Function to open a new tab with results from respective search query
 * @returns
 */
function openNewResultTab(resultNumber, name, genID, file, birth, death, age) {
	// increase the global tab count by 1
	ALL_TABS_EVER_COUNT++;
	
	// the new tab header
	var newTabItem = document.createElement("li");
	newTabItem.className = "nav-item";

	// the 'label' of the tab header
	var newLink = document.createElement("a");
	newLink.className = "nav-link";
	newLink.href = "#search" + ALL_TABS_EVER_COUNT;
	$(newLink).addClass("role");
	$(newLink).attr("role", "tab");
	$(newLink).addClass("data-toggle");
	$(newLink).attr("data-toggle", "tab");

	// name of the tab
	let shortenedName = shortenString(name, 30);
	newLink.innerHTML = "<b>Suchergebnis " + resultNumber + ":&nbsp</b>" + shortenedName;
	newLink.title = name;
	
	// close button for the tab (added to the 'label' from above)
	var closeButton = document.createElement("button");
	$(closeButton).addClass("close");
	$(closeButton).addClass("closeTab");
	closeButton.type = "button";
	closeButton.innerHTML = "×";
	newLink.appendChild(closeButton);
	$(closeButton).click(function() {
		//there are multiple elements which has .closeTab icon so close the tab whose close icon is clicked
	    var tabContentId = $(this).parent().attr("href");
	    $(this).parent().parent().remove(); //remove li of tab
	    $('#myTab a:last').tab('show'); // Select first tab
	    $(tabContentId).remove(); //remove respective tab content
	});

	// add all elements needed for the tab header
	newTabItem.appendChild(newLink);
	document.getElementById("searchTabs").appendChild(newTabItem);

	// the actual content container of the tab
	var newTabContent = document.createElement("div");
	newTabContent.className = "tab-pane";
	newTabContent.id = "search" + ALL_TABS_EVER_COUNT;
	document.getElementById("tab-contents").appendChild(newTabContent);
	newTabContent.style = "width: 100%;";
	
	// show information about the result person within the tab
	let tabContentHeader = document.createElement("h5");
	tabContentHeader.innerHTML = "Suchergebnis " + resultNumber;
	newTabContent.appendChild(tabContentHeader);
	
	let chosenResultCard = document.createElement("div");
	chosenResultCard.className = "chosenResultCard";
	newTabContent.appendChild(chosenResultCard);
	
	let divName = setName(ALL_TABS_EVER_COUNT, name, Number.POSITIVE_INFINITY);
	divName.className = "chosenResultCardSubDiv";
	chosenResultCard.appendChild(divName);
	
	let divGenID = setGenID(ALL_TABS_EVER_COUNT, genID);
	divGenID.className = "chosenResultCardSubDiv";
	chosenResultCard.appendChild(divGenID);
	
	let divFile = setFile(ALL_TABS_EVER_COUNT, file);
	divFile.className = "chosenResultCardSubDiv";
	chosenResultCard.appendChild(divFile);
	
	let divBirth = setBirth(ALL_TABS_EVER_COUNT, birth);
	divBirth.className = "chosenResultCardSubDiv";
	chosenResultCard.appendChild(divBirth);
	
	let divDeath = setDeath(ALL_TABS_EVER_COUNT, death);
	divDeath.className = "chosenResultCardSubDiv";
	chosenResultCard.appendChild(divDeath);
	
	let divAge = setAge(ALL_TABS_EVER_COUNT, age);
	divAge.className = "chosenResultCardSubDiv";
	chosenResultCard.appendChild(divAge);
	
	// show feedback for user that the application is loading
	let loadFeedbackSpan = document.createElement("span");
	loadFeedbackSpan.innerHTML = "<i>Stammbaum wird geladen...</i> ⏳";
	newTabContent.appendChild(loadFeedbackSpan);
	
	// set the new tab as active
	activateTab("#search" + ALL_TABS_EVER_COUNT);
	
	// send query for the initial tree data to DB via servlet
	var dataJSON;
	let xmlhttp = new XMLHttpRequest();
	let requestURL = SERVER_IP + '/Servlet?fetchTree&rootId=' + genID;
	
	// for testing
	console.log(requestURL);
	
	xmlhttp.onreadystatechange = function () {
		if (this.readyState == 4 && this.status == 200) {
			dataJSON = JSON.parse(this.responseText);
			
			// for testing
			console.log(dataJSON);
			
			// add the corresponding genealogical tree
			buildInitialTree(genID, newTabContent, dataJSON, ALL_TABS_EVER_COUNT);
			
			// remove the loading feedback
			newTabContent.removeChild(loadFeedbackSpan);
		}
	};
	xmlhttp.open("GET", requestURL, true);
	xmlhttp.send();
}