/**
 * Contains everything that is related to the family tree functionalities
 */

/**
 * Builds the initial family tree for the given genID of a person,
 * and adds it to the given container
 * @param genID the given genID of the person
 * @param newTabContent the given container
 * @returns
 */
function buildInitialTree(genID, newTabContent, root, tabIndexNumber) {
	// the container for the svg for the family tree
	let divForSVG = document.createElement("div");
	newTabContent.appendChild(divForSVG);
	divForSVG.style = "width: 100%; flex-grow: 1; background: white; position: relative;";
		
	// make an SVG
	var svg = d3.select(divForSVG).append("svg")
		.attr("width", '100%')
		.attr("height", '100%')
		.attr("class", "familty-tree-svg")
		.call(d3.zoom()
//			.filter(() => {return 
//				(d3.event.ctrlKey);
//			})
			.on('zoom', function(d) { svg.attr('transform', d3.event.transform); })
		)
		.append("g");
	
	function childrenMissing(id, childCount){
		if(childCount == 0)
			return 0;
		var existingRelations = 0;
		relations = root.childRelations;
		for (var i = 0; i < relations.length; i++)
			if(relations[i].source == id)
				existingRelations ++;
		return childCount > existingRelations;
	}
	function siblingsMissing(id, siblingCount){
		if(siblingCount == 0)
			return 0;
		var existingRelations = 0;
		var parentIds = [];
		var siblingIds = [];
		relations = root.childRelations;
		for (var i = 0; i < relations.length; i++)
			if(relations[i].target == id)
				parentIds.push(relations[i].source);
		for (var i = 0; i < relations.length; i++)
			if(parentIds.includes(relations[i].source) && relations[i].target != id)
				siblingIds.push(relations[i].target);
		existingRelations = siblingIds.filter(function(val, i, arr) { 
    		return arr.indexOf(val) === i;}).length;
		return siblingCount > existingRelations;
	}
	function parentsMissing(id, parentCount){
		if(parentCount == 0)
			return 0;
		var existingRelations = 0;
		relations = root.childRelations;
		for (var i = 0; i < relations.length; i++)
			if(relations[i].target == id)
				existingRelations ++;
		return parentCount > existingRelations;
	}
	function spousesMissing(id, spouseCount){
		if(spouseCount == 0)
			return 0;
		var existingRelations = 0;
		relations = root.spouses;
		for (var i = 0; i < relations.length; i++)
			// Only one pair of the redundant relation is stored in the json
			if(relations[i].source == id || relations[i].target == id)
				existingRelations ++;
		return spouseCount > existingRelations;		
	}
	function drawTree() {
		var width = treeDimensionForJSON(root).x, height = treeDimensionForJSON(root).y
		
		/* For the drop shadow filter... */
		// use this filter for adding shadow effects, e. g. to rects
		let defs = svg.append("defs");
		
		let shadowID = "dropshadow" + tabIndexNumber;
		let shadowURL = "url(#dropshadow" + tabIndexNumber + ")"
		let filter = defs.append("filter")
			.attr("id", shadowID)
		
		filter.append("feGaussianBlur")
			.attr("in", "SourceAlpha")
			.attr("stdDeviation", 5)
			.attr("result", "blur");
		filter.append("feOffset")
			.attr("in", "blur")
			.attr("dx", 3)
			.attr("dy", 3)
			.attr("result", "offsetBlur");
		
		let feMerge = filter.append("feMerge");
		
		feMerge.append("feMergeNode")
			.attr("in", "offsetBlur")
		feMerge.append("feMergeNode")
			.attr("in", "SourceGraphic");
		
		// My JSON note the
		// no_parent: true this ensures that the node will not be linked to its parent
		// hidden: true ensures that the nodes is not visible.
	//	var root = {}
	
		var spouses = root.spouses;
		if (spouses === undefined) {
			spouses = [];
		}
	
	//	delete root.spouses;
	
		var allNodes = flatten(root);
		// This maps the spouses together mapping uses the ID using the blue line
		// TODO leave (maybe superfluous) id object in or remove it?
	
		// Compute the layout.
		var tree = d3.layout.tree().size([ width, height ]), nodes = tree.nodes(root), links = tree
				.links(nodes);
	
		// Create the link lines.
		svg.selectAll(".link").data(links).enter().append("path").attr("class", "link")
				.attr("d", elbow);
	
		var nodes = svg.selectAll(".node").data(nodes).enter();
	
		// First draw spouse line with blue line
		svg.selectAll(".spouse").data(spouses).enter().append("path").attr("class",
				"spouse").attr("d", spouseLine);
		
		var nodeGroup = nodes.append("g").attr("class", "nodeGroup")
			.on("mouseenter", function(d) {
				d3.select(this).raise();
			});
		
		// Create the node rectangles.
		nodeGroup.append("rect").attr("class", "node").attr("height", 141)
				.attr("width", 370)
				.style("fill", function(d) {
					// if we have the searched person, than add special color for better highlight
					if (d.genID === genID) {
						return "rgba(166, 206, 227, 0.71)";
					} else {
						return "rgba(255,255,255, 0.95)";
					}
					
				}).attr("id", function(d) {
					return d.id;
				}).attr("display", function(d) {
					if (d.hidden) {
						return "none"
					} else {
						return ""
					}
				}).attr("x", kx).attr("y", ky)
				.attr("rx", 5).attr("ry", 5) // rounded corners for the rect
				.attr("filter", shadowURL)
				
		// Create the node text label.
		let maxTextLabelLength = 40;
		let copiedMessage = "Zur Zwischenablage hinzugefügt";
		// the name label
		nodeGroup.append("text").text(function(d) {
				if (d.hidden !== undefined && d.hidden !== null && d.hidden === true) {
					return "";
				} else {
					if (d.name !== null && d.name !== undefined && d.name !== "") {
						return shortenString(d.name, maxTextLabelLength);
					} else {
						return "Name: -";
					}
				}
			})
			.on("click", function(d) {
				copyStringToClipboard(d.name);
				notifySuccessful(copiedMessage);
			})
			.attr("class", "mouseHoverPointer")
			.attr("x", tx).attr("y", ty)
			.attr("font-weight", "bold")
			.append("svg:title").text(function(d, i) { return d.name; })
	
		// the genID label
		nodeGroup.append("text").text(function(d) {
			if (d.hidden !== undefined && d.hidden !== null && d.hidden === true) {
				return "";
			} else {
				if (d.genID !== null && d.genID !== undefined) {
					return "GenID: " + d.genID;
				} else {
					return "GenID: -";
				}
			}
		})
		.on("click", function(d) {
				copyStringToClipboard(d.genID);
				notifySuccessful(copiedMessage);
			})
		.attr("class", "mouseHoverPointer")
		.attr("x", tx).attr("y", ty).attr("dy", "1.1em")
		.append("svg:title").text(function(d, i) { return d.genID; })
		
		// the file label
		nodeGroup.append("text").text(function(d) {
				if (d.hidden !== undefined && d.hidden !== null && d.hidden === true) {
					return "";
				} else {
					if (d.file !== null && d.file !== undefined) {
						return "File: " + shortenString(d.file, maxTextLabelLength);
					} else {
						return "File: -";
					}
				}
			})
			.on("click", function(d) {
				copyStringToClipboard(d.file);
				notifySuccessful(copiedMessage);
			})
			.attr("class", "mouseHoverPointer")
			.attr("x", tx).attr("y", ty).attr("dy", "2.2em")
			.append("svg:title").text(function(d, i) { return d.file; })
			
		// the birth label
		nodeGroup.append("text").text(function(d) {
				if (d.hidden !== undefined && d.hidden !== null && d.hidden === true) {
					return "";
				} else {
					if (d.born !== null && d.born !== undefined) {
						return "Geburt: " + shortenString(d.born, maxTextLabelLength);
					} else {
						return "Geburt: -";
					}
				}
			})
			.on("click", function(d) {
				copyStringToClipboard(d.born);
				notifySuccessful(copiedMessage);
			})
			.attr("class", "mouseHoverPointer")
			.attr("x", tx).attr("y", ty).attr("dy", "3.3em")
			.append("svg:title").text(function(d, i) { return d.born; })
			
		// the death label
		nodeGroup.append("text").text(function(d) {
				if (d.hidden !== undefined && d.hidden !== null && d.hidden === true) {
					return "";
				} else {
					if (d.died !== null && d.died !== undefined) {
						return "Tod: " + shortenString(d.died, maxTextLabelLength);
					} else {
						return "Tod: -";
					}
				}
			})
			.on("click", function(d) {
				copyStringToClipboard(d.died);
				notifySuccessful(copiedMessage);
			})
			.attr("class", "mouseHoverPointer")
			.attr("x", tx).attr("y", ty).attr("dy", "4.4em")
			.append("svg:title").text(function(d, i) { return d.died; })
			
		// the age label
		nodeGroup.append("text").text(function(d) {
				if (d.hidden !== undefined && d.hidden !== null && d.hidden === true) {
					return "";
				} else {
					if (d.age !== null && d.age !== undefined) {
						return "Alter: " + shortenString(d.age, maxTextLabelLength);
					} else {
						return "Alter: -";
					}
				}
			})
			.on("click", function(d) {
				copyStringToClipboard(d.age);
				notifySuccessful(copiedMessage);
			})
			.attr("class", "mouseHoverPointer")
			.attr("x", tx).attr("y", ty).attr("dy", "5.5em")
			.append("svg:title").text(function(d, i) { return d.age; })
		
		
		// create buttons for further interaction
		let buttonHeight = 21;
		let buttonWidth = 21;
		let roundCornerX = 3;
		let roundCornerY = 3;
		let titleParentButtonTooltip = "Weitere Elternteile laden";
		let titleSiblingButtonTooltip = "Weitere Geschwister laden";
		let titleSpousesButtonTooltip = "Weitere Ehepartner laden";
		let titleChildrenButtonTooltip = "Weitere Kinder laden";
		let normalButtonColor = "white";
		let greyOutButtonColor = "#808080";
		// more parent button
		nodeGroup.append("rect").attr("class", "nodeButton")
			.attr("height", buttonHeight)
			.attr("width", buttonWidth)
			.attr("display", function(d) {
				if (d.hidden) {
					return "none"
				} else {
					return ""
//					if (d.parentCount > 0) {
//						return ""
//					} else {
//						return "none"
//					}
				}
			})
			.attr('opacity', function(d) {
				if (parentsMissing(d.id, d.parentCount) > 0) {
					return 1;
				} else {
					return 0.25;
				}
			})
			.style("fill", function(d) { // grey out the button if no more elements available for tree expansion
				if (parentsMissing(d.id, d.parentCount) > 0) {
					return normalButtonColor;
				} else {
					return greyOutButtonColor;
				}
				
			}).attr("x", kxButton1).attr("y", kyButton)
			.attr("rx", roundCornerX).attr("ry", roundCornerY) // rounded corners for the rect
			.on("click", parentButtonListener)
			.append("svg:title").text(function(d, i) { return titleParentButtonTooltip; })
	
		// more siblings button
		nodeGroup.append("rect").attr("class", "nodeButton")
		.attr("height", buttonHeight)
		.attr("width", buttonWidth)
		.attr("display", function(d) {
			if (d.hidden) {
				return "none"
			} else {
				return ""
//				if (d.siblingCount > 0) {
//					return ""
//				} else {
//					return "none"
//				}
			}
		})
		.attr('opacity', function(d) {
			if (siblingsMissing(d.id, d.siblingCount) > 0) {
				return 1;
			} else {
				return 0.25;
			}
		})
		.style("fill", function(d) { // grey out the button if no more elements available for tree expansion
			if (siblingsMissing(d.id, d.siblingCount) > 0) {
				return normalButtonColor;
			} else {
				return greyOutButtonColor;
			}
			
		}).attr("x", kxButton2).attr("y", kyButton)
		.attr("rx", roundCornerX).attr("ry", roundCornerY) // rounded corners for the rect
		.on("click", siblingButtonListener)
		.append("svg:title").text(function(d, i) { return titleSiblingButtonTooltip; })
		
		// more spouses button
		nodeGroup.append("rect").attr("class", "nodeButton")
		.attr("height", buttonHeight)
		.attr("width", buttonWidth)
		.attr("display", function(d) {
			if (d.hidden) {
				return "none"
			} else {
				return ""
//				if (d.spouseCount > 0) {
//					return ""
//				} else {
//					return "none"
//				}
			}
		})
		.attr('opacity', function(d) {
			if (spousesMissing(d.id, d.spouseCount) > 0) {
				return 1;
			} else {
				return 0.25;
			}
		})
		.style("fill", function(d) { // grey out the button if no more elements available for tree expansion
			if (spousesMissing(d.id, d.spouseCount) > 0) {
				return normalButtonColor;
			} else {
				return greyOutButtonColor;
			}
			
		}).attr("x", kxButton3).attr("y", kyButton)
		.attr("rx", roundCornerX).attr("ry", roundCornerY) // rounded corners for the rect
		.on("click", spousesButtonListener)
		.append("svg:title").text(function(d, i) { return titleSpousesButtonTooltip; })
		
		// more children button
		nodeGroup.append("rect").attr("class", "nodeButton")
		.attr("height", buttonHeight)
		.attr("width", buttonWidth)
		.attr("display", function(d) {
			if (d.hidden) {
				return "none"
			} else {
				return ""
//				if (d.childCount > 0) {
//					return ""
//				} else {
//					return "none"
//				}
			}
		})
		.attr('opacity', function(d) {
			if (childrenMissing(d.id, d.childCount)>0){
				return 1;
			} else {
				return 0.25;
			}
		})
		.style("fill", function(d) { // grey out the button if no more elements available for tree expansion
			if (childrenMissing(d.id, d.childCount)>0){
				//d.childCount > 0) {
				return normalButtonColor;
			} else {
				return greyOutButtonColor;
			}
			
		}).attr("x", kxButton4).attr("y", kyButton)
		.attr("rx", roundCornerX).attr("ry", roundCornerY) // rounded corners for the rect
		.on("click", childrenButtonListener)
		.append("svg:title").text(function(d, i) { return titleChildrenButtonTooltip; })
		
		
		// the button labels
		// more parent button label
		nodeGroup.append("text").text(function(d) {
			if (d.genID !== null && d.genID !== undefined) {
				return "E"
//				if (d.parentCount > 0) {
//					return "E"
//				}
			} else {
				return "";
			}
		})
		.attr('opacity', function(d) {
			if (parentsMissing(d.id, d.parentCount) > 0) {
				return 1;
			} else {
				return 0.25;
			}
		})
		.attr("class", "mouseHoverPointer")
		.attr("x", kxButtonLabel1).attr("y", kyButtonLabel)
		.on("click", parentButtonListener)
		.append("svg:title").text(function(d, i) { return titleParentButtonTooltip; })
		
		// more siblings button label
		nodeGroup.append("text").text(function(d) {
			if (d.genID !== null && d.genID !== undefined) {
				return "G"
//				if (d.siblingCount > 0) {
//					return "G"
//				}
			} else {
				return "";
			}
		})
		.attr('opacity', function(d) {
			if (siblingsMissing(d.id, d.siblingCount) > 0) {
				return 1;
			} else {
				return 0.25;
			}
		})
		.attr("class", "mouseHoverPointer")
		.attr("x", kxButtonLabel2).attr("y", kyButtonLabel)
		.on("click", siblingButtonListener)
		.append("svg:title").text(function(d, i) { return titleSiblingButtonTooltip; })
		
		// more spouses button label
		nodeGroup.append("text").text(function(d) {
			if (d.genID !== null && d.genID !== undefined) {
				return "P"
//				if (d.spouseCount > 0) {
//					return "P"
//				}
			} else {
				return "";
			}
		})
		.attr('opacity', function(d) {
			if (spousesMissing(d.id, d.spouseCount) > 0) {
				return 1;
			} else {
				return 0.25;
			}
		})
		.attr("class", "mouseHoverPointer")
		.attr("x", kxButtonLabel3).attr("y", kyButtonLabel)
		.on("click", spousesButtonListener)
		.append("svg:title").text(function(d, i) { return titleSpousesButtonTooltip; })
		
		// more children button label
		nodeGroup.append("text").text(function(d) {
			if (d.genID !== null && d.genID !== undefined) {
				return "K"
//				if (d.childCount > 0) {
//					return "K"
//				}
			} else {
				return "";
			}
		})
		.attr('opacity', function(d) {
			if (childrenMissing(d.id, d.childCount)>0){
				return 1;
			} else {
				return 0.25;
			}
		})
		.attr("class", "mouseHoverPointer")
		.attr("x", kxButtonLabel4).attr("y", kyButtonLabel)
		.on("click", childrenButtonListener)
		.append("svg:title").text(function(d, i) { return titleChildrenButtonTooltip; })
		
		/**
		 * This defines the line between spouses.
		 */
		function spouseLine(d, i) {
			// start point
			var start = allNodes.filter(function(v) {
				if (d.source == v.id) {
					return true;
				} else {
					return false;
				}
			});
			// end point
			var end = allNodes.filter(function(v) {
				if (d.target == v.id) {
					return true;
				} else {
					return false;
				}
			});
			// define the start coordinate and end coordinate
			var linedata = [ {
				x : start[0].x,
				y : start[0].y
			}, {
				x : end[0].x,
				y : end[0].y
			} ];
			var fun = d3.svg.line().x(function(d) {
				return d.x;
			}).y(function(d) {
				return d.y;
			}).interpolate("linear");
			return fun(linedata);
		}
	}
	drawTree();
	
	// loading feedback for user	
	let loadingFeedback = document.createElement("span");
	divForSVG.appendChild(loadingFeedback);
	loadingFeedback.className = "openFamilyTreeButton loadingFeedback";
	loadingFeedback.innerHTML = "<i>Lade...</i> ⏳";
	$(loadingFeedback).hide();
	
	//listeners for buttons
	function parentButtonListener(d) {
		if(parentsMissing(d.id, d.parentCount)>0)
			buttonListenerHelper(d, '/Servlet?parent', "&childId=");
	};
	function siblingButtonListener(d) {
		// TODO Not implemented for now
		if(false){
		buttonListenerHelper(d, '/Servlet?siblings', "&siblingId=");
		}
	};
	function spousesButtonListener(d) {
		if(spousesMissing(d.id, d.spouseCount)>0)
			buttonListenerHelper(d, '/Servlet?spouses', "&spouseId=");
	};
	function childrenButtonListener(d) {
		if(childrenMissing(d.id, d.childCount)>0)
			buttonListenerHelper(d, '/Servlet?children', "&parentId=");
	};
	function buttonListenerHelper(d, requestCategory, requestParameter) {
		// show feedback to user that application is loading
		$(loadingFeedback).show();
		
		// build the url for the request
		// first the prefix
		let requestURLPrefix = SERVER_IP + requestCategory;
		let requestURL = requestURLPrefix;
		
		// then the id of the node that was clicked
		requestURL += requestParameter + d.genID;
				
		// for testing
		console.log(requestURL);
		
		// prepare the http request
		var dataJSON;
		let xmlhttp = new XMLHttpRequest();
		// behavior on receiving the response of the request
		xmlhttp.onreadystatechange = function () {
			if (this.readyState == 4 && this.status == 200) {
				dataJSON = JSON.parse(this.responseText);
				
				// for testing
				console.log(dataJSON);
				
				if (jQuery.isEmptyObject(dataJSON) == false) {
					// asign new json data for tree
					root = dataJSON;
					
					// refresh tree
					svg.html("");
					drawTree();
					
				} else {
					console.log('The received JSON is empty!');	
				}
				
				// remove the loading feedback
				$(loadingFeedback).hide();
			}
		};
		// send the request
		xmlhttp.open("POST", requestURL, true);
		xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
		let oldTree = JSON.stringify(root, getCircularReplacer());
		xmlhttp.send(oldTree);
	};
	
	
	// full screen button	
	let fullscreenButton = document.createElement("button");
	divForSVG.appendChild(fullscreenButton);
	fullscreenButton.className = "openFamilyTreeButton fullscreenButton";
	fullscreenButton.innerHTML = "⛶";
	fullscreenButton.title = "Vollbildmodus toggeln";
	$(fullscreenButton).click(function() {
	    $(divForSVG).toggleClass('fullscreenDiv');
	});
}

/*
 * To make the nodes in flat mode. This gets all the nodes in same level
 */
function flatten(root) {
	var n = [], i = 0;

	function recurse(node) {
		if (node.children)
			node.children.forEach(recurse);
		if (!node.id)
			node.id = ++i;
		n.push(node);
	}
	recurse(root);
	return n;
}
/**
 * This draws the lines between nodes.
 */
function elbow(d, i) {
	if (d.target.no_parent) {
		return "M0,0L0,0";
	}
	var diff = d.source.y - d.target.y;
	// 0.40 defines the point from where you need the line to break out change
	// is as per your choice.
	var ny = d.target.y + diff * 0.40;

	linedata = [ {
		x : d.target.x + 80,
		y : d.target.y
	}, {
		x : d.target.x + 80,
		y : ny
	}, {
		x : d.source.x + 80,
		y : d.source.y
	} ]

	var fun = d3.svg.line().x(function(d) {
		return d.x;
	}).y(function(d) {
		return d.y;
	}).interpolate("step-after");
	return fun(linedata);
}

const getCircularReplacer = () => {
	const seen = new WeakSet();
	return (key, value) => {
		if (typeof value === "object" && value !== null) {
			if (seen.has(value)) {
				return;
			}
			seen.add(value);
		}
		return value;
	};
};