# MFS
Notizen zu den Treffen sind chronilogisch (Neu oben) sortiert.

## TODO:
- (17.03.20) Vorschlag: Kinder im Stammbaum von links nach rechts nach GenID sortieren, momentan scheint Reihenfolge zufällig
- Im Startbaum werden keine Geschwister mehr mitgeladen (?)

## 14.09.18 (Online-Meeting)
- Suchanfrage stellen und an Server schicken
- Server schickt JSON mit Zwischenergebnisse, Anzeige von Zwischenergebnissen als Liste
- Wähle ein Zwischenergebnis und schicke an Server
- Server schickt initiale JSON für Ergebnisbaum
- Potentielle weitere Anfragen zum Nachladen von weiteren Knoten

## 04.09.18
- nächstes Treffen: noch kein Termin
- Einzelne Äste vom Stammbaum ein-/ausklappen (unsichtbare Knoten über normale Knoten)

## 14.08.18
- nächstes Treffen 04.09.18, 14:00 Uhr 
- Wikipedia-Suche: Nach Kindern in Wikipedia Einträgen suchen und vergleichen mit Datenbank

## 02.08.18
- nächstes Treffen 14.08.18, 11:30 Uhr
- Vorschlag mit separater Ansicht für Liste mit potentiellen Ergebnissen akzeptiert
- Statt kleinen Kreisen als Knoten größere Box mit Details zu Person, mit mehreren Modi (weniger/mehr Details)
- SpouseOf/ChildOf-Beziehungen unterscheiden (z.B. gestrichelte/durchgezogene Kanten), Ebenen abwechselnde Farben zum besseren unterscheiden
- Baum traversieren/explorieren nach oben und unten wichtig (siehe neue Fotos im Ordner GUI Design)

# TODO: 
- Bug: Collapsible Tree Layout: sehr komisches Verhalten wenn man Knoten verschiebt
- Parameter Darstellung: 
    - Age: Int, einfaches Textfeld
    - date of Birth/Death: YYYY-MM-DD, 2x Slider + 2x Textfeld für Birth + Death jeweils
    - Name/Familyname:
    - number of Children:

- Problem: Suche nach Einzelpersonen: siehe Notizblock
    - Erst eine Liste anzeigen von potentiellen Ergebnissen (Grad des Details?)
    - Neue Suche -> Neuer Tab: Beinhaltet Liste + Graph, verschiebbare Split screen Anzeige


## 13.07.18
- Fragen:
	- Verbindungen von Knoten auf gleicher Ebene wie genau? Für Geschwister: Anzeigen per Highlighting beim Hovern (keine Kantenverbindungen mehr)
	- Anzeige von Daten zu individuellen Knoten? Extra Ansicht, Daten öffnen per Klick auf Knoten
- Servlet funktioniert schon, aber gibt momentan gesamte Datenbank aus 
-> ToDo: Verbinden des Servlet mit GUI zur Parameterübergabe	
- Abgabe am 18.10.18

## 15.06.18
- nächstes Meeting am Freitag, 29.06.18, ca. 11:40 Uhr
- Todo: Weitere Bearbeitung der alten Todo's, keine neuen

## 08.06.18
- nächstes Meeting Freitag, 15.06.18 11:30 Uhr
- In Hauptbaum: Knoten hinzufügen auf gleicher Ebene, mit Verbindung zu Nachbarknoten (zB auf Wurzelebene)
- Hauptbaum: Hervorheben von einzelnen Ebenen (zB Label, Hintergrundfarben)
- Allgemein Style/Design der Oberfläche (zB Bootstrap)
- Ansprechen der Datenbank, Visualisierung mit richtigen Daten aus Datenbank

## 29. Mai
- Nächstes Treffen am 08.06.18, 11:15 Uhr
### TODO:
- Visualisierungen in den jeweiligen Ansichten platzieren, Library für Design verwenden, z. B. Bootstrap, jQueryUI o. ä.
- Visualisierung der Hauptansicht drehen
- Datenbank ansprechen, Visualisierung mit richtigen Daten, wenn möglich
- D3 Konflikt mit mehreren Versionen

## 18. Mai
### Features: 
- Pro Suchanfrage ein Tab
    - Kombiniere Collapsive Indented Tree mit Fileroptionen: Graue gefilterte Einträge aus
- M/F Flag automatisch setzen?
- Interaktiv Schreiben/Korrigieren der Datenbank im Falle von falschen 
- Minimap für größere Trees -> möglich?

### TODO: 
- Wikipedia Feature: Direktsuche bzw Liste von potentiellen Personen aus Wikipedia
    - Über Standartbrowser dann aufrufen, eventuell eigenes Fenster (Platzverschwendung?)
    - Standart deutsch, andere sprachen trozdem durchsuchen?
- Indented Tree mit Daten füllen

## 24. April
- Nächste Aufgaben: Einarbeitung in Neo4j und D3, Erstellen von Mockups für Oberfläche
![](./GUIDesign/GUI.png)

## 18. April
- Anwendungsfälle:
    - Alle Informationen zu einer Person anzeigen lassen (Verlinkung?)
    - Verwandtschaft zwischen 2 Personen (Funktion bereits vorhanden)
- Layout Mockup
    - Generationen darstellen, pro Zeile nur eine Generation
    - Übersicht: visuelle Ordnung
    - d3.js 
- Web based, nicht öffentlich 
    - Daten sind von nicht auffindbaren Tschechen
    